BLE Server and Test Application release 0.0.9 2019-5-18
 
Supported platforms:	Windows 10 x86, x64 version 1809, servicing 10.0.17763.X or later.

BLEServer Installation steps:

	1.	Please be sure to install the latest Windows 10 updates.

	2.	Copy all of the installation files to a directory of your choice.

	3.	Create the directory "platforms" within the directory chosen above.

	4.	Move qwindows.dll to the platforms directory.

	5.	Install the Visual Studio 2013 and 2017 redistributable packages (only the 32-bit versions are needed at this time).

	6.	Run the executables BLEServer.exe and TestApplication.exe from the installation path, or take appropriate steps
		to ensure the QT DLLs are accessible.

Changes v0.0.9:

	1.	Move to asynchronouse data communication model on both client and server to enable request cancellation, 
		detection of missing or crashed client/server, real-time error recovery, etc..

	2.	Add keep-alive packet to detect dead client (only).  Ctrl-C and process termination recovery is only supported
		on client side at this time.

	3.	Add dead client detection and associated resource and pipe re-initialization for subsequent re-connect of command
		and both data pipes.

        4.	Adjust timings and synchronization of thread startup, thread shut-down and other multi-thread and inter-process
		communication mechanisms to support asynchronous model.

Changes v0.0.8:
	1.	Addition of MAC filtering. The file btconfig.txt is now scanned during initialization. Only MAC addresses contained in
		this file are enumerated, paired and used.  btconfig.txt should be in the same folder as the executable.

	2.	Improvements to optimize performance during simulataneous command and stream operation.

	Note:  Proper GUI display on DPI systems (greater than 1920x1080) may require additional QT configuration. The included 
	configuration file (qt.conf) file supports 3840x2400 systems.  This file should be placed in the same directory as the 
	executable.

Changes v0.0.7:

	1.	BT Command handling improvements.

Changes v0.0.6:

	1.	Add initial support for auto-pair and auto un-pair functionality.  Auto-pairing of two (and only) two devices during
		BLEServer startup.  Auto-unpair occurs when user presses 'q' to exit BLEServer.  

	2.	Device handle refresh for un-paired initialization / enumeration. 

	3.	Populate device state field of GUI.

	4.	Startup with streaming disabled.

Changes (v0.0.5:

	1.	Streams are no longer started at initialization.  Press the "Start IMU Streaming" button or the "start"
		button after selecting the individual device to start streaming.  Note: Device Enumeration still occurs
		at initialization.

	2.	Preliminary support has been added for the "link" and "terminate link" buttons.  Additional testing
		is needed here.  The "Terminate Active Links" button should also work.

	3.	Additional support for dynamic device recovery (priorities 3-5) added to improve robustness.

	4.	Device State and BLE Controller status text fields updated to reflect additional situations, this feature is 
		not complete yet.

	5.	Code cleanup, additional removal of BlueGiga code.


Changes (v0.04 (0.03 did not include #4):

	1.	Server now supports multiple invocations of the Test Application (one at a time).  The server no longer 
		must be restarted between test sessions.

	2.	Device detection has been extended to support device tree updates.  This eliminates a condition where
		devices were not always detected during initialization.

	3.	A conection check has been added before device operations are performed.

	4.	Added dynamic device insertion and removal.

Changes (v0.02):

	1.	Support has been added to stop and start the stream of each device from within the GUI. 

	2.	Minimal device status has been added to the GUI.

	3.	Dual device/stream operation is now supported.  Device data is displayed on the user terminal and also stored in separate log files.
		Presumably the terminal data dump is not necessary and will be removed or at least disabled in a future (probably next) release.

	4.	Device and resource cleanup is now performed between the client and server during GUI application exit. This improves the user
		experience and prevents the server from spewing errors.  Improved resource managment will eventually allow multiple TestApplication
		runs while the server is still running, but this is not supported yet.

	5.	Major cleanup/removal of old BlueGiga code.

	6.	Users should now press 'q' to exit the server application. This will allow proper cleanup of Windows resources and
		help aid in testing. Improper exit could leave devices 'hanging' and cause time-outs on some platforms.  Such an issue will normally 
		arise during the next execution of the server, especially if it is immediate.

	7.	The Test Application application can be exited by simply pressing the 'X' at the top right of the GUI window.  Proper cleanup of pipes, threads
		and other Windows respources is attempted before this program exits.

Errata:
	1.	The server must be restarted for each use/test-run, restarting TestApplication.exe is not curently supported during execution of the same 
		BleServer instance.

	2.	Bluetooth devices should be paried.

	3.	Multiple payloads may arrive on the client side at times.  This normal and will vary with system performance.  The data can easily be separated
		if needed.

	4.	Dynamic device removal, add and updates are not currently supported.

	5.	Corner cases may exist that require additional and or sprecial handling.  Limited testing has not revealed any major flaws/issues so far.

	6.	The implementaton as it stands currently only streams the 0xFFF4 characteristic off of service 0000FFF0-0000-1000-8000-00805F9B34FB from each 
		device.  





