
BLE Server and Test Application release 0.04 2019-3-10

Supported platforms:	Windows 10 x86, x64 version 1809, servicing 10.0.17763.X or later.

BLEServer Installation steps:

	1.	Please be sure to install the latest Windows 10 updates.

	1.	Make the following directory on drive C: and copy the attached qwindows.dll to it:

			C:\qt\5.5\msvc2013\plugins\platforms

	2.	Add an envionment variable named QT_PLUGIN_PATH that points to the directory above.

	3.	Copy all of the installation files to an installtion directory of your choice.

	4.	Install the Visual Studio 2013 and 2017 redistributable packages (only the 32-bit versions are needed at this time).

	5.	Run the executables BLEServer.exe and TestApplication.exe from the installation path, or take appropriate steps
		to ensure the QT DLLs are accessible.


Changes (v0.04 (0.03 did not include #4):

	1.	Server now supports multiple invocations of the Test Application (one at a time).  The server no longer 
		must be restarted between test sessions.

	2.	Device detection has been extended to support device tree updates.  This eliminates a condition where
		devices were not always detected during initialization.

	3.	A conection check has been added before device operations are performed.

	4.	Added dynamic device insertion and removal.

Changes (v0.02):

	1.	Support has been added to stop and start the stream of each device from within the GUI. 

	2.	Minimal device status has been added to the GUI.

	3.	Dual device/stream operation is now supported.  Device data is displayed on the user terminal and also stored in separate log files.
		Presumably the terminal data dump is not necessary and will be removed or at least disabled in a future (probably next) release.

	4.	Device and resource cleanup is now performed between the client and server during GUI application exit. This improves the user
		experience and prevents the server from spewing errors.  Improved resource managment will eventually allow multiple TestApplication
		runs while the server is still running, but this is not supported yet.

	5.	Major cleanup/removal of old BlueGiga code.

	6.	Users should now press 'q' to exit the server application. This will allow proper cleanup of Windows resources and
		help aid in testing. Improper exit could leave devices 'hanging' and cause time-outs on some platforms.  Such an issue will normally 
		arise during the next execution of the server, especially if it is immediate.

	7.	The Test Application application can be exited by simply pressing the 'X' at the top right of the GUI window.  Proper cleanup of pipes, threads
		and other Windows respources is attempted before this program exits.

Errata:
	1.	The server must be restarted for each use/test-run, restarting TestApplication.exe is not curently supported during execution of the same 
		BleServer instance.

	2.	Bluetooth devices should be paried.

	3.	Multiple payloads may arrive on the client side at times.  This normal and will vary with system performance.  The data can easily be separated
		if needed.

	4.	Dynamic device removal, add and updates are not currently supported.

	5.	Corner cases may exist that require additional and or sprecial handling.  Limited testing has not revealed any major flaws/issues so far.

	6.	The implementaton as it stands currently only streams the 0xFFF4 characteristic off of service 0000FFF0-0000-1000-8000-00805F9B34FB from each 
		device.  





