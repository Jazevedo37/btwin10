//
// BLEServer: Enumerates and provides BLE services for TechMah devices.
// Author: Dan Rebello
//
//

#define	UNCACHED_MODE		1
#define CONNECT_DEBUG       1

#include "stdafx.h"
#include <iostream>
#include <initguid.h>
#include <winsock2.h>
#include <ws2bth.h>
#include <Windows.Foundation.h>
#include <Windows.Devices.Bluetooth.h>
#include <Windows.Devices.Bluetooth.Advertisement.h>
#include <wrl/wrappers/corewrappers.h>
#include <wrl/event.h>
#include <collection.h>
#include <ppltasks.h>
#include <string>
#include <sstream> 
#include <iomanip>
#include <experimental/resumable>
#include <pplawait.h>
#include "version.h"
#include "pipeprot.h"


#include <stdio.h>
#include "concrt.h"

#pragma comment(lib,"WS2_32")

#define DEVICE_ACCESS_ENUM_STR(status) ((unsigned)status == 1 ? "Allowed" : ((unsigned)status==3 ? "DeniedBySystem" : ((unsigned)status==2 ? "DeniedByUser" : "Unspecified")))

using namespace Platform;
using namespace Windows::Devices;
using namespace std;
using namespace concurrency;
using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;

//using namespace Windows::Foundation;
using namespace Windows::Devices::Enumeration;
using namespace Windows::Devices::Bluetooth;
using namespace Windows::Devices::Bluetooth::GenericAttributeProfile;
using namespace Windows::Devices::Bluetooth::Rfcomm;
using namespace Windows::Storage::Streams;

#include "BLEServer.h"
using namespace BleSrvr;
//#pragma warning (default : 4692)
//#include "make_public_pragma.h"
//#pragma make_public(BLEServer)

// Characteristics in the information service
static const uint16 CHARACTERISTIC_SYSTEM_ID_UUID			= 0x2a23;			///< UUID for system ID characteristic
static const uint16 CHARACTERISTIC_MODEL_NUMBER_UUID		= 0x2a24;			///< UUID for model number characteristic (e.g., ADEX-0003)
static const uint16 CHARACTERISTIC_SERIAL_NUMBER_UUID		= 0x2a25;			///< UUID for serial number characteristic (e.g., 2015-03-00002)
static const uint16 CHARACTERISTIC_FIRMWARE_REVISION_UUID	= 0x2a26;			///< UUID for firmware revision characteristic (e.g., 000116 Rev.A)
static const uint16 CHARACTERISTIC_HARDWARE_REVISION_UUID	= 0x2a27;			///< UUID for hardware revision characteristic (e.g., 000076 Rev.A)
static const uint16 CHARACTERISTIC_SOFTWARE_REVISION_UUID	= 0x2a28;			///< UUID for software revision characteristic (e.g., 000115 Rev.A)
static const uint16 CHARACTERISTIC_MANUFACTURER_NAME_UUID	= 0x2a29;			///< UUID for manufacturer name characteristic

// Characteristics in the IMU and LED service
static const uint16 CHARACTERISTIC_MEASUREMENT_UUID			= 0xFFF4;			///< UUID for finding handle for reading IMU measurements
static const uint16 CHARACTERISTIC_LED_UUID					= 0xFFF6;			///< UUID for finding handle for LED state commands
static const uint16 CHARACTERISTIC_STREAMING_CONFIG_UUID	= 0x2902;			///< UUID for turning data streaming on/off

auto serviceUUID = Bluetooth::BluetoothUuidHelper::FromShortId(0xffe5);
auto characteristicUUID = Bluetooth::BluetoothUuidHelper::FromShortId(0xffe9);

void NotifyUser(String^ strMessage);

int gDeviceCnt = 0;
int gIdx = 0; 
//unsigned long long gBTAddress = 0;
unsigned short precursorArray[10] = { 0x0003, 0x1810, 0x181B, 0x181E, 0x181F, 0x1818, 0x1816, 0x1808, 0x1809, 0x180d };


std::wstring formatBluetoothAddress(unsigned long long BluetoothAddress) {

	std::wostringstream ret;

	ret << std::hex << std::setfill(L'0')

		<< std::setw(2) << ((BluetoothAddress >> (5 * 8)) & 0xff) << ":"

		<< std::setw(2) << ((BluetoothAddress >> (4 * 8)) & 0xff) << ":"

		<< std::setw(2) << ((BluetoothAddress >> (3 * 8)) & 0xff) << ":"

		<< std::setw(2) << ((BluetoothAddress >> (2 * 8)) & 0xff) << ":"

		<< std::setw(2) << ((BluetoothAddress >> (1 * 8)) & 0xff) << ":"

		<< std::setw(2) << ((BluetoothAddress >> (0 * 8)) & 0xff);

	return ret.str();

}

#define AdvertisementWatcherMethod	0

BTPipeMessage gMsg;
BLEServer gServerInst;
BOOL gBLEThreadRun = true;

vector<unsigned __int64> gAllowedMACS;
HANDLE gCmdPipeHandle;

void BLEServer::BLEThread(HANDLE *pPipeHandle) {
	HANDLE pipeHandle = *pPipeHandle;
	DWORD bytesRead = 0, bytesWritten;
	BLEDeviceInstance^ pInst;
    int requestCnt = 0;
    DWORD keepAliveTick = 0;
    bool firstRead = false;
    bool connected = false;

	printf("\nBLEServer v%d.%d.%d: Ready:\n", VER_MAJOR, VER_MINORH, VER_MINORL);
	BTPipeMessage& msg = gMsg;
    BTPipeMessage msgKA;
    memset(&msgKA, 0, sizeof(BTPipeMessage));
    msgKA.msgId = BT_KEEP_ALIVE;

    DWORD lastTick = 0;
    int paramIdx = 0;
    OVERLAPPED overlapped;
    HANDLE cmdReadEvent;
    if( (cmdReadEvent = CreateEvent(NULL, TRUE, FALSE, NULL))==NULL)
        printf("Failed to create cmdReadEvent, error %d.\n", GetLastError());
    
    bool gotCmd = false;
    bool readPending = false;
	while (gBLEThreadRun) {
        DWORD tick = GetTickCount();
        if (requestCnt > 1 && tick - lastTick >= 500) {
            lastTick = tick;
            if (connected && tick > keepAliveTick + 1500) {
                printf("BLEServer: Keep Alive dead client detected.\n");
                // Now re-initialize any data pipes that may have been up
                for (auto macit = gServerInst.mMAC2Inst.begin(); macit != gServerInst.mMAC2Inst.end(); macit++) {
                    pInst = macit->second;
                    // Remove any value changed event notifications that might be registered.
                    vector<int>::iterator it = pInst->mActiveNotificationUUIDs.begin();
                    while ( it != pInst->mActiveNotificationUUIDs.end()) {
                        int uuid = *it;
                        printf("Removing UUID %d from Active Notifications for MAC %llX.\n", uuid, pInst->mMACAddr);
                        bool rc = SetCharacteristicNotification(macit->first, uuid, 0);
                        if (!rc) 
                            pInst->mActiveNotificationUUIDs.erase(it);

                        printf("Removing value changed callback.\n");
                        UnInstallCharacteristicValueChangedCallback(macit->first, uuid);
                        it = pInst->mActiveNotificationUUIDs.begin();
                    }

                    printf("Disconnecting DATA pipe for MAC %llX.\n", pInst->mMACAddr);
                    DisconnectNamedPipe((HANDLE)(pInst->mGetPipeHandle()));  
                    OVERLAPPED dataPipeAsync;
                    memset(&dataPipeAsync, 0, sizeof(OVERLAPPED));
                    ConnectNamedPipe((HANDLE)(pInst->mGetPipeHandle()), &dataPipeAsync);
                }

                printf("Disconnecting CMD pipe.\n");
                // Re-iniaitlize the command pipe, get it ready to accept a new connection
				DisconnectNamedPipe(pipeHandle);		// Perhaps a new client will connect to this pipe later				
				OVERLAPPED cmdPipeAsync;
				memset(&cmdPipeAsync, 0, sizeof(OVERLAPPED));
				ConnectNamedPipe(pipeHandle, &cmdPipeAsync);
                firstRead = connected = false;
                requestCnt = 0;
            }
        }

        if (!firstRead && requestCnt == 0) {    // Init case
				memset(&overlapped, 0 , sizeof(OVERLAPPED));
                overlapped.hEvent = cmdReadEvent;
                DWORD bytesRead;
                bool rc = ReadFile(pipeHandle, &msg, sizeof(BTPipeMessage), &bytesRead, &overlapped);
                DWORD lastError = GetLastError();
                if (rc == 0 && lastError == ERROR_IO_PENDING) {
                    firstRead = readPending = true;
                    printf("First read started\n");
                }
                else if(rc && bytesRead) {
                    firstRead = true;
                    gotCmd = true;
                }
                // 536 - ERROR_PIPE_LISTENING, 232 - ERROR_NO_DATA, ERROR_IO_PENDING - 997
                if (lastError != ERROR_PIPE_LISTENING && lastError != ERROR_NO_DATA) 
                    printf("Readfile rc=%d GetLastError %d bytesRead %d\n", rc, lastError, bytesRead);
        }

		DWORD wfsorc = WAIT_TIMEOUT;
        if (readPending)
            wfsorc = WaitForSingleObject(cmdReadEvent, 0);
        if (wfsorc == WAIT_OBJECT_0) {
            ResetEvent(cmdReadEvent);
            gotCmd = true;
        }

        if(gotCmd) {
            gotCmd = false;
			keepAliveTick = GetTickCount();
            requestCnt++;
            bool result = GetOverlappedResult(pipeHandle, &overlapped, &bytesRead, FALSE);
            if (bytesRead != sizeof(BTPipeMessage)) {
                printf("BLEServer: Corrupt pipe message, bytesRead=%d.\n", bytesRead);
                continue;
            }

//printf("BLEServer: Pipe processing msg=0x%x bytesRead %d\n", msg.msgId, bytesRead);
			switch (msg.msgId) {

			case BT_DEVICE_INFO:
				msg.rc = (BTPipeMsgReturnCode)gServerInst.GetDeviceInfo(msg.macAddr, &msg.param[0], &msg.param[1], &msg.param[2]);
				if (!WriteFile(pipeHandle, &msg, sizeof(BTPipeMessage), &bytesWritten, NULL)) {
					printf("BLEServer: Error writing msg response to pipe.\n");
					continue;
				}
				break;

            case BT_SCAN:	
			{
				gServerInst.EnumerateBLE();
				printf("BT_SCAN: gDeviceCnt=%d\n", gDeviceCnt);
				unsigned int cnt = 0;
				while (gDeviceCnt < 1 && cnt++ < 10) {
					Sleep(50);
					printf(".");
				}
                printf("\n");
				msg.param[0] = gDeviceCnt;				// Count of devices found
				printf("BLEServer: Found %d connected devices.\n", gDeviceCnt);
                // Return MAC address for each  device found, this will be the device handle
				paramIdx = 0;
                for (auto it = mMAC2Inst.begin(); it != mMAC2Inst.end(); it++) {
					BLEDeviceInstance^ pInst = it->second;
printf("%s: MAC %llX\n", __FUNCTION__, pInst->mMACAddr);
                    msg.param[1+paramIdx*2] = pInst->mMACAddr >> 32;
                    msg.param[1+paramIdx*2+1] = pInst->mMACAddr & 0xFFFFFFFF;
					paramIdx++;
                }

                if (gDeviceCnt >= 2) {
                    connected = true;
                }

				if (gDeviceCnt)
					msg.rc = BT_SUCCESS;
				else msg.rc = BT_FAIL;

				if (!WriteFile(pipeHandle, &msg, sizeof(BTPipeMessage), &bytesWritten, NULL)) {
					printf("BLEServer: Error writing msg respose to pipe.\n");
					continue;
				}
                keepAliveTick = GetTickCount();
			}
				break;

            case BT_SET_MAC_ADDRESSES:
                for(unsigned int x=1; x < msg.param[0]+1 && x < 16; x++) {
                    gAllowedMACS.push_back((unsigned __int64)msg.param[x] << 32 | msg.param[x+1]);
                    printf("BT_SET_MAC_ADDRESS: Adding MAC %llx\n", (unsigned __int64)msg.param[x] << 32 | msg.param[x+1]);
                }
                msg.rc = BT_SUCCESS;
                if (!WriteFile(pipeHandle, &msg, sizeof(BTPipeMessage), &bytesWritten, NULL)) {
                    printf("BLEServer: Error writing msg respose to pipe.\n");
                    continue;
                }
                break;

			case BT_SET_LED:
				msg.rc = BT_SUCCESS;
				if (!WriteFile(pipeHandle, &msg, sizeof(BTPipeMessage), &bytesWritten, NULL)) {
					printf("BLEServer: Error writing msg respose to pipe.\n");
					continue;
				}
				break;

			case BT_READ_CHARACTERISTIC:
				if (gServerInst.ReadCharacteristic(msg.macAddr, msg.param[0], msg.buffer, sizeof(msg.buffer), &msg.bufLen))
					msg.rc = BT_SUCCESS;
				else msg.rc = BT_FAIL;

				printf("bufLen=%d\n", msg.bufLen);
				if (!WriteFile(pipeHandle, &msg, sizeof(BTPipeMessage), &bytesWritten, NULL)) {
					printf("BLEServer: Error writing msg respose to pipe.\n");
					continue;
				}
				break;

			case BT_WRITE_CHARACTERISTIC:
				if (gServerInst.WriteCharacteristic(msg.macAddr, msg.param[0], msg.buffer, msg.bufLen))
					msg.rc = BT_SUCCESS;
				else msg.rc = BT_FAIL;

				if (!WriteFile(pipeHandle, &msg, sizeof(BTPipeMessage), &bytesWritten, NULL)) {
					printf("BLEServer: Error writing msg respose to pipe.\n");
					continue;
				}
				break;

			case BT_ENABLE_NOTIFICATION:
				printf("BT_ENABLE_NOTIFICATION: Installing ValueChanged callback for MAC %llX.\n", msg.macAddr);
				gServerInst.InstallCharacteristicValueChangedCallback(msg.macAddr, msg.param[0]);	// Install Callback and notifications for 0xFFF4
				printf("BT_ENABLE_NOTIFICATION: Enabling notification for MAC %llX.\n", msg.macAddr);
				gServerInst.SetCharacteristicNotification(msg.macAddr, msg.param[0], 1);
				msg.rc = BT_SUCCESS;
				if (!WriteFile(pipeHandle, &msg, sizeof(BTPipeMessage), &bytesWritten, NULL)) {
					printf("BLEServer: Error writing msg respose to pipe.\n");
					continue;
				}
				printf("BT_ENABLE_NOTIFICATION complete.\n");
				break;

			case BT_DISABLE_NOTIFICATION:
				printf("BT_DISABLE_NOTIFICATION: Disabling notification for MAC %llX.\n", msg.macAddr);
				gServerInst.SetCharacteristicNotification(msg.macAddr, msg.param[0], 0);
				printf("BT_DISABLE_NOTIFICATION: UnInstalling ValueChanged callback for MAC %llX.\n", msg.macAddr);
				gServerInst.UnInstallCharacteristicValueChangedCallback(msg.macAddr, msg.param[0]);	// Install Callback and notifications for 0xFFF4
				
				msg.rc = BT_SUCCESS;
				if (!WriteFile(pipeHandle, &msg, sizeof(BTPipeMessage), &bytesWritten, NULL)) {
					printf("BLEServer: Error writing msg respose to pipe.\n");
					continue;
				}
				printf("BT_DISABLE_NOTIFICATION complete.\n");
				break;

			case BT_DISCONNECTING_DATA_PIPE:
				printf("BT_DICONNECTING_DATA_PIPE: MAC %llX.\n", msg.macAddr);
				
				pInst = gServerInst.mGetDeviceInstance(msg.macAddr);
                DisconnectNamedPipe((HANDLE)(pInst->mGetPipeHandle()));  
				OVERLAPPED dataPipeAsync;
				memset(&dataPipeAsync, 0, sizeof(OVERLAPPED));
				ConnectNamedPipe((HANDLE)(pInst->mGetPipeHandle()), &dataPipeAsync);
				msg.rc = BT_SUCCESS;
				if (!WriteFile(pipeHandle, &msg, sizeof(BTPipeMessage), &bytesWritten, NULL)) {
					printf("BLEServer: Error writing msg respose to pipe.\n");
					continue;
				}
				printf("BT_DICONNECTING_DATA_PIPE: complete.\n");
				break;

			case BT_DISCONNECTING_CMD_PIPE:
				printf("BT_DICONNECTING_CMD_PIPE:\n");
				DisconnectNamedPipe(pipeHandle);		// Perhaps a new client will connect to this pipe later				
				OVERLAPPED cmdPipeAsync;
				memset(&cmdPipeAsync, 0, sizeof(OVERLAPPED));
				ConnectNamedPipe(pipeHandle, &cmdPipeAsync);
				printf("BT_DICONNECTING_CMD_PIPE: complete.\n");
                firstRead = connected = false;
                requestCnt = 0;
				break;

            case BT_KEEP_ALIVE:

                msg.param[0] = gServerInst.mGetDeviceCnt(); // Return devuce count in param[0]
                for (int x=0; x < 2*3; x+=3) {      // Initialize keep alive status reply msg
                    msg.param[1+x] = 0;             // Initialize bits 32-64 of MAC address for device
                    msg.param[1+x+1] = 0;           // Initialize bits 0-31 of MAC address for device
                    msg.param[1+x+2] = 0;           // Initialize connect status of device, 0 is disconnected
                }
                // Return device connect statuses in param[1] & param[2] 
                paramIdx=0;
                for (auto it = mMAC2Inst.begin(); it != mMAC2Inst.end(); it++) {
                    pInst = it->second;
//printf("KA: MAC %llX\n", pInst->mMACAddr);
                    msg.param[1+paramIdx*3] = pInst->mMACAddr >> 32;
                    msg.param[1+paramIdx*3+1] = pInst->mMACAddr & 0xFFFFFFFF;
                    msg.param[1+paramIdx*3+2] = pInst->mConnected;
                    paramIdx++;
                }
//                printf("KA cnt %d resp %x %x\n", paramIdx, msg.param[3], msg.param[6]);

                // Send keep-alive packet to the client.  If this write fails the client has crashed or been terminated.
                if (!WriteFile(pipeHandle, &msg, sizeof(BTPipeMessage), &bytesWritten, NULL)) {
                    printf("BLEServer: Cannot send Keep Alive response, error %d\n", GetLastError());

                    printf("Disconnecting CMD pipe.\n");
                    DisconnectNamedPipe(pipeHandle);		// Perhaps a new client will connect to this pipe later				
                    OVERLAPPED cmdPipeAsync;
                    memset(&cmdPipeAsync, 0, sizeof(OVERLAPPED));
                    ConnectNamedPipe(pipeHandle, &cmdPipeAsync);
                    firstRead = connected = false;
                    requestCnt = 0;
                }
                else keepAliveTick = GetTickCount();
                break;

			default:
				printf("BLEServer: Invalid pipe message!\n");
			}

            // Start another read
            memset(&overlapped, 0 , sizeof(OVERLAPPED));
            overlapped.hEvent = cmdReadEvent;
            DWORD bytesRead;
            bool rc = ReadFile(pipeHandle, &msg, sizeof(BTPipeMessage), &bytesRead, &overlapped);
            DWORD lastError = GetLastError();
            if (rc == 0 && lastError == ERROR_IO_PENDING) {
                readPending = true;
            }
            else if(rc && lastError == ERROR_SUCCESS && bytesRead) {
                gotCmd = true;
            }
		}
	}
	CloseHandle(pipeHandle);

	printf("BLEServer: Thread exiting.\n");
}

void BLEThreadStarter(HANDLE *handle) {
	gServerInst.BLEThread(handle);
}

int main(Array<String^>^ args) {

	Microsoft::WRL::Wrappers::RoInitializeWrapper initialize(RO_INIT_MULTITHREADED);
	   
	CoInitializeSecurity(nullptr, // TODO: "O:BAG:BAD:(A;;0x7;;;PS)(A;;0x3;;;SY)(A;;0x7;;;BA)(A;;0x3;;;AC)(A;;0x3;;;LS)(A;;0x3;;;NS)"
				-1, nullptr,nullptr,
				RPC_C_AUTHN_LEVEL_DEFAULT,	RPC_C_IMP_LEVEL_IDENTIFY,
				NULL, EOAC_NONE, nullptr);

	HANDLE pipeHandle;
	char tmpstr[256];
	strcpy(tmpstr, "\\\\.\\pipe\\btserver");
	printf("BLEServer: Opening CMD pipe: %s\n", tmpstr);

/*	if( (pipeHandle = CreateNamedPipeA((LPCSTR)tmpstr, PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED, 
                                       PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_NOWAIT | PIPE_WAIT,
		2,	// 1 instances
        2048, 2048, 0, NULL)) == INVALID_HANDLE_VALUE)
*/ 
	if( (pipeHandle = CreateNamedPipeA((LPCSTR)tmpstr, PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED, 
                                       PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,
        PIPE_UNLIMITED_INSTANCES,   // Unlimited instances
//		2,	// 2 instances
		2048, 2048, 0, NULL)) == INVALID_HANDLE_VALUE) 
    {
		printf("\nBLEServer: CMD Pipe creation failed for reason %d.\n", GetLastError());
			exit(1);
	}

	OVERLAPPED cmdAsync;
	memset(&cmdAsync, 0, sizeof(OVERLAPPED));
	ConnectNamedPipe(pipeHandle, &cmdAsync);

	printf("BLEServer: CMD Pipe created.\n");

	DWORD bleThreadId;
	HANDLE bleThreadHandle;
	
    if ((bleThreadHandle = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)&BLEThreadStarter, &pipeHandle, 0, &bleThreadId)) == NULL) {
		printf("BLEServer: Thread Creation failed, rc=%d.\n", GetLastError());
		exit(1);
	}

	printf("Press 'q' to quit.\n");
	while (1) {
		int c = std::cin.peek();		
		if (c == 'q' || c == 'Q')
		{
			printf("BLEServer: Shutting down...\n");
			gServerInst.CloseDevices();
			gBLEThreadRun = false;
			break;
		}
		else {
			if (c != EOF) {
				std::cin >> c;
			}
		}		
	}

#if AdvertisementWatcherMethod
	Bluetooth::Advertisement::BluetoothLEAdvertisementWatcher^ bleAdvertisementWatcher = ref new Bluetooth::Advertisement::BluetoothLEAdvertisementWatcher();

	bleAdvertisementWatcher->ScanningMode = Bluetooth::Advertisement::BluetoothLEScanningMode::Active;

	bleAdvertisementWatcher->Received += ref new Windows::Foundation::TypedEventHandler<Bluetooth::Advertisement::BluetoothLEAdvertisementWatcher ^, Windows::Devices::Bluetooth::Advertisement::BluetoothLEAdvertisementReceivedEventArgs ^>(

		[bleAdvertisementWatcher](Bluetooth::Advertisement::BluetoothLEAdvertisementWatcher ^watcher, Bluetooth::Advertisement::BluetoothLEAdvertisementReceivedEventArgs^ eventArgs) {

		auto serviceUuids = eventArgs->Advertisement->ServiceUuids;
		std::cout << "Found: " << eventArgs->Advertisement->LocalName->Data() << "\n";
		auto count = eventArgs->Advertisement->ServiceUuids->Size;
		for (unsigned int x = 0; x < count; x++) {
			//printf("UUID: %s\n", eventArgs->Advertisement->ServiceUuids->GetAt(x));
			printf("got one");
		}
		unsigned int index = -1;

		if (serviceUuids->IndexOf(serviceUUID, &index)) {

			String^ strAddress = ref new String(formatBluetoothAddress(eventArgs->BluetoothAddress).c_str());

			std::wcout << "Target service found on device: " << strAddress->Data() << std::endl;

			bleAdvertisementWatcher->Stop();
			connectToBulb(eventArgs->BluetoothAddress);
		}
	});

	bleAdvertisementWatcher->Start();
	
	int a;

	std::cin >> a;
#endif
	return 0;

}

void UnPair(BLEDeviceInstance^ pInst) {
	printf("Attempting unpair of device.\n");
	IAsyncOperation<DeviceUnpairingResult^>^ upr = pInst->mGetDevObj()->DeviceInformation->Pairing->UnpairAsync();
	auto unPairTask = create_task(upr);
	DeviceUnpairingResult^ unpairResult = unPairTask.get();
	if (unpairResult->Status == Windows::Devices::Enumeration::DeviceUnpairingResultStatus::Unpaired) {
		printf("Device Successfully unpaired!\n");
	}
	else printf("upr->Status = %d\n", upr->Status);
}

void BLEServer::CloseDevices() {
//	std::map<DWORD64, String^>::iterator devIdIt;
	std::map<DWORD64, BLEDeviceInstance^>::iterator mac2instIt;

	BLEDeviceInstance^ pInst;

    for (auto it = mMAC2Inst.begin(); it != mMAC2Inst.end(); ) {
		pInst = it->second;

		printf("BLEServer: Closing MAC %llX.\n", pInst->mMACAddr);

		// attempt to unpair
        if(pInst->mGetDevObj() && pInst->mGetDevObj()->DeviceInformation->Pairing->IsPaired) {
			UnPair(pInst);
        }
		
		printf("BLEServer: Calling BluetoothLEDevice Destructor.\n");
		pInst->mGetDevObj()->~BluetoothLEDevice();
		printf(" .");
		auto devIt = mDeviceId2BLEDevInst.find(pInst->mDeviceIdStr);
		if(devIt != mDeviceId2BLEDevInst.end())
			mDeviceId2BLEDevInst.erase(devIt);

		printf(" .");		
		for (auto idx2devIt = mIdx2DeviceId.begin(); idx2devIt != mIdx2DeviceId.end(); ) {
			mIdx2DeviceId.erase(idx2devIt);
			idx2devIt = mIdx2DeviceId.begin();
		}
		printf(" .");

//        devIdIt = mMAC2DeviceId.find(pInst->mMACAddr);
//        if (devIdIt != mMAC2DeviceId.end())
//            mMAC2DeviceId.erase(devIdIt);

        mac2instIt = mMAC2Inst.find(pInst->mMACAddr);
		if(mac2instIt != mMAC2Inst.end())
			mMAC2Inst.erase(mac2instIt);

		it = mMAC2Inst.begin();
		printf(" .\n");
	}
}

unsigned int GetCharacteristicType(char *cstr) {
	char charNumStr[16];
	unsigned int characteristicNum;

	memset(charNumStr, 0, sizeof(charNumStr));
	strncpy(charNumStr, cstr + 1, 8);
	sscanf(charNumStr, "%x", &characteristicNum);
	return(characteristicNum & 0xFFFF);
}

char *GetCharacteristicNameFromType(unsigned int characteristicNum) {

	switch (characteristicNum) {
	   	case 0x2A00:
			return("Device Name");
		case 0x2A01:
			return("Appearance");
		case 0x2A02:
			return("Peripheral Privacy Flag");
		case 0x2A03:
			return("Reconnection Address");
		case 0x2A04:
			return("Peripheral Preferred Connection Parameters");

		case CHARACTERISTIC_SYSTEM_ID_UUID:
			return("CHARACTERISTIC_SYSTEM_ID_UUID");
		case CHARACTERISTIC_MODEL_NUMBER_UUID:
			return("CHARACTERISTIC_MODEL_NUMBER_UUID");
		case CHARACTERISTIC_SERIAL_NUMBER_UUID:
			return("CHARACTERISTIC_SERIAL_NUMBER_UUID");
		case CHARACTERISTIC_FIRMWARE_REVISION_UUID:
			return("CHARACTERISTIC_FIRMWARE_REVISION_UUID");
		case CHARACTERISTIC_HARDWARE_REVISION_UUID:
			return("CHARACTERISTIC_HARDWARE_REVISION_UUID");
		case CHARACTERISTIC_SOFTWARE_REVISION_UUID:
			return("CHARACTERISTIC_SOFTWARE_REVISION_UUID");
		case CHARACTERISTIC_MANUFACTURER_NAME_UUID:
			return("CHARACTERISTIC_MANUFACTURER_NAME_UUID");
			
		// Characteristics in the IMU and LED service
				
		case CHARACTERISTIC_MEASUREMENT_UUID:
			return("CHARACTERISTIC_MEASUREMENT_UUID");
		case CHARACTERISTIC_LED_UUID:
			return("CHARACTERISTIC_LED_UUID");
		case CHARACTERISTIC_STREAMING_CONFIG_UUID:
			return("CHARACTERISTIC_STREAMING_CONFIG_UUID");
			
		default:
			return("Unknown");
	}

	return("Unknown\n");
}

char *GetCharacteristicName(char *cstr) {
	unsigned int characteristicNum;

	characteristicNum = GetCharacteristicType(cstr);
	return(GetCharacteristicNameFromType(characteristicNum));
}


bool BLEServer::DeviceInstanceFromIdx(int idx, BLEDeviceInstance^& devInst) {
	std::map<int, String^>::iterator devIdIt;
	std::map<String^, BLEDeviceInstance^>::iterator devIt;

	if ((devIdIt = mIdx2DeviceId.find(idx)) == mIdx2DeviceId.end())
		return false;

	String^ deviceIdStr = devIdIt->second;

	printf("DeviceInstanceFromIdx: idx %d\n", idx);
	NotifyUser("Device ID = " + deviceIdStr);

	if ((devIt = mDeviceId2BLEDevInst.find(deviceIdStr)) == mDeviceId2BLEDevInst.end()) {
		printf("DeviceInstanceFromIdx: idx %d NOT FOUND!\n", idx);
		return false;
	}
	devInst = devIt->second;
	return true;

}

int BLEServer::GetDeviceInfo(DWORD64 macAddr, unsigned int *btAdrHi, unsigned int *btAdrLo, unsigned int *connectStatus) {

	std::map<DWORD64, BLEDeviceInstance^>::iterator mac2instIt;
	int rc = BT_FAIL;

	printf("GetDeviceInfo: mac %llX\n", macAddr);

	BLEDeviceInstance^ pInst;
	mac2instIt = mMAC2Inst.find(macAddr);
	if (mac2instIt != mMAC2Inst.end()) {
		pInst = mac2instIt->second;
		printf("GetDeviceInfo: pInst %x\n", (unsigned int)(int *)pInst);
		if (pInst) {
			rc = BT_SUCCESS;
			*btAdrHi = pInst->mGetDevObj()->BluetoothAddress >> 32;
			*btAdrLo = pInst->mGetDevObj()->BluetoothAddress & 0xFFFFFFFF;
			*connectStatus = pInst->mIsConnected() ? 1 : 0;
		}
		printf("*btAdrHi %x *btAdrLo %x *connectStatus %x\n", *btAdrHi, *btAdrLo, *connectStatus);
	}
	
	printf("GetDeviceInfo: rc 0x%x\n", rc);

	return(rc);
}

bool gInRC = false;

bool BLEServer::ReadCharacteristic(DWORD64 macAddr, int characteristicUUID, unsigned char *pOut, unsigned int bufSz, unsigned int *len) {
	GattCommunicationStatus status = GattCommunicationStatus::Unreachable;
	GattCharacteristic^ gattCharacteristic;
	std::map<int, GattCharacteristic^>::iterator it;
	BLEDeviceInstance^ pInst;
	HANDLE signal = CreateEvent(nullptr, true, false, nullptr);
	
	gInRC = true;

    auto mac2InstIt = mMAC2Inst.find(macAddr);
    if (mac2InstIt == mMAC2Inst.end())
		return false;

    pInst = mac2InstIt->second;

    IAsyncOperation<BluetoothLEDevice^>^ fromIdAction = BluetoothLEDevice::FromIdAsync(pInst->mDeviceIdStr);
    auto mytask = create_task(fromIdAction);
    BluetoothLEDevice^ btdev = mytask.get();
		
    if (pInst->mIsConnected()) {
		int rc;
		gattCharacteristic = pInst->getCharacteristic(characteristicUUID, &rc);
		if (rc != BT_SUCCESS) {
			printf("ReadCharacteristic: Error %d\n", rc);
			return(false);
		}

		if (len) *len = 0;

		printf("Reading Characteristic MAC %llX UUID 0x%8.8X. bufSz=%d pOut=0x%x obj %x\n", macAddr, characteristicUUID, bufSz, (unsigned int)pOut, (unsigned int)(int *)gattCharacteristic);
		
		printf("Connect status=%s\n", pInst->mGetDevObj()->ConnectionStatus == Windows::Devices::Bluetooth::BluetoothConnectionStatus::Connected ? "Connected" : "Disconnected");
		
		if (pInst->mGetDevObj()->ConnectionStatus != Windows::Devices::Bluetooth::BluetoothConnectionStatus::Connected) {
            if(!pInst->mRefresh()) {
                printf("mRefresh failed\n");
                return(false);
            }
//			IAsyncOperation<BluetoothLEDevice^>^ action = pInst->mGetDevObj()->FromIdAsync(deviceIdStr);
//			auto mytask = create_task(action);
//			mytask.then([this, signal, pInst](BluetoothLEDevice^ btdev) {
//				SetEvent(signal);
//				printf("ATTEMPTED RECONNECT %s\n", pInst->mGetDevObj()->ConnectionStatus == Windows::Devices::Bluetooth::BluetoothConnectionStatus::Connected ? "SUCCESS!" : "FAILED");
//			});
//			mytask.wait();
//			WaitForSingleObject(signal, INFINITE);
//			ResetEvent(signal);
		}

   		WaitForSingleObject(mBTSem, INFINITE);
	#if UNCACHED_MODE
   		IAsyncOperation<GattReadResult^>^ readAction = gattCharacteristic->ReadValueAsync(Windows::Devices::Bluetooth::BluetoothCacheMode::Uncached);
	#else
		IAsyncOperation<GattReadResult^>^ readAction = gattCharacteristic->ReadValueAsync(Windows::Devices::Bluetooth::BluetoothCacheMode::Cached);
	#endif
		auto rTsk = create_task(readAction);
		GattReadResult^ rStatus = rTsk.get();
		unsigned char *ptr = pOut;
		if (rStatus->Status == Windows::Devices::Bluetooth::GenericAttributeProfile::GattCommunicationStatus::Success) {
			printf("Read Status %d len %d\n", (int)rStatus->Status, rStatus->Value->Length);
			auto dr = ::Windows::Storage::Streams::DataReader::FromBuffer(rStatus->Value);
			printf("Read Characteristic data (pOut=%x, gInRC=%d): ", (unsigned int)pOut, gInRC);
			for (unsigned int x = 0; x < rStatus->Value->Length && x < bufSz; x++) {
				*ptr = dr->ReadByte();
				printf("%x ", *ptr);
				ptr++;
				*len = *len + 1;
			}
		}
		else {
			printf("Read Status Error %d\n", rStatus->Status);			
		}		
		status = rStatus->Status;
		ReleaseSemaphore(mBTSem, 1, NULL);
		gInRC = false;
		if (status == Windows::Devices::Bluetooth::GenericAttributeProfile::GattCommunicationStatus::Success)
			return true;
		else
			return false;
	}
	else return false;
}

bool BLEServer::WriteCharacteristic(DWORD64 macAddr, int characteristicUUID, unsigned char *pIn, int len) {
	GattCharacteristic^ gattCharacteristic;
	std::map<DWORD64, String^>::iterator devIdIt;
	std::map<int, GattCharacteristic^>::iterator it;
	BLEDeviceInstance^ pInst;
	HANDLE signal = CreateEvent(nullptr, true, false, nullptr);

	printf("Write Characteristic: MAC %llX UUID 0x%8.8X on target.\n", macAddr, characteristicUUID);

    auto mac2InstIt = mMAC2Inst.find(macAddr);
    if (mac2InstIt == mMAC2Inst.end()) 
        return false;
    pInst = mac2InstIt->second;
	
    if (pInst->mIsConnected()) {
		int rc;
		gattCharacteristic = pInst->getCharacteristic(characteristicUUID, &rc);
		if (rc != BT_SUCCESS) {
			printf("WriteCharacteristic:  FAILED to get characteristic, rc=%d.\n", rc);
			return(false);
		}

		DataWriter^ dw = ref new DataWriter();
		
		printf("Connect status=%s\n", pInst->mGetDevObj()->ConnectionStatus == Windows::Devices::Bluetooth::BluetoothConnectionStatus::Connected ? "Connected" : "Disconnected");

		if (pInst->mGetDevObj()->ConnectionStatus != Windows::Devices::Bluetooth::BluetoothConnectionStatus::Connected) {
            if(!pInst->mRefresh()) {
                printf("mRefresh failed\n");
                return(false);
            }
//			IAsyncOperation<BluetoothLEDevice^>^ action = pInst->mGetDevObj()->FromIdAsync(deviceIdStr);
//			auto mytask = create_task(action);
//			mytask.then([this, signal, pInst](BluetoothLEDevice^ btdev) {
//				SetEvent(signal);
//				printf("ATTEMPTED RECONNECT %s\n", pInst->mGetDevObj()->ConnectionStatus == Windows::Devices::Bluetooth::BluetoothConnectionStatus::Connected ? "SUCCESS!" : "FAILED");
//			});
//			mytask.wait();
//			WaitForSingleObject(signal, INFINITE);
//			ResetEvent(signal);
		}

		for (int x = 0; x < len; x++) {
			dw->WriteByte(*pIn);
			pIn++;
		}
		IBuffer^ ibuf = dw->DetachBuffer();

		WaitForSingleObject(mBTSem, INFINITE);
		IAsyncOperation<GattCommunicationStatus>^ writeAction = gattCharacteristic->WriteValueAsync(ibuf);
		//										writeAction = gctrstic->WriteValueAsync(ibuf, Windows::Devices::Bluetooth::GenericAttributeProfile::GattWriteOption::WriteWithoutResponse);

		auto wTsk = create_task(writeAction);
		GattCommunicationStatus wStatus = wTsk.get();
		printf("Write Characteristic status %d\n", (int)wStatus);
		ReleaseSemaphore(mBTSem, 1, NULL);
		return true;
    }
	else return false;
}

void BLEDeviceInstance::OnValueChanged(Windows::Devices::Bluetooth::GenericAttributeProfile::GattCharacteristic ^ gattChar, Windows::Devices::Bluetooth::GenericAttributeProfile::GattValueChangedEventArgs ^ valChanged) {

//	printf("OnValueChanged: len=%d\n", valChanged->CharacteristicValue->Length);

	auto dr = ::Windows::Storage::Streams::DataReader::FromBuffer(valChanged->CharacteristicValue);

	unsigned char tmpbuf[128];
	for (unsigned int x = 0; x < valChanged->CharacteristicValue->Length && x < sizeof(tmpbuf); x++) {
		unsigned char val = dr->ReadByte();
//		if (x % 16 == 0) printf("\n");
//		printf("%2.2X ", val & 0xFF);
		tmpbuf[x] = val;
	}
//	printf("\n");

	DWORD bytesWritten;
	if (!WriteFile(mDataPipeHandle, tmpbuf, valChanged->CharacteristicValue->Length, &bytesWritten, NULL)) {
		printf("BLEServer: Error %d writing to data pipe.\n", GetLastError());
	}

}


bool BLEServer::InstallCharacteristicValueChangedCallback(DWORD64 macAddr, int characteristicUUID) {
	GattCharacteristic^ gattCharacteristic;
	std::map<int, GattCharacteristic^>::iterator it;
	BLEDeviceInstance^ pInst;
	HANDLE signal = CreateEvent(nullptr, true, false, nullptr);

	printf("InstallCharacteristicValueChangedCallback: MAC %llX UUID 0x%8.8X on target.\n", macAddr, characteristicUUID);

    auto mac2InstIt = mMAC2Inst.find(macAddr);
	if (mac2InstIt == mMAC2Inst.end()) {
		printf("InstallCharacteristicValueChangedCallback: bad MAC %llX.\n", macAddr);
		return false;
	}

    pInst = mac2InstIt->second;

    if (pInst->mIsConnected()) {
		int rc;
		gattCharacteristic = pInst->getCharacteristic(characteristicUUID, &rc);
		if (rc != BT_SUCCESS) {
			printf("InstallCharacteristicValueChangedCallback: inst %x Error %d\n", pInst, rc);
			
			return(false);
		}

		printf("Installing value changed CB for instance %x\n", pInst);
		Windows::Foundation::EventRegistrationToken token = gattCharacteristic->ValueChanged += ref new Windows::Foundation::TypedEventHandler<Windows::Devices::Bluetooth::GenericAttributeProfile::GattCharacteristic ^, Windows::Devices::Bluetooth::GenericAttributeProfile::GattValueChangedEventArgs ^>(pInst, &BleSrvr::BLEDeviceInstance::OnValueChanged);
		pInst->SetValueChangedToken(token);
        pInst->mValueChangedCallbacks.push_back(token);
		return true;
    }
	else return false;
}

bool BLEServer::UnInstallCharacteristicValueChangedCallback(DWORD64 macAddr, int characteristicUUID) {
	GattCharacteristic^ gattCharacteristic;
	std::map<int, GattCharacteristic^>::iterator it;
	BLEDeviceInstance^ pInst;
	HANDLE signal = CreateEvent(nullptr, true, false, nullptr);

	printf("UnInstallCharacteristicValueChangedCallback: MAC %llX UUID 0x%8.8X on target.\n", macAddr, characteristicUUID);

    auto mac2InstIt = mMAC2Inst.find(macAddr);
	if (mac2InstIt == mMAC2Inst.end()) {
		printf("UnInstallCharacteristicValueChangedCallback: bad MAC %llX.\n", macAddr);
		return false;
	}

    pInst = mac2InstIt->second;

    if (pInst->mIsConnected()) {
		int rc;
		gattCharacteristic = pInst->getCharacteristic(characteristicUUID, &rc);
		if (rc != BT_SUCCESS) {
			printf("UnInstallCharacteristicValueChangedCallback: Error %d\n", rc);
			return(false);
		}

		gattCharacteristic->ValueChanged -= pInst->GetValueChangedToken();
        for (vector<Windows::Foundation::EventRegistrationToken>::iterator it = pInst->mValueChangedCallbacks.begin(); 
              it != pInst->mValueChangedCallbacks.end(); it++) {
			if ((*it).Equals(pInst->GetValueChangedToken())) {
                printf("Removing callback token from MAC %llX.\n", pInst->mMACAddr);
				pInst->mValueChangedCallbacks.erase(it);
            }
        }
        return true;
    }
	else return false;
}

bool BLEServer::SetCharacteristicNotification(DWORD64 macAddr, int characteristicUUID, int mode) {
	GattCharacteristic^ gattCharacteristic;
	std::map<int, GattCharacteristic^>::iterator it;
	BLEDeviceInstance^ pInst;
	HANDLE signal = CreateEvent(nullptr, true, false, nullptr);

	printf("%s: MAC %llX UUID 0x%8.8X mode %d on target.\n", __FUNCTION__, macAddr, characteristicUUID, mode);

    auto mac2InstIt = mMAC2Inst.find(macAddr);
	if (mac2InstIt == mMAC2Inst.end()) {
		printf("%s: bad MAC %llX.\n", __FUNCTION__, macAddr);
		return false;
	}

    pInst = mac2InstIt->second;

    if (pInst->mIsConnected()) {
		int rc;
		gattCharacteristic = pInst->getCharacteristic(characteristicUUID, &rc);
		if (rc != BT_SUCCESS) return(false);

		IAsyncOperation<GattWriteResult^ >^ enableAction;
		if(mode) {
			enableAction = gattCharacteristic->WriteClientCharacteristicConfigurationDescriptorWithResultAsync(Windows::Devices::Bluetooth::GenericAttributeProfile::GattClientCharacteristicConfigurationDescriptorValue::Notify);
            pInst->mActiveNotificationUUIDs.push_back(characteristicUUID);
        }
		else {
			enableAction = gattCharacteristic->WriteClientCharacteristicConfigurationDescriptorWithResultAsync(Windows::Devices::Bluetooth::GenericAttributeProfile::GattClientCharacteristicConfigurationDescriptorValue::None);
            for (vector<int>::iterator it = pInst->mActiveNotificationUUIDs.begin(); it != pInst->mActiveNotificationUUIDs.end(); it++) {
                if (*it == characteristicUUID) {
                    printf("Removing UUID %d from Active Notifications.\n", characteristicUUID);
					pInst->mActiveNotificationUUIDs.erase(it);
                    break;
                }
            }
        }
		
		auto wTsk = create_task(enableAction);

		wTsk.then([this, signal](GattWriteResult^ gwResult) {
			printf("%s: result %d\n", __FUNCTION__, gwResult->Status);
			SetEvent(signal);
		});
		wTsk.wait();
		WaitForSingleObject(signal, INFINITE);

		return true;
    }
	else return false;
}


void BLEServer::Pair() {

}

/*

TypedEventHandler<DeviceInformationCustomPairing, DevicePairingRequestedEventArgs> PairingRequested
*/
void BLEServerCallback::PairingRequestedHandler(DeviceInformationCustomPairing^ sender, DevicePairingRequestedEventArgs^ args)
{
	printf("PairingRequestedHandler:\n");
    switch (args->PairingKind)
    {
	case DevicePairingKinds::ConfirmOnly:
		printf("PairingRequestedHandler: ConfirmOnly.\n");
        // Windows itself will pop the confirmation dialog as part of "consent" if this is running on Desktop or Mobile
        // If this is an App for 'Windows IoT Core' where there is no Windows Consent UX, you may want to provide your own confirmation.
        args->Accept();
        break;

    case DevicePairingKinds::DisplayPin:
		printf("PairingRequestedHandler: DisplayPIN.\n");
        // We just show the PIN on this side. The ceremony is actually completed when the user enters the PIN

        // on the target device. We automatically accept here since we can't really "cancel" the operation

        // from this side.

        args->Accept();
#if 0
        Dispatcher->RunAsync(CoreDispatcherPriority::Normal, ref new DispatchedHandler(
            [this, args]()
        {
            ShowPairingPanel("Please enter this PIN on the device you are pairing with: " + args->Pin, args->PairingKind);
        }));
#endif
        break;

    case DevicePairingKinds::ProvidePin:
        {
			printf("PairingRequestedHandler: ProvidePIN.\n");
            // A PIN may be shown on the target device and the user needs to enter the matching PIN on 

            // this Windows device. Get a deferral so we can perform the async request to the user.

			args->Accept("123114");
            //auto collectPinDeferral = args->GetDeferral();
#if 0
            Dispatcher->RunAsync(CoreDispatcherPriority::Normal, ref new DispatchedHandler(
                [this, collectPinDeferral, args]()
            {
                GetPinFromUserAsync().then([this, collectPinDeferral, args](String^ pin)
                {
                    if (pin != nullptr)
                    {
                        args->Accept(pin);
                    }
                    collectPinDeferral->Complete();
                }, task_continuation_context::use_current());
            }));
#endif
        }
        break;

    case DevicePairingKinds::ConfirmPinMatch:
        {
			printf("PairingRequestedHandler: ConfirmPinMatch.\n");
			// We show the PIN here and the user responds with whether the PIN matches what they see

            // on the target device. Response comes back and we set it on the PinComparePairingRequestedData

            // then complete the deferral.

            auto displayMessageDeferral = args->GetDeferral();
#if 0
            Dispatcher->RunAsync(CoreDispatcherPriority::Normal, ref new DispatchedHandler(
                [this, displayMessageDeferral, args]()
            {
                GetUserConfirmationAsync(args->Pin).then([this, displayMessageDeferral, args](bool accept)
                {
                    if (accept)
                    {
                        args->Accept();
                    }
                    displayMessageDeferral->Complete();
                }, task_continuation_context::use_current());
            }));
#endif
        }
        break;
    }
}

bool GetDeviceMACFromId(String^ id, unsigned char *macAdr) {

	String^ mystr = id;
	wchar_t *ptr = (wchar_t*)mystr->Data();
	ptr = wcsstr(ptr, L"BluetoothLE#BluetoothLE");
	if (ptr) {
		ptr += strlen("BluetoothLE#BluetoothLE");		// Get past driver nanme
		ptr = wcsstr(ptr, L"-");						// Get to the dash, device MAC address is after it
		ptr++;											// Get to device MAC
		
		unsigned int macAdrInt[6];
		int cnt = swscanf(ptr, L"%2x:%2x:%2x:%2x:%2x:%2x", &macAdrInt[5], &macAdrInt[4], &macAdrInt[3], &macAdrInt[2], &macAdrInt[1], &macAdrInt[0]);
		if (cnt == 0) 
			return false;
		else
			printf("Device MAC %02.02x:%02.02x:%02.02x:%02.02x:%02.02x:%02.02x\n", macAdrInt[5], macAdrInt[4], macAdrInt[3], macAdrInt[2], macAdrInt[1], macAdrInt[0]);

		for (int x = 0; x < 6; x++)
			macAdr[x] = (unsigned char)macAdrInt[x];	// Copy the MAC to user buffer

		return true;
	}
	return false;
}


unsigned __int64 MACAddrFromString(unsigned char *macAdr) {
	unsigned __int64 macAddr = 0;

	for (int x = 0; x < 6; x++)
		macAddr |= (unsigned __int64)macAdr[x] << x*8;

    //printf("MACAddrFromString: result %llx\n", macAddr);
    return(macAddr);
}


boolean BLEServer::OpenDevice(DeviceInformation^ devinfo) {
	bool bumpDeviceCnt = false;
	String^ deviceIdStr = devinfo->Id;
    std::map<String^,DeviceInformation^>::iterator devId2DevInfoIt;
    unsigned char macAddr[6];
    bool deviceFound = false;

    // Is MAC address in our list?

    if(GetDeviceMACFromId(deviceIdStr, &macAddr[0])) {
        for (unsigned int x = 0; x < gAllowedMACS.size(); x++) {
            if( gAllowedMACS[x] == MACAddrFromString(&macAddr[0]) ) {
                deviceFound = true;
            }
        }
        if (deviceFound) {
            printf("Device allowed: %llx\n", MACAddrFromString(&macAddr[0]));
        }
        else {
            printf("Device %llx not in list.\n", MACAddrFromString(&macAddr[0]));
            return false;
        }
    }


    devId2DevInfoIt = mDeviceId2DevInfo.find(deviceIdStr);
    if (devId2DevInfoIt != mDeviceId2DevInfo.end()) 
        return(false);                              // Device already exists

    mDeviceId2DevInfo[deviceIdStr] = devinfo;

	if (deviceIdStr) {
		NotifyUser("TechMah IMU Rev1.00 found: Id = " + deviceIdStr);

		BluetoothDeviceId^ btid = BluetoothDeviceId::FromId(deviceIdStr);
		char *str = btid->IsLowEnergyDevice ? "Low Energy" : "Classic";
		printf("Device gIdx %d type %s\n", gIdx, str);

		IAsyncOperation<BluetoothLEDevice^>^ fromIdAction = BluetoothLEDevice::FromIdAsync(deviceIdStr);
		auto mytask = create_task(fromIdAction);
		BluetoothLEDevice^ btdev = mytask.get();
		//mytask.S
		//if ((unsigned int)btdev == 0)
		//	return(false);
		
		std::map<String^, BLEDeviceInstance^>::iterator it;
		it = mDeviceId2BLEDevInst.find(deviceIdStr);
		printf("Waiting for connect:\n");

		bool connected = false;
		printf("BLE device access status: %d (1=Allowed).\n", btdev->DeviceAccessInformation->CurrentStatus);
		if (btdev->ConnectionStatus != Windows::Devices::Bluetooth::BluetoothConnectionStatus::Connected) {
			printf("BLE Device not connected, status = %d.\n", btdev->ConnectionStatus);				
            printf("BLE Device IsPaired %d CanPair %d.\n", btdev->DeviceInformation->Pairing->IsPaired,
                   btdev->DeviceInformation->Pairing->CanPair);

            if(!btdev->DeviceInformation->Pairing->IsPaired && btdev->DeviceInformation->Pairing->CanPair) {
                
				printf("OpenDevice: Attempting to pair device.\n");

//				btdev->DeviceInformation->Pairing->Custom->PairingRequested += ref new TypedEventHandler<DeviceInformationCustomPairing^, DevicePairingRequestedEventArgs^>(this, &BLEServer::PairingRequestedHandler);
//                btdev->DeviceInformation->Pairing->Custom->PairingRequested += ref new TypedEventHandler<DeviceInformationCustomPairing^, DevicePairingRequestedEventArgs^>(this, &BLEServerCallback::PairingRequestedHandler);
				this->mpPairingCallback->mInstallCallback(btdev);

				for (int attempt = 0; attempt < 3; attempt++) {
					IAsyncOperation<DevicePairingResult^>^ pr = btdev->DeviceInformation->Pairing->Custom->PairAsync(Windows::Devices::Enumeration::DevicePairingKinds::ProvidePin);
					auto pairTask = create_task(pr);
					DevicePairingResult^ dpr = pairTask.get();
					printf("OpenDevice: Device Pairing Result status %d (0=paired).\n", dpr->Status);
					if (dpr->Status != Windows::Devices::Enumeration::DevicePairingResultStatus::Paired) {    // Only return false here if pair fails
						if (attempt == 3 - 1) return false;
						else printf("Retrying pair:\n");
					}
					else break;
				}
                fromIdAction = BluetoothLEDevice::FromIdAsync(deviceIdStr); // Initiate a connect
                mytask = create_task(fromIdAction);
                btdev = mytask.get();
                if (btdev->ConnectionStatus == Windows::Devices::Bluetooth::BluetoothConnectionStatus::Connected)
                    connected = true;
            }
		}
		else connected = true;
			
		BLEDeviceInstance^ pInst;
		if (it == mDeviceId2BLEDevInst.end()) {
			pInst = ref new BLEDeviceInstance(deviceIdStr, btdev, connected, gIdx);
			unsigned char macAddr[6];
			pInst->GetDeviceMACFromId(deviceIdStr, &macAddr[0]);
			pInst->mSetMACAddr(&macAddr[0]);
			mDeviceId2BLEDevInst[deviceIdStr] = pInst;
			printf("Initializing instance 0x%x MAC %llX Idx %d\n", (unsigned int)(int *)pInst, pInst->mMACAddr, gIdx);
			NotifyUser("Device ID = " + deviceIdStr);
			mIdx2DeviceId[gIdx] = deviceIdStr;				
			mIdx2Inst[gIdx] = pInst;
            mMAC2Inst[pInst->mMACAddr] = pInst;
			bumpDeviceCnt = true;
			gIdx++;
		}
		else {
			pInst = it->second;
		}

        if(pInst->mIsConnected()) printf("BLE Device %d Connected.\n", pInst->mGetInstance());						
        else printf("***BLE Device %d ***NOT*** Connected.***\n", pInst->mGetInstance());						

		printf("Device %d connect %d BluetoothAddress=0x%llx\n", pInst->mGetInstance(), btdev->ConnectionStatus, pInst->mGetDevObj()->BluetoothAddress);
//		gBTAddress = pInst->mGetDevObj()->BluetoothAddress;
#if UNCACHED_MODE
		IAsyncOperation<GattDeviceServicesResult^>^ getGattServicesAction = pInst->mGetDevObj()->GetGattServicesAsync(Windows::Devices::Bluetooth::BluetoothCacheMode::Uncached);
#else
		IAsyncOperation<GattDeviceServicesResult^>^ getGattServicesAction = pInst->mGetDevObj()->GetGattServicesAsync(Windows::Devices::Bluetooth::BluetoothCacheMode::Cached);
#endif
		auto gsTsk = create_task(getGattServicesAction);
        GattDeviceServicesResult ^ gsRslt;
        try {
            gsRslt = gsTsk.get();
        } 
        catch (task_canceled tc) {
            printf("GetGattServicesAsync() task was canceled.\n");
        } 
        catch (COMException^ ce) {
            printf("GetGattServicesAsync() COM exception occured.\n");
        } 
        catch (Exception^ e)
//        catch (AggregateException ae) 
		{
                printf("GetGattServicesAsync() call exception!\n");
        }
		Guid guid;
//		HANDLE signal = CreateEvent(nullptr, true, false, nullptr);
		if (gsRslt->Status==GattCommunicationStatus::Success) {
			if (pInst->mGetDevObj()->ConnectionStatus == Windows::Devices::Bluetooth::BluetoothConnectionStatus::Connected) {
				printf("BLE Device %d Connected.\n", pInst->mGetInstance());
				pInst->mSetConnected(true);
			}

			LPOLESTR str;
			str = SysAllocString(L"{00000000-0000-0000-0000-000000000000} ");
			int strsz = wcslen(str);
			char cstr[256];
			memset(cstr, 0, sizeof(cstr));
			int serviceCnt = gsRslt->Services->Size;
			printf("Service Cnt=%d\n", serviceCnt);
					
			for (int x = 0; x < serviceCnt; x++) {
printf("Service idx %d\n", x);
				GattDeviceService^ service;
				guid = gsRslt->Services->GetAt(x)->Uuid;
				if (StringFromGUID2(guid, str, strsz)) {
					int bw = wcstombs(cstr, str, sizeof(cstr));
					printf("service %d: guid %s guidlen %d\n", x, cstr, bw);							
				}
				else
					printf("Buffer sz %d too small 1!!\n", strsz);

				GattCharacteristic^ gctrstic;
				IVectorView<GattCharacteristic^>^ gattCharList;
				int characteristicCnt;							
													
				service = gsRslt->Services->GetAt(x);
				DeviceAccessStatus status = service->DeviceAccessInformation->CurrentStatus;
#if UNCACHED_MODE
				IAsyncOperation<GattCharacteristicsResult^>^ getCharsAction = service->GetCharacteristicsAsync(Windows::Devices::Bluetooth::BluetoothCacheMode::Uncached);
#else
				IAsyncOperation<GattCharacteristicsResult^>^ getCharsAction = service->GetCharacteristicsAsync(Windows::Devices::Bluetooth::BluetoothCacheMode::Cached);
#endif
				auto gcTsk = create_task(getCharsAction);
				GattCharacteristicsResult^ gcResult = gcTsk.get();

				if(gcResult->Status == Windows::Devices::Bluetooth::GenericAttributeProfile::GattCommunicationStatus::Success)
					gattCharList = gcResult->Characteristics;

				if (gcResult) printf(" gcResult->Status %d\n", gcResult->Status);
				characteristicCnt = 0;
				if(gcResult && gattCharList && gcResult->Status == Windows::Devices::Bluetooth::GenericAttributeProfile::GattCommunicationStatus::Success)
					characteristicCnt = gattCharList->Size;						
												
				printf("Service %d Characteristic cnt=%d\n", x, characteristicCnt);
				for (int y = 0; y < characteristicCnt; y++) {
					gctrstic = gattCharList->GetAt(y);
					guid = gctrstic->Uuid;
							
					if (StringFromGUID2(guid, str, strsz)) {								
						wcstombs(cstr, str, sizeof(cstr));
						//printf("service %d characteristic #%d: UUID=%s Name=%s\n", x, y, cstr, GetCharacteristicName(cstr));
						int characteristicUUID = GetCharacteristicType(cstr);
						//printf("characteristicUUID=0x%x idx=%x\n", characteristicUUID, pInst->mGetInstance());								
						pInst->mAddCharacteristic(characteristicUUID, gctrstic);
							switch(characteristicUUID) {
								case CHARACTERISTIC_MODEL_NUMBER_UUID:
								case CHARACTERISTIC_SERIAL_NUMBER_UUID:
								case CHARACTERISTIC_FIRMWARE_REVISION_UUID:
								case CHARACTERISTIC_HARDWARE_REVISION_UUID:
								case CHARACTERISTIC_SOFTWARE_REVISION_UUID:
								case CHARACTERISTIC_MANUFACTURER_NAME_UUID:
									printf("%s: ", GetCharacteristicNameFromType(characteristicUUID));												   
									{
#if UNCACHED_MODE
										IAsyncOperation<GattReadResult^>^ readAction = gctrstic->ReadValueAsync(Windows::Devices::Bluetooth::BluetoothCacheMode::Uncached);
#else
										IAsyncOperation<GattReadResult^>^ readAction = gctrstic->ReadValueAsync(Windows::Devices::Bluetooth::BluetoothCacheMode::Cached);
#endif
										auto rTsk = create_task(readAction);
                                        GattReadResult^ readResult = rTsk.get();
											char tmpBuff[128];
											char *cptr = tmpBuff;
											if (readResult->Status == Windows::Devices::Bluetooth::GenericAttributeProfile::GattCommunicationStatus::Success) {
												auto dr = ::Windows::Storage::Streams::DataReader::FromBuffer(readResult->Value);
												for (unsigned int x = 0; x < readResult->Value->Length && x < sizeof(tmpBuff)-1; x++)
													*cptr++ = dr->ReadByte();
												*cptr = 0;
												printf("%s\n", tmpBuff);
											}
											else printf("Read value failed, status = %d.\n", readResult->Status);
									}
									break;									

                                default:
                                    printf("service %d characteristic #%d: UUID=%s Name=%s\n", x, y, cstr, GetCharacteristicName(cstr));
                                    printf("characteristicUUID=0x%x instance=%x\n", characteristicUUID, pInst->mGetInstance());
									break;
							}
						}
						else
							printf("Buffer sz %d too small 2!!\n", strsz);
					}
printf("End Service %d\n", x);
				}
				SysFreeString(str);

				if (bumpDeviceCnt) gDeviceCnt++;
printf("bumpDeviceCnt %d gDeviceCnt %d\n", bumpDeviceCnt, gDeviceCnt);

#if 0
printf("Installing Default ValueChanged callback for idx %x.\n", pInst->mGetInstance());
InstallCharacteristicValueChangedCallback(pInst->mGetInstance(), 0xFFF4);	// Install Callback and notifications for 0xFFF4
printf("Enabling Default notification for idx %d.\n", pInst->mGetInstance());
SetCharacteristicNotification(pInst->mGetInstance(), 0xFFF4, 1);
#endif
			}
			else {
				char errstr[128];
				switch (gsRslt->Status) {
					case 0:
						strcpy(errstr, "Success");
					case 1:
						strcpy(errstr, "Unreachable");
						break;
					case 2:
						strcpy(errstr, "Protocol error");
						break;
					case 3:
						strcpy(errstr, "Access Denied");
						break;
					default:
						strcpy(errstr, "Unknown");

					}
					printf("GetGattServicesAsync failed: GattCommunicationStatus = %d (%s).\n", gsRslt->Status, errstr);
                    printf("OpenDevice exit.\n");
                    return(false);
			}

		printf("BLE Device status = %d.\n", btdev->ConnectionStatus);		
	}

	printf("OpenDevice exit.\n");
	return true;
}

bool BLEDeviceInstance::GetDeviceMACFromId(String^ id, unsigned char *macAdr) {

	String^ mystr = id;
	wchar_t *ptr = (wchar_t*)mystr->Data();
	ptr = wcsstr(ptr, L"BluetoothLE#BluetoothLE");
	if (ptr) {
		//wchar_t btControllerMACStr[128];
		ptr += strlen("BluetoothLE#BluetoothLE");		// Get past driver nanme
		//wcsncpy(&btControllerMACStr[0], ptr, 12 + 5);	// Copy controller MAC address string,  12 characters, 5 colons
		ptr = wcsstr(ptr, L"-");						// Get to the dash, device MAC address is after it
		//btControllerMACStr[12 + 5] = 0;				// Terminate the controller MAC address
		ptr++;											// Get to device MAC
		
		unsigned int macAdrInt[6];
		int cnt = swscanf(ptr, L"%2x:%2x:%2x:%2x:%2x:%2x", &macAdrInt[5], &macAdrInt[4], &macAdrInt[3], &macAdrInt[2], &macAdrInt[1], &macAdrInt[0]);
		if (cnt == 0) 
			return false;
		else
			printf("Device MAC %02.02x:%02.02x:%02.02x:%02.02x:%02.02x:%02.02x\n", macAdrInt[5], macAdrInt[4], macAdrInt[3], macAdrInt[2], macAdrInt[1], macAdrInt[0]);

		for (int x = 0; x < 6; x++)
			macAdr[x] = (unsigned char)macAdrInt[x];	// Copy the MAC to user buffer

		return true;
	}
	return false;
}


void BLEServer::EnumerateBLE() {

	mWatcherStopped = false;
	mEnumComplete = false;
	mEnumStopping = false;

	String^ selector;
//	selector = "(System.Devices.Aep.ProtocolId:={bb7bb05e-5972-42b5-94fc-76eaa7084d49}) AND (System.Devices.Aep.CanPair:=System.StructuredQueryType.Boolean#True OR System.Devices.Aep.IsPaired:=System.StructuredQueryType.Boolean#True)"; // ~works
	selector = "(System.Devices.Aep.ProtocolId:={bb7bb05e-5972-42b5-94fc-76eaa7084d49})";
	//selector = BluetoothLEDevice::GetDeviceSelector();

	// Kind is specified in the selector info

	deviceWatcher = DeviceInformation::CreateWatcher(selector, nullptr, (DeviceInformationKind)5);  // 5, Association endpoint
	// Hook up handlers for the watcher events before starting the watcher

	handlerAdded = ref new TypedEventHandler<DeviceWatcher^, DeviceInformation^>([this](DeviceWatcher^ sender, DeviceInformation^ deviceInfo) {
		if (deviceInfo) {			
			if (deviceInfo->Name == "TechMah IMU Rev1.00") {
				printf("\n");
				NotifyUser("Add: " + deviceInfo->Id);
				
				if (OpenDevice(deviceInfo)) {
					printf("OpenDevice Success!\n\n");
					if (gDeviceCnt >= 2) {
	//					mEnumStopping = true;
	//					deviceWatcher->Stop();
					}
				}
			}
		}
	});

	handlerAddedToken = deviceWatcher->Added += handlerAdded;

	handlerUpdated = ref new TypedEventHandler<DeviceWatcher^, DeviceInformationUpdate^>([this](DeviceWatcher^ sender, DeviceInformationUpdate^ deviceInfoUpdate) {

		NotifyUser("UpdatedHandler : " + deviceInfoUpdate->Id);
		
		std::map<String^, BLEDeviceInstance^>::iterator devIt;
		std::map<String^, DeviceInformation^>::iterator devInfoIt;
		printf("Looking for device instance.\n");
		if ((devIt = mDeviceId2BLEDevInst.find(deviceInfoUpdate->Id)) == mDeviceId2BLEDevInst.end()) {
			printf("Instance not found, Looking for DeviceInfo.\n");			
			if((devInfoIt = mDeviceId2DevInfo.find(deviceInfoUpdate->Id)) == mDeviceId2DevInfo.end()) {
				NotifyUser("No Instance or DeviceInfo found for Id: " + deviceInfoUpdate->Id);
				// Got update but device not found, lets open it.
				IAsyncOperation<DeviceInformation^>^ action = DeviceInformation::CreateFromIdAsync(deviceInfoUpdate->Id);
				auto mytask = create_task(action);
				DeviceInformation^ deviceInfo = mytask.get();
				NotifyUser("Attempting open of: " + deviceInfoUpdate->Id);
				if(OpenDevice(deviceInfo)) {
					printf("OpenDevice (from update) Success!\n");
					if (gDeviceCnt >= 2) {
	//					mEnumStopping = true;
	//					deviceWatcher->Stop();
					}
				}
			}
		}
		else {
            printf("Device instance found.\n");

			// Update case
			BLEDeviceInstance^ pInst;
			pInst = devIt->second;

			// Find the corresponding updated DeviceInformation in the collection and pass the update object
			// to the Update method of the existing DeviceInformation. This automatically updates the object
			// for us.
			if ((devInfoIt = mDeviceId2DevInfo.find(deviceInfoUpdate->Id)) != mDeviceId2DevInfo.end()) {
				DeviceInformation^ devInf = devInfoIt->second;
				devInf->Update(deviceInfoUpdate);
			}
		}
	} );

	handlerUpdatedToken = deviceWatcher->Updated += handlerUpdated;

	handlerRemoved = ref new TypedEventHandler<DeviceWatcher^, DeviceInformationUpdate^>([this](DeviceWatcher^ sender, DeviceInformationUpdate^ deviceInfoUpdate) {
		std::map<String^, BLEDeviceInstance^>::iterator devIt;
		std::map<String^, DeviceInformation^>::iterator devInfoIt;

		if ((devIt = mDeviceId2BLEDevInst.find(deviceInfoUpdate->Id)) != mDeviceId2BLEDevInst.end()) {
			NotifyUser("RemovedHandler: " + deviceInfoUpdate->Id);
			BLEDeviceInstance^ pInst = devIt->second;
            pInst->mRefresh(); // Never disconnect..
//			int idx = pInst->mGetIdx();
//			mIdx2Inst.erase(idx);
//			mIdx2DeviceId.erase(idx);
//			mDeviceId2BLEDevInst.erase(devIt);
		}
//        if((devInfoIt = mDeviceId2DevInfo.find(deviceInfoUpdate->Id)) != mDeviceId2DevInfo.end()){
//			mDeviceId2DevInfo.erase(devInfoIt);
//        }
//		uint32 index = 0;
	} );

	handlerRemovedToken = deviceWatcher->Removed += handlerRemoved;

	handlerEnumCompleted = ref new TypedEventHandler<DeviceWatcher^, Object^>([this](DeviceWatcher^ sender, Object^ obj) {

						NotifyUser("EnumCompleteHandler: ");	
						this->mEnumComplete = true;
	}
	);

	handlerEnumCompletedToken = deviceWatcher->EnumerationCompleted += handlerEnumCompleted;

	handlerStopped = ref new TypedEventHandler<DeviceWatcher^, Object^>([this](DeviceWatcher^ sender, Object^ obj) {
					NotifyUser("StoppedHandler: ");		

					this->mEnumComplete = true;
					mWatcherStopped = true;
					sender->Added -= handlerAddedToken;
					sender->Updated -= handlerUpdatedToken;
					sender->Removed -= handlerRemovedToken;
					sender->EnumerationCompleted -= handlerEnumCompletedToken;
					sender->Stopped -= handlerStoppedToken;		

	}
	);

	handlerStoppedToken = deviceWatcher->Stopped += handlerStopped;
	
	deviceWatcher->Start();

	printf("Scanning devices:\n");
	int loopcnt = 0;
    while (!mEnumComplete && gDeviceCnt < 2)
		Sleep(250);
//	printf("Waiting for enumeration stop:\n");
//	if(!mWatcherStopped && !mEnumStopping)
//		deviceWatcher->Stop();
//	while (!mWatcherStopped)
//		Sleep(250);

	printf("%d Device(s) Found!\n", gDeviceCnt);
}

/*
Windows::Foundation::TypedEventHandler<Windows::Devices::Bluetooth::BluetoothLEDevice^, Object^>^ BLEDeviceInstance::connectStatusChanged(BluetoothLEDevice^ btDev, Object^ obj) {
	printf("BluetoothLEDevice connect status change:\n");
}
*/	

void NotifyUser(String^ strMessage);

void NotifyUser(String^ strMessage) {

	std::wstring str1(strMessage->Data());
	printf("BLEServer: %ws\n", str1.c_str());

}