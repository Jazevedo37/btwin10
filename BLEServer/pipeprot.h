
#ifndef _PIPEPROT_H_
#define _PIPEPROT_H_

#include <windows.h>

#define WINDOWS_10_PORT         0

            
typedef enum BTPipeMsgId {
    BT_SCAN,
    BT_DEVICE_INFO,
    BT_SET_LED,
    BT_SET_MAC_ADDRESSES,
    BT_READ_CHARACTERISTIC,
    BT_WRITE_CHARACTERISTIC,
    BT_ENABLE_NOTIFICATION,
    BT_DISABLE_NOTIFICATION,
    BT_DISCONNECTING_DATA_PIPE,
    BT_DISCONNECTING_CMD_PIPE,
    BT_KEEP_ALIVE,
	BT_INVALID
} BTPipeMsgId;

typedef enum BTPipeMsgReturnCode {
    BT_SUCCESS,
    BT_FAIL,
    BT_COMM_ERR
} BTPipeMsgReturnCode;

#define MAX_BT_MSGBUF_SZ   512


typedef struct BTPipeMessage {
    BTPipeMsgId			msgId;
    BTPipeMsgReturnCode	rc;
//    unsigned int        idx;        // Instance index
    DWORD64             macAddr;    // Device MAC address, also used as device handle
    unsigned int        bufSz;      // Size of users return buffer
    unsigned int        bufLen;     // Length of data in buffer
    unsigned int		param[16];
    unsigned char		buffer[MAX_BT_MSGBUF_SZ];
} BTPipeMessage;


typedef struct BluetoothBLESrvrCmd {
    bool dispatched;        // Indicates command has been sent to server
    bool asyncReadStarted;  // Indicates ReafFile has been called (asynchronously)
    OVERLAPPED mOverlapped;
    bool triggerEvent;       // True to trigger event
    HANDLE responseEvent;
    BTPipeMessage *ptr;
    BTPipeMessage msg;
/*    
    struct {
        BTPipeMsgId			msgId;
        BTPipeMsgReturnCode	rc;
        unsigned int        bufSz;      // Size of users return buffer
        unsigned int        bufLen;     // Length of data in buffer
        unsigned int		param[16];  //
                                        // Returns: param[0] - # of devices detected
                                        //          param[1] - MAC address of 1st device found
                                        //          param[2] - lower 2 bytes of MAC (in MSBs)
                                        //          param[3] - MAC address of 2nd device found
                                        //          param[4] - lower 2 bytes of MAC (in MSBs)
        unsigned char		buffer[MAX_BT_MSGBUF_SZ];
                                        //          buffer[0] - Index of first device
                                        //          ..
                                        //          buffer[n] - Device index N of device N
    } btScan;

    struct {
        BTPipeMsgId			msgId;
        BTPipeMsgReturnCode	rc;
        unsigned int        bufSz;      // Size of users return buffer
        unsigned int        bufLen;     // Length of data in buffer
        unsigned int		param[16];
        unsigned char		buffer[MAX_BT_MSGBUF_SZ];
    } btReadCharacteristic;

    struct {
        BTPipeMsgId			msgId;
        BTPipeMsgReturnCode	rc;
        unsigned int        bufSz;      // Size of users return buffer
        unsigned int        bufLen;     // Length of data in buffer
        unsigned int		param[16];
        unsigned char		buffer[MAX_BT_MSGBUF_SZ];
    } btWriteCharacterisitic;
*/
} BluetoothBLESrvrCmd;

extern HANDLE gCmdPipeHandle;
extern HANDLE gDataPipeHandle;

class BluetoothSrvrPipe {

public:
    BluetoothSrvrPipe() {};
    ~BluetoothSrvrPipe() {};

    bool open(HANDLE *pCmdPipeHandle) {
        if ((*pCmdPipeHandle = CreateFile(L"\\\\.\\pipe\\btserver", GENERIC_READ | GENERIC_WRITE,
                                                           FILE_SHARE_READ | FILE_SHARE_WRITE,
                                                           NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE) {
            if (!WaitNamedPipe(L"\\\\.\\pipe\\btserver", NMPWAIT_USE_DEFAULT_WAIT))
            {
                printf("Could not open BLEServer Command pipe.\n"); 
                return false;
            }
        }                      

        printf("CMD Pipe 0x%x opened.\n", (unsigned int)*pCmdPipeHandle);
        return true; 
    };

    bool close(HANDLE pipeHandle) { 
        CloseHandle(pipeHandle);
        return true; 
    };
};


BTPipeMsgReturnCode SendPipeRequest(HANDLE pipeHandle, const BTPipeMessage *msg);
//BTPipeMsgReturnCode SendPipeRequest2(BluetoothController& ctrlr, const BTPipeMessage *msg);
BTPipeMsgReturnCode SendPipeRequestAsync(HANDLE pipeHandle, const BTPipeMessage *msg);
BTPipeMsgReturnCode GetPipeRequestResponse(HANDLE pipeHandle, const BTPipeMessage *msg, OVERLAPPED *pOverlapped);

char *formatMAC(unsigned long long mac, char *ptr, int len);

extern HANDLE gPipeHandle;

#endif
