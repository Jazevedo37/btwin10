
#pragma once


const int maxDevices = 16;


namespace BleSrvr
{
	public ref class BLEDeviceInstance sealed {
		friend class BLEServer;
        int idx;
        bool mOpen;
		volatile bool mConnected;	
		Windows::Foundation::EventRegistrationToken mValueChangedToken;
		BluetoothLEDevice^ btLEDevObj;
		HANDLE mDataPipeHandle;
		String^ mDeviceIdStr;
		DWORD64 mMACAddr;		        // MAC address of device

		std::map<int, GattCharacteristic^> mUUID2CharacteristicMap;		// UUID to GattCharacteristic map

        Windows::Foundation::TypedEventHandler<Windows::Devices::Bluetooth::BluetoothLEDevice^, Object^>^ connectStatusChange;
        vector<int> mActiveNotificationUUIDs;
        vector<Windows::Foundation::EventRegistrationToken> mValueChangedCallbacks;

		~BLEDeviceInstance() {
			CloseHandle(mDataPipeHandle);
		}

	public:
		BLEDeviceInstance(String^ deviceIdStr, BluetoothLEDevice ^btdev, bool connected, int instance) {
			idx = instance;
			mOpen = false;
			btLEDevObj = btdev;
			mConnected = connected;
			mOpen = true;
            mDeviceIdStr = deviceIdStr;

            unsigned char macAddr[6];
			GetDeviceMACFromId(deviceIdStr, &macAddr[0]);
			mSetMACAddr(&macAddr[0]);

			std::wstring mystr(deviceIdStr->Data());
			printf("BLEDeviceInstance constructor: %ws\n", mystr.c_str());

			char tmpstr[128];
			sprintf(tmpstr, "\\\\.\\pipe\\btserver-data%llx", mMACAddr);
			printf("Opening Data Pipe: %s\n", tmpstr);
			if ((mDataPipeHandle = CreateNamedPipeA((LPCSTR)tmpstr, PIPE_ACCESS_DUPLEX, PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT | PIPE_NOWAIT,
                PIPE_UNLIMITED_INSTANCES,
//				1, // 1 instance
                2048, 2048, 0, NULL)) == INVALID_HANDLE_VALUE) {
				printf("\nPipe creation failed for instance %d reason %d.", instance, GetLastError());				
			}
            OVERLAPPED dataPipeAsync;
			memset(&dataPipeAsync, 0, sizeof(OVERLAPPED));
			ConnectNamedPipe(mDataPipeHandle, &dataPipeAsync);
			printf("Pipe: %s ready.\n", tmpstr);

            connectStatusChange = ref new TypedEventHandler<Windows::Devices::Bluetooth::BluetoothLEDevice^, Object^>([this](BluetoothLEDevice^ btDev, Object^ obj) {
                mConnected = btDev->ConnectionStatus == Windows::Devices::Bluetooth::BluetoothConnectionStatus::Connected ? true : false;
                printf("BluetoothLEDevice connect status change: Idx %d connectStat %d\n", this->idx, mConnected );
            });
            btdev->ConnectionStatusChanged += connectStatusChange;
		}

		GattCharacteristic^ getCharacteristic(int uuid, int *rc) {
			std::map<int, GattCharacteristic^>::iterator it;
			it = mUUID2CharacteristicMap.find(uuid);
			if (it == mUUID2CharacteristicMap.end()) {
				printf("getCharacteristic: FAIL for UUID 0x%x\n", uuid);
				*rc = BT_FAIL;
				//return((GattCharacteristic^)(int *)NULL);
			}
			else {
				*rc = BT_SUCCESS;
				return(it->second);
			}
		}

		void mSetMACAddr(unsigned char *macAdr) {
			mMACAddr = 0;
			for (int x = 0; x < 6; x++)
				mMACAddr |= (DWORD64)macAdr[x] << (x*8);
		}

		void mGetMACAddr(unsigned char *macAdr) {
			for (int x = 0; x < 6; x++)
				macAdr[x] = ((mMACAddr >> (5-x)*8) & 0xFF);
		}

		BluetoothLEDevice^ mGetDevObj() {
			return btLEDevObj;
		}

        BluetoothLEDevice^ mRefresh() {
			std::wstring mystr(mDeviceIdStr->Data());
			printf("**** BLEDeviceInstance::mRefresh for %ws\n", mystr.c_str());
            IAsyncOperation<BluetoothLEDevice^>^ fromIdAction = BluetoothLEDevice::FromIdAsync(mDeviceIdStr);
            auto mytask = create_task(fromIdAction);
            btLEDevObj = mytask.get();
			printf("**** BLEDeviceInstance::mRefresh obj %8.8x for %ws\n", btLEDevObj, mystr.c_str());
			if(btLEDevObj->ConnectionStatus != Windows::Devices::Bluetooth::BluetoothConnectionStatus::Connected)
				printf("Reconnect FAILED\n");
			return(btLEDevObj);
        }

		bool mIsConnected() {
			return mConnected;
		}

		void mSetConnected(bool connected) {
			mConnected = connected;
		}

		int mGetInstance() {
			return idx;
		}

		bool mAddCharacteristic(int uuid, GattCharacteristic^ gctrstic) {
			mUUID2CharacteristicMap[uuid] = gctrstic;
			return true;
		}

		void SetValueChangedToken(Windows::Foundation::EventRegistrationToken token) {
			mValueChangedToken = token;
		}

		Windows::Foundation::EventRegistrationToken GetValueChangedToken() {
			return(mValueChangedToken);
		}

		unsigned int mGetPipeHandle() {
			return((unsigned int)mDataPipeHandle);
		}

        int mGetIdx() {
            return(idx);
        }
        
		bool GetDeviceMACFromId(String^ id, unsigned char *macAdr);

//Windows::Foundation::TypedEventHandler<Windows::Devices::Bluetooth::BluetoothLEDevice^, Platform::Object^>^ connStatusChangedEvent;

//void InstallCharacteristicValueChangedCallback(GattCharacteristic^ gattCharacteristic);
		void OnValueChanged(Windows::Devices::Bluetooth::GenericAttributeProfile::GattCharacteristic ^ gattChar, Windows::Devices::Bluetooth::GenericAttributeProfile::GattValueChangedEventArgs ^ valChanged);
	};

	public ref class BLEServerCallback sealed {
		friend class BLEServer;

		void mInstallCallback(BluetoothLEDevice^ btdev) {
			btdev->DeviceInformation->Pairing->Custom->PairingRequested += ref new TypedEventHandler<DeviceInformationCustomPairing^, DevicePairingRequestedEventArgs^>(this, &BLEServerCallback::PairingRequestedHandler);
		}

		void PairingRequestedHandler(DeviceInformationCustomPairing^ sender, DevicePairingRequestedEventArgs^ args);
	};

    class BLEServer sealed {
        friend ref class BLEDeviceInstance;
    public:
		BLEServer() { 
			printf("BLEServer Initializing.\n"); 
//            EnumerateBLE();
			mBTSem = CreateSemaphore(NULL, 1, 1, NULL);

			mpPairingCallback = ref new BLEServerCallback;			
		};
		~BLEServer() { 
//			printf("BLEServer Destructor.\n"); 
			delete mpPairingCallback;
		};

		void EnumerateBLE();
		boolean OpenDevice(DeviceInformation^ devinfo);
		bool WriteCharacteristic(DWORD64 macAddr, int characteristicType, unsigned char *pIn, int len);
		bool ReadCharacteristic(DWORD64 macAddr, int characteristicType, unsigned char *pOut, unsigned int bufSz, unsigned int *len);
		bool InstallCharacteristicValueChangedCallback(DWORD64 macAddr, int characteristicUUID);
		bool UnInstallCharacteristicValueChangedCallback(DWORD64 macAddr, int characteristicUUID);
		//void OnValueChanged(Windows::Devices::Bluetooth::GenericAttributeProfile::GattCharacteristic ^ gattChar, Windows::Devices::Bluetooth::GenericAttributeProfile::GattValueChangedEventArgs ^ valChanged);
		bool SetCharacteristicNotification(DWORD64 macAddr, int characteristicUUID, int mode);
		int GetDeviceInfo(DWORD64 macAddr, unsigned int *btAdrHi, unsigned int *btAdrLo, unsigned int *connectStatus);
		void CloseDevices();
		void BLEThread(HANDLE *pPipeHandle);
		void Pair();
        //static void PairingRequestedHandler(DeviceInformationCustomPairing^ sender, DevicePairingRequestedEventArgs^ args);

//		BLEDeviceInstance^ mGetDeviceInstance(int idx) {
//			return(mIdx2Inst[idx]);
//		}

        BLEDeviceInstance^ mGetDeviceInstance(DWORD64 macAddr) {
            return(mMAC2Inst[macAddr]);
        }
        
        int mGetDeviceCnt() {
            return(mIdx2Inst.size());
        }

    private:
		
        bool mWatcherStopped;
		Windows::Devices::Enumeration::DeviceWatcher^ deviceWatcher;
        Windows::Foundation::TypedEventHandler<Windows::Devices::Enumeration::DeviceWatcher^, Windows::Devices::Enumeration::DeviceInformation^>^ handlerAdded;
        Windows::Foundation::TypedEventHandler<Windows::Devices::Enumeration::DeviceWatcher^, Windows::Devices::Enumeration::DeviceInformationUpdate^>^ handlerUpdated;
        Windows::Foundation::TypedEventHandler<Windows::Devices::Enumeration::DeviceWatcher^, Windows::Devices::Enumeration::DeviceInformationUpdate^>^ handlerRemoved;
        Windows::Foundation::TypedEventHandler<Windows::Devices::Enumeration::DeviceWatcher^, Platform::Object^>^ handlerEnumCompleted;
        Windows::Foundation::TypedEventHandler<Windows::Devices::Enumeration::DeviceWatcher^, Platform::Object^>^ handlerStopped;
        Windows::Foundation::EventRegistrationToken handlerAddedToken;
        Windows::Foundation::EventRegistrationToken handlerUpdatedToken;
        Windows::Foundation::EventRegistrationToken handlerRemovedToken;
        Windows::Foundation::EventRegistrationToken handlerEnumCompletedToken;
        Windows::Foundation::EventRegistrationToken handlerStoppedToken;
        Windows::Foundation::EventRegistrationToken handlerConnectStatusChanged;
		
		bool mEnumComplete;
		bool mEnumStopping;
		HANDLE mBTSem;
		std::map<String^, DeviceInformation^> mDeviceId2DevInfo;    // Remove?
		std::map<String^, BLEDeviceInstance^> mDeviceId2BLEDevInst; // 
		std::map<int, String^> mIdx2DeviceId;                       // Remove?
		std::map<int, BLEDeviceInstance^> mIdx2Inst;                // Remove?
        std::map<DWORD64, BLEDeviceInstance^> mMAC2Inst;
		bool DeviceInstanceFromIdx(int idx, BLEDeviceInstance^& devInst);
		BLEServerCallback^ mpPairingCallback;
};

}

