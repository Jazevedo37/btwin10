/****************************************************************************
** Meta object code from reading C++ file 'BluetoothController.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../include/BluetoothController.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'BluetoothController.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_BluetoothController_t {
    QByteArrayData data[34];
    char stringdata0[432];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_BluetoothController_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_BluetoothController_t qt_meta_stringdata_BluetoothController = {
    {
QT_MOC_LITERAL(0, 0, 19), // "BluetoothController"
QT_MOC_LITERAL(1, 20, 13), // "errorOccurred"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 9), // "errorCode"
QT_MOC_LITERAL(4, 45, 8), // "scanning"
QT_MOC_LITERAL(5, 54, 11), // "deviceAdded"
QT_MOC_LITERAL(6, 66, 18), // "BluetoothImuDevice"
QT_MOC_LITERAL(7, 85, 6), // "device"
QT_MOC_LITERAL(8, 92, 13), // "deviceRemoved"
QT_MOC_LITERAL(9, 106, 17), // "deviceListChanged"
QT_MOC_LITERAL(10, 124, 12), // "stateChanged"
QT_MOC_LITERAL(11, 137, 18), // "newDataPacketReady"
QT_MOC_LITERAL(12, 156, 12), // "addNewDevice"
QT_MOC_LITERAL(13, 169, 7), // "DWORD64"
QT_MOC_LITERAL(14, 177, 21), // "updateBleDeviceString"
QT_MOC_LITERAL(15, 199, 14), // "newDeviceState"
QT_MOC_LITERAL(16, 214, 18), // "newControllerState"
QT_MOC_LITERAL(17, 233, 17), // "updateLedStateGui"
QT_MOC_LITERAL(18, 251, 18), // "clearGuiDeviceList"
QT_MOC_LITERAL(19, 270, 12), // "scanComplete"
QT_MOC_LITERAL(20, 283, 12), // "scanCanceled"
QT_MOC_LITERAL(21, 296, 11), // "deviceFound"
QT_MOC_LITERAL(22, 308, 21), // "controllerInfoChanged"
QT_MOC_LITERAL(23, 330, 14), // "packetReceived"
QT_MOC_LITERAL(24, 345, 10), // "ble_header"
QT_MOC_LITERAL(25, 356, 3), // "hdr"
QT_MOC_LITERAL(26, 360, 10), // "packetSent"
QT_MOC_LITERAL(27, 371, 5), // "State"
QT_MOC_LITERAL(28, 377, 1), // "s"
QT_MOC_LITERAL(29, 379, 16), // "deviceLockedDown"
QT_MOC_LITERAL(30, 396, 4), // "done"
QT_MOC_LITERAL(31, 401, 2), // "rc"
QT_MOC_LITERAL(32, 404, 6), // "endAll"
QT_MOC_LITERAL(33, 411, 20) // "slotDeviceLockedDown"

    },
    "BluetoothController\0errorOccurred\0\0"
    "errorCode\0scanning\0deviceAdded\0"
    "BluetoothImuDevice\0device\0deviceRemoved\0"
    "deviceListChanged\0stateChanged\0"
    "newDataPacketReady\0addNewDevice\0DWORD64\0"
    "updateBleDeviceString\0newDeviceState\0"
    "newControllerState\0updateLedStateGui\0"
    "clearGuiDeviceList\0scanComplete\0"
    "scanCanceled\0deviceFound\0controllerInfoChanged\0"
    "packetReceived\0ble_header\0hdr\0packetSent\0"
    "State\0s\0deviceLockedDown\0done\0rc\0"
    "endAll\0slotDeviceLockedDown"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_BluetoothController[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      24,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      22,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  134,    2, 0x06 /* Public */,
       4,    0,  137,    2, 0x06 /* Public */,
       5,    1,  138,    2, 0x06 /* Public */,
       8,    1,  141,    2, 0x06 /* Public */,
       9,    0,  144,    2, 0x06 /* Public */,
      10,    1,  145,    2, 0x06 /* Public */,
      11,    0,  148,    2, 0x06 /* Public */,
      12,    5,  149,    2, 0x06 /* Public */,
      14,    5,  160,    2, 0x06 /* Public */,
      15,    1,  171,    2, 0x06 /* Public */,
      16,    1,  174,    2, 0x06 /* Public */,
      17,    0,  177,    2, 0x06 /* Public */,
      18,    0,  178,    2, 0x06 /* Public */,
      19,    0,  179,    2, 0x06 /* Public */,
      20,    0,  180,    2, 0x06 /* Public */,
      21,    1,  181,    2, 0x06 /* Public */,
      22,    0,  184,    2, 0x06 /* Public */,
      23,    1,  185,    2, 0x06 /* Public */,
      26,    1,  188,    2, 0x06 /* Public */,
      10,    1,  191,    2, 0x06 /* Public */,
      29,    0,  194,    2, 0x06 /* Public */,
      30,    1,  195,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      32,    0,  198,    2, 0x0a /* Public */,
      33,    0,  199,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 13, QMetaType::QString, QMetaType::Int, QMetaType::Bool,    2,    2,    2,    2,    2,
    QMetaType::Void, QMetaType::QString, QMetaType::Int, QMetaType::QString, QMetaType::Int, QMetaType::Bool,    2,    2,    2,    2,    2,
    QMetaType::Void, 0x80000000 | 13,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 24,   25,
    QMetaType::Void, 0x80000000 | 24,   25,
    QMetaType::Void, 0x80000000 | 27,   28,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   31,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void BluetoothController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        BluetoothController *_t = static_cast<BluetoothController *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->errorOccurred((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->scanning(); break;
        case 2: _t->deviceAdded((*reinterpret_cast< const BluetoothImuDevice(*)>(_a[1]))); break;
        case 3: _t->deviceRemoved((*reinterpret_cast< const BluetoothImuDevice(*)>(_a[1]))); break;
        case 4: _t->deviceListChanged(); break;
        case 5: _t->stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->newDataPacketReady(); break;
        case 7: _t->addNewDevice((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< DWORD64(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< bool(*)>(_a[5]))); break;
        case 8: _t->updateBleDeviceString((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< bool(*)>(_a[5]))); break;
        case 9: _t->newDeviceState((*reinterpret_cast< DWORD64(*)>(_a[1]))); break;
        case 10: _t->newControllerState((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->updateLedStateGui(); break;
        case 12: _t->clearGuiDeviceList(); break;
        case 13: _t->scanComplete(); break;
        case 14: _t->scanCanceled(); break;
        case 15: _t->deviceFound((*reinterpret_cast< const BluetoothImuDevice(*)>(_a[1]))); break;
        case 16: _t->controllerInfoChanged(); break;
        case 17: _t->packetReceived((*reinterpret_cast< ble_header(*)>(_a[1]))); break;
        case 18: _t->packetSent((*reinterpret_cast< ble_header(*)>(_a[1]))); break;
        case 19: _t->stateChanged((*reinterpret_cast< State(*)>(_a[1]))); break;
        case 20: _t->deviceLockedDown(); break;
        case 21: _t->done((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: _t->endAll(); break;
        case 23: _t->slotDeviceLockedDown(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (BluetoothController::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::errorOccurred)) {
                *result = 0;
            }
        }
        {
            typedef void (BluetoothController::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::scanning)) {
                *result = 1;
            }
        }
        {
            typedef void (BluetoothController::*_t)(const BluetoothImuDevice & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::deviceAdded)) {
                *result = 2;
            }
        }
        {
            typedef void (BluetoothController::*_t)(const BluetoothImuDevice & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::deviceRemoved)) {
                *result = 3;
            }
        }
        {
            typedef void (BluetoothController::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::deviceListChanged)) {
                *result = 4;
            }
        }
        {
            typedef void (BluetoothController::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::stateChanged)) {
                *result = 5;
            }
        }
        {
            typedef void (BluetoothController::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::newDataPacketReady)) {
                *result = 6;
            }
        }
        {
            typedef void (BluetoothController::*_t)(QString , DWORD64 , QString , int , bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::addNewDevice)) {
                *result = 7;
            }
        }
        {
            typedef void (BluetoothController::*_t)(QString , int , QString , int , bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::updateBleDeviceString)) {
                *result = 8;
            }
        }
        {
            typedef void (BluetoothController::*_t)(DWORD64 );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::newDeviceState)) {
                *result = 9;
            }
        }
        {
            typedef void (BluetoothController::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::newControllerState)) {
                *result = 10;
            }
        }
        {
            typedef void (BluetoothController::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::updateLedStateGui)) {
                *result = 11;
            }
        }
        {
            typedef void (BluetoothController::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::clearGuiDeviceList)) {
                *result = 12;
            }
        }
        {
            typedef void (BluetoothController::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::scanComplete)) {
                *result = 13;
            }
        }
        {
            typedef void (BluetoothController::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::scanCanceled)) {
                *result = 14;
            }
        }
        {
            typedef void (BluetoothController::*_t)(const BluetoothImuDevice & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::deviceFound)) {
                *result = 15;
            }
        }
        {
            typedef void (BluetoothController::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::controllerInfoChanged)) {
                *result = 16;
            }
        }
        {
            typedef void (BluetoothController::*_t)(ble_header );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::packetReceived)) {
                *result = 17;
            }
        }
        {
            typedef void (BluetoothController::*_t)(ble_header );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::packetSent)) {
                *result = 18;
            }
        }
        {
            typedef void (BluetoothController::*_t)(State );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::stateChanged)) {
                *result = 19;
            }
        }
        {
            typedef void (BluetoothController::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::deviceLockedDown)) {
                *result = 20;
            }
        }
        {
            typedef void (BluetoothController::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothController::done)) {
                *result = 21;
            }
        }
    }
}

const QMetaObject BluetoothController::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_BluetoothController.data,
      qt_meta_data_BluetoothController,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *BluetoothController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *BluetoothController::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_BluetoothController.stringdata0))
        return static_cast<void*>(const_cast< BluetoothController*>(this));
    return QObject::qt_metacast(_clname);
}

int BluetoothController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 24)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 24;
    }
    return _id;
}

// SIGNAL 0
void BluetoothController::errorOccurred(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void BluetoothController::scanning()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void BluetoothController::deviceAdded(const BluetoothImuDevice & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void BluetoothController::deviceRemoved(const BluetoothImuDevice & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void BluetoothController::deviceListChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void BluetoothController::stateChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void BluetoothController::newDataPacketReady()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void BluetoothController::addNewDevice(QString _t1, DWORD64 _t2, QString _t3, int _t4, bool _t5)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void BluetoothController::updateBleDeviceString(QString _t1, int _t2, QString _t3, int _t4, bool _t5)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void BluetoothController::newDeviceState(DWORD64 _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void BluetoothController::newControllerState(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void BluetoothController::updateLedStateGui()
{
    QMetaObject::activate(this, &staticMetaObject, 11, Q_NULLPTR);
}

// SIGNAL 12
void BluetoothController::clearGuiDeviceList()
{
    QMetaObject::activate(this, &staticMetaObject, 12, Q_NULLPTR);
}

// SIGNAL 13
void BluetoothController::scanComplete()
{
    QMetaObject::activate(this, &staticMetaObject, 13, Q_NULLPTR);
}

// SIGNAL 14
void BluetoothController::scanCanceled()
{
    QMetaObject::activate(this, &staticMetaObject, 14, Q_NULLPTR);
}

// SIGNAL 15
void BluetoothController::deviceFound(const BluetoothImuDevice & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 15, _a);
}

// SIGNAL 16
void BluetoothController::controllerInfoChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 16, Q_NULLPTR);
}

// SIGNAL 17
void BluetoothController::packetReceived(ble_header _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 17, _a);
}

// SIGNAL 18
void BluetoothController::packetSent(ble_header _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 18, _a);
}

// SIGNAL 19
void BluetoothController::stateChanged(State _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 19, _a);
}

// SIGNAL 20
void BluetoothController::deviceLockedDown()
{
    QMetaObject::activate(this, &staticMetaObject, 20, Q_NULLPTR);
}

// SIGNAL 21
void BluetoothController::done(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 21, _a);
}
QT_END_MOC_NAMESPACE
