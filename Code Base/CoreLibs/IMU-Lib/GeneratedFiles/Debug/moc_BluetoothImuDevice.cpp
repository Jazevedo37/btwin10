/****************************************************************************
** Meta object code from reading C++ file 'BluetoothImuDevice.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../include/BluetoothImuDevice.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'BluetoothImuDevice.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_BluetoothImuDevice_t {
    QByteArrayData data[16];
    char stringdata0[222];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_BluetoothImuDevice_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_BluetoothImuDevice_t qt_meta_stringdata_BluetoothImuDevice = {
    {
QT_MOC_LITERAL(0, 0, 18), // "BluetoothImuDevice"
QT_MOC_LITERAL(1, 19, 21), // "signalStrengthChanged"
QT_MOC_LITERAL(2, 41, 0), // ""
QT_MOC_LITERAL(3, 42, 6), // "device"
QT_MOC_LITERAL(4, 49, 10), // "connecting"
QT_MOC_LITERAL(5, 60, 9), // "connected"
QT_MOC_LITERAL(6, 70, 13), // "disconnecting"
QT_MOC_LITERAL(7, 84, 12), // "disconnected"
QT_MOC_LITERAL(8, 97, 21), // "streamingStateChanged"
QT_MOC_LITERAL(9, 119, 9), // "dataReady"
QT_MOC_LITERAL(10, 129, 12), // "stateChanged"
QT_MOC_LITERAL(11, 142, 13), // "servicesFound"
QT_MOC_LITERAL(12, 156, 20), // "characteristicsFound"
QT_MOC_LITERAL(13, 177, 11), // "infoChanged"
QT_MOC_LITERAL(14, 189, 15), // "ledStateChanged"
QT_MOC_LITERAL(15, 205, 16) // "deviceLockedDown"

    },
    "BluetoothImuDevice\0signalStrengthChanged\0"
    "\0device\0connecting\0connected\0disconnecting\0"
    "disconnected\0streamingStateChanged\0"
    "dataReady\0stateChanged\0servicesFound\0"
    "characteristicsFound\0infoChanged\0"
    "ledStateChanged\0deviceLockedDown"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_BluetoothImuDevice[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      13,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   79,    2, 0x06 /* Public */,
       4,    1,   82,    2, 0x06 /* Public */,
       5,    1,   85,    2, 0x06 /* Public */,
       6,    1,   88,    2, 0x06 /* Public */,
       7,    1,   91,    2, 0x06 /* Public */,
       8,    1,   94,    2, 0x06 /* Public */,
       9,    1,   97,    2, 0x06 /* Public */,
      10,    1,  100,    2, 0x06 /* Public */,
      11,    1,  103,    2, 0x06 /* Public */,
      12,    1,  106,    2, 0x06 /* Public */,
      13,    1,  109,    2, 0x06 /* Public */,
      14,    1,  112,    2, 0x06 /* Public */,
      15,    0,  115,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 0,    3,
    QMetaType::Void, 0x80000000 | 0,    3,
    QMetaType::Void, 0x80000000 | 0,    3,
    QMetaType::Void, 0x80000000 | 0,    3,
    QMetaType::Void, 0x80000000 | 0,    3,
    QMetaType::Void, 0x80000000 | 0,    3,
    QMetaType::Void, 0x80000000 | 0,    3,
    QMetaType::Void, 0x80000000 | 0,    3,
    QMetaType::Void, 0x80000000 | 0,    3,
    QMetaType::Void, 0x80000000 | 0,    3,
    QMetaType::Void, 0x80000000 | 0,    3,
    QMetaType::Void, 0x80000000 | 0,    3,
    QMetaType::Void,

       0        // eod
};

void BluetoothImuDevice::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        BluetoothImuDevice *_t = static_cast<BluetoothImuDevice *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->signalStrengthChanged((*reinterpret_cast< const BluetoothImuDevice(*)>(_a[1]))); break;
        case 1: _t->connecting((*reinterpret_cast< const BluetoothImuDevice(*)>(_a[1]))); break;
        case 2: _t->connected((*reinterpret_cast< const BluetoothImuDevice(*)>(_a[1]))); break;
        case 3: _t->disconnecting((*reinterpret_cast< const BluetoothImuDevice(*)>(_a[1]))); break;
        case 4: _t->disconnected((*reinterpret_cast< const BluetoothImuDevice(*)>(_a[1]))); break;
        case 5: _t->streamingStateChanged((*reinterpret_cast< const BluetoothImuDevice(*)>(_a[1]))); break;
        case 6: _t->dataReady((*reinterpret_cast< const BluetoothImuDevice(*)>(_a[1]))); break;
        case 7: _t->stateChanged((*reinterpret_cast< const BluetoothImuDevice(*)>(_a[1]))); break;
        case 8: _t->servicesFound((*reinterpret_cast< const BluetoothImuDevice(*)>(_a[1]))); break;
        case 9: _t->characteristicsFound((*reinterpret_cast< const BluetoothImuDevice(*)>(_a[1]))); break;
        case 10: _t->infoChanged((*reinterpret_cast< const BluetoothImuDevice(*)>(_a[1]))); break;
        case 11: _t->ledStateChanged((*reinterpret_cast< const BluetoothImuDevice(*)>(_a[1]))); break;
        case 12: _t->deviceLockedDown(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (BluetoothImuDevice::*_t)(const BluetoothImuDevice & ) const;
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothImuDevice::signalStrengthChanged)) {
                *result = 0;
            }
        }
        {
            typedef void (BluetoothImuDevice::*_t)(const BluetoothImuDevice & ) const;
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothImuDevice::connecting)) {
                *result = 1;
            }
        }
        {
            typedef void (BluetoothImuDevice::*_t)(const BluetoothImuDevice & ) const;
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothImuDevice::connected)) {
                *result = 2;
            }
        }
        {
            typedef void (BluetoothImuDevice::*_t)(const BluetoothImuDevice & ) const;
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothImuDevice::disconnecting)) {
                *result = 3;
            }
        }
        {
            typedef void (BluetoothImuDevice::*_t)(const BluetoothImuDevice & ) const;
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothImuDevice::disconnected)) {
                *result = 4;
            }
        }
        {
            typedef void (BluetoothImuDevice::*_t)(const BluetoothImuDevice & ) const;
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothImuDevice::streamingStateChanged)) {
                *result = 5;
            }
        }
        {
            typedef void (BluetoothImuDevice::*_t)(const BluetoothImuDevice & ) const;
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothImuDevice::dataReady)) {
                *result = 6;
            }
        }
        {
            typedef void (BluetoothImuDevice::*_t)(const BluetoothImuDevice & ) const;
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothImuDevice::stateChanged)) {
                *result = 7;
            }
        }
        {
            typedef void (BluetoothImuDevice::*_t)(const BluetoothImuDevice & ) const;
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothImuDevice::servicesFound)) {
                *result = 8;
            }
        }
        {
            typedef void (BluetoothImuDevice::*_t)(const BluetoothImuDevice & ) const;
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothImuDevice::characteristicsFound)) {
                *result = 9;
            }
        }
        {
            typedef void (BluetoothImuDevice::*_t)(const BluetoothImuDevice & ) const;
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothImuDevice::infoChanged)) {
                *result = 10;
            }
        }
        {
            typedef void (BluetoothImuDevice::*_t)(const BluetoothImuDevice & ) const;
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothImuDevice::ledStateChanged)) {
                *result = 11;
            }
        }
        {
            typedef void (BluetoothImuDevice::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BluetoothImuDevice::deviceLockedDown)) {
                *result = 12;
            }
        }
    }
}

const QMetaObject BluetoothImuDevice::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_BluetoothImuDevice.data,
      qt_meta_data_BluetoothImuDevice,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *BluetoothImuDevice::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *BluetoothImuDevice::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_BluetoothImuDevice.stringdata0))
        return static_cast<void*>(const_cast< BluetoothImuDevice*>(this));
    return QObject::qt_metacast(_clname);
}

int BluetoothImuDevice::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void BluetoothImuDevice::signalStrengthChanged(const BluetoothImuDevice & _t1)const
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(const_cast< BluetoothImuDevice *>(this), &staticMetaObject, 0, _a);
}

// SIGNAL 1
void BluetoothImuDevice::connecting(const BluetoothImuDevice & _t1)const
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(const_cast< BluetoothImuDevice *>(this), &staticMetaObject, 1, _a);
}

// SIGNAL 2
void BluetoothImuDevice::connected(const BluetoothImuDevice & _t1)const
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(const_cast< BluetoothImuDevice *>(this), &staticMetaObject, 2, _a);
}

// SIGNAL 3
void BluetoothImuDevice::disconnecting(const BluetoothImuDevice & _t1)const
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(const_cast< BluetoothImuDevice *>(this), &staticMetaObject, 3, _a);
}

// SIGNAL 4
void BluetoothImuDevice::disconnected(const BluetoothImuDevice & _t1)const
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(const_cast< BluetoothImuDevice *>(this), &staticMetaObject, 4, _a);
}

// SIGNAL 5
void BluetoothImuDevice::streamingStateChanged(const BluetoothImuDevice & _t1)const
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(const_cast< BluetoothImuDevice *>(this), &staticMetaObject, 5, _a);
}

// SIGNAL 6
void BluetoothImuDevice::dataReady(const BluetoothImuDevice & _t1)const
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(const_cast< BluetoothImuDevice *>(this), &staticMetaObject, 6, _a);
}

// SIGNAL 7
void BluetoothImuDevice::stateChanged(const BluetoothImuDevice & _t1)const
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(const_cast< BluetoothImuDevice *>(this), &staticMetaObject, 7, _a);
}

// SIGNAL 8
void BluetoothImuDevice::servicesFound(const BluetoothImuDevice & _t1)const
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(const_cast< BluetoothImuDevice *>(this), &staticMetaObject, 8, _a);
}

// SIGNAL 9
void BluetoothImuDevice::characteristicsFound(const BluetoothImuDevice & _t1)const
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(const_cast< BluetoothImuDevice *>(this), &staticMetaObject, 9, _a);
}

// SIGNAL 10
void BluetoothImuDevice::infoChanged(const BluetoothImuDevice & _t1)const
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(const_cast< BluetoothImuDevice *>(this), &staticMetaObject, 10, _a);
}

// SIGNAL 11
void BluetoothImuDevice::ledStateChanged(const BluetoothImuDevice & _t1)const
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(const_cast< BluetoothImuDevice *>(this), &staticMetaObject, 11, _a);
}

// SIGNAL 12
void BluetoothImuDevice::deviceLockedDown()
{
    QMetaObject::activate(this, &staticMetaObject, 12, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
