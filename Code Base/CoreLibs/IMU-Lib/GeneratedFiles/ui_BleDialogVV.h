/********************************************************************************
** Form generated from reading UI file 'BleDialogVV.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BLEDIALOGVV_H
#define UI_BLEDIALOGVV_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QLabel *label;
    QLabel *labelBleStatus;
    QGroupBox *groupBox;
    QPushButton *pushButtonScan;
    QPushButton *pushButtonTerminateLinkRequests;
    QPushButton *pushButtonTerminateActiveLinks;
    QPushButton *pushButtonReinitializeDongle;
    QPushButton *pushButtonRelinkAll;
    QLabel *label_3;
    QPushButton *pushButtonStartImuStreaming;
    QPushButton *pushButtonStopImuStreaming;
    QLineEdit *lineEditScanFilterString;
    QPushButton *pushButtonResetDongle;
    QGroupBox *groupBox_5;
    QRadioButton *radioButtonOneDataPacket;
    QRadioButton *radioButtonTwoDataPackets;
    QGroupBox *groupBox_7;
    QLabel *label_4;
    QLineEdit *lineEditMinConnInterval;
    QLabel *label_5;
    QLineEdit *lineEditMaxConnInterval;
    QGroupBox *groupBox_2;
    QCheckBox *checkBoxBleLeds;
    QCheckBox *checkBoxBleDeviceStringFiltering;
    QGroupBox *groupBox_3;
    QListWidget *listWidget;
    QLabel *labelBleDeviceState;
    QLabel *label_7;
    QPushButton *pushButtonSendBleCommand;
    QPushButton *pushButtonTerminateLinkDevice;
    QPushButton *pushButtonDevicePermanentLockdown;
    QPushButton *pushButtonDeviceSoftwareReset;
    QLabel *label_8;
    QLineEdit *lineEditBleDeviceHandle;
    QLineEdit *lineEditBleDeviceValue;
    QLabel *label_9;
    QGroupBox *groupBox_4;
    QRadioButton *radioButtonRedLed;
    QRadioButton *radioButtonYellowLed;
    QRadioButton *radioButtonGreenLed;
    QRadioButton *radioButtonBlinkingAllLeds;
    QRadioButton *radioButtonNoLed;
    QRadioButton *radioButtonBlinkingRedLed;
    QRadioButton *radioButtonBlinkingYellowLed;
    QRadioButton *radioButtonBlinkingGreenLed;
    QRadioButton *radioButtonRedYellowLed;
    QRadioButton *radioButtonRedGreenLed;
    QRadioButton *radioButtonYellowGreenLed;
    QGroupBox *groupBox_6;
    QPushButton *pushButtonStartStreamingDevice;
    QPushButton *pushButtonStopStreamingDevice;
    QPushButton *pushButtonRelinkDevice;
    QLabel *labelBleDeviceRefreshRate;
    QLabel *label_10;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QStringLiteral("Dialog"));
        Dialog->resize(749, 696);
        Dialog->setMouseTracking(false);
        label = new QLabel(Dialog);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(380, 590, 140, 30));
        QFont font;
        font.setFamily(QStringLiteral("Arial"));
        font.setPointSize(10);
        label->setFont(font);
        labelBleStatus = new QLabel(Dialog);
        labelBleStatus->setObjectName(QStringLiteral("labelBleStatus"));
        labelBleStatus->setGeometry(QRect(520, 590, 170, 30));
        QFont font1;
        font1.setFamily(QStringLiteral("Arial"));
        font1.setPointSize(10);
        font1.setBold(true);
        font1.setWeight(75);
        labelBleStatus->setFont(font1);
        groupBox = new QGroupBox(Dialog);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(10, 14, 731, 209));
        groupBox->setFont(font);
        pushButtonScan = new QPushButton(groupBox);
        pushButtonScan->setObjectName(QStringLiteral("pushButtonScan"));
        pushButtonScan->setGeometry(QRect(10, 30, 170, 30));
        pushButtonTerminateLinkRequests = new QPushButton(groupBox);
        pushButtonTerminateLinkRequests->setObjectName(QStringLiteral("pushButtonTerminateLinkRequests"));
        pushButtonTerminateLinkRequests->setGeometry(QRect(550, 30, 170, 30));
        pushButtonTerminateActiveLinks = new QPushButton(groupBox);
        pushButtonTerminateActiveLinks->setObjectName(QStringLiteral("pushButtonTerminateActiveLinks"));
        pushButtonTerminateActiveLinks->setGeometry(QRect(370, 30, 170, 30));
        pushButtonReinitializeDongle = new QPushButton(groupBox);
        pushButtonReinitializeDongle->setObjectName(QStringLiteral("pushButtonReinitializeDongle"));
        pushButtonReinitializeDongle->setGeometry(QRect(370, 70, 170, 30));
        pushButtonRelinkAll = new QPushButton(groupBox);
        pushButtonRelinkAll->setObjectName(QStringLiteral("pushButtonRelinkAll"));
        pushButtonRelinkAll->setGeometry(QRect(190, 30, 170, 30));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(20, 110, 120, 30));
        label_3->setFont(font);
        pushButtonStartImuStreaming = new QPushButton(groupBox);
        pushButtonStartImuStreaming->setObjectName(QStringLiteral("pushButtonStartImuStreaming"));
        pushButtonStartImuStreaming->setGeometry(QRect(10, 70, 170, 30));
        pushButtonStopImuStreaming = new QPushButton(groupBox);
        pushButtonStopImuStreaming->setObjectName(QStringLiteral("pushButtonStopImuStreaming"));
        pushButtonStopImuStreaming->setGeometry(QRect(190, 70, 170, 30));
        lineEditScanFilterString = new QLineEdit(groupBox);
        lineEditScanFilterString->setObjectName(QStringLiteral("lineEditScanFilterString"));
        lineEditScanFilterString->setGeometry(QRect(190, 110, 171, 30));
        pushButtonResetDongle = new QPushButton(groupBox);
        pushButtonResetDongle->setObjectName(QStringLiteral("pushButtonResetDongle"));
        pushButtonResetDongle->setGeometry(QRect(550, 70, 170, 30));
        groupBox_5 = new QGroupBox(groupBox);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        groupBox_5->setGeometry(QRect(550, 110, 171, 89));
        groupBox_5->setFocusPolicy(Qt::NoFocus);
        groupBox_5->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        groupBox_5->setFlat(false);
        radioButtonOneDataPacket = new QRadioButton(groupBox_5);
        radioButtonOneDataPacket->setObjectName(QStringLiteral("radioButtonOneDataPacket"));
        radioButtonOneDataPacket->setGeometry(QRect(14, 22, 131, 30));
        radioButtonTwoDataPackets = new QRadioButton(groupBox_5);
        radioButtonTwoDataPackets->setObjectName(QStringLiteral("radioButtonTwoDataPackets"));
        radioButtonTwoDataPackets->setGeometry(QRect(14, 56, 131, 30));
        radioButtonTwoDataPackets->setChecked(true);
        groupBox_7 = new QGroupBox(groupBox);
        groupBox_7->setObjectName(QStringLiteral("groupBox_7"));
        groupBox_7->setGeometry(QRect(370, 110, 171, 89));
        groupBox_7->setFocusPolicy(Qt::NoFocus);
        groupBox_7->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        groupBox_7->setFlat(false);
        label_4 = new QLabel(groupBox_7);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(10, 20, 50, 30));
        label_4->setFont(font);
        lineEditMinConnInterval = new QLineEdit(groupBox_7);
        lineEditMinConnInterval->setObjectName(QStringLiteral("lineEditMinConnInterval"));
        lineEditMinConnInterval->setGeometry(QRect(60, 20, 100, 25));
        label_5 = new QLabel(groupBox_7);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(10, 50, 50, 30));
        label_5->setFont(font);
        lineEditMaxConnInterval = new QLineEdit(groupBox_7);
        lineEditMaxConnInterval->setObjectName(QStringLiteral("lineEditMaxConnInterval"));
        lineEditMaxConnInterval->setGeometry(QRect(60, 50, 100, 25));
        groupBox_2 = new QGroupBox(Dialog);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(8, 586, 361, 101));
        groupBox_2->setFont(font);
        checkBoxBleLeds = new QCheckBox(groupBox_2);
        checkBoxBleLeds->setObjectName(QStringLiteral("checkBoxBleLeds"));
        checkBoxBleLeds->setGeometry(QRect(20, 28, 200, 30));
        checkBoxBleLeds->setFont(font);
        checkBoxBleDeviceStringFiltering = new QCheckBox(groupBox_2);
        checkBoxBleDeviceStringFiltering->setObjectName(QStringLiteral("checkBoxBleDeviceStringFiltering"));
        checkBoxBleDeviceStringFiltering->setGeometry(QRect(20, 60, 200, 30));
        checkBoxBleDeviceStringFiltering->setFont(font);
        groupBox_3 = new QGroupBox(Dialog);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(8, 230, 731, 351));
        groupBox_3->setFont(font);
        listWidget = new QListWidget(groupBox_3);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(12, 30, 350, 111));
        labelBleDeviceState = new QLabel(groupBox_3);
        labelBleDeviceState->setObjectName(QStringLiteral("labelBleDeviceState"));
        labelBleDeviceState->setGeometry(QRect(510, 310, 170, 30));
        labelBleDeviceState->setFont(font1);
        label_7 = new QLabel(groupBox_3);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(370, 310, 140, 30));
        label_7->setFont(font);
        pushButtonSendBleCommand = new QPushButton(groupBox_3);
        pushButtonSendBleCommand->setObjectName(QStringLiteral("pushButtonSendBleCommand"));
        pushButtonSendBleCommand->setGeometry(QRect(370, 110, 170, 30));
        pushButtonTerminateLinkDevice = new QPushButton(groupBox_3);
        pushButtonTerminateLinkDevice->setObjectName(QStringLiteral("pushButtonTerminateLinkDevice"));
        pushButtonTerminateLinkDevice->setGeometry(QRect(550, 30, 170, 30));
        pushButtonDevicePermanentLockdown = new QPushButton(groupBox_3);
        pushButtonDevicePermanentLockdown->setObjectName(QStringLiteral("pushButtonDevicePermanentLockdown"));
        pushButtonDevicePermanentLockdown->setGeometry(QRect(370, 70, 170, 30));
        pushButtonDeviceSoftwareReset = new QPushButton(groupBox_3);
        pushButtonDeviceSoftwareReset->setObjectName(QStringLiteral("pushButtonDeviceSoftwareReset"));
        pushButtonDeviceSoftwareReset->setGeometry(QRect(550, 70, 170, 30));
        label_8 = new QLabel(groupBox_3);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(550, 110, 60, 30));
        label_8->setFont(font);
        lineEditBleDeviceHandle = new QLineEdit(groupBox_3);
        lineEditBleDeviceHandle->setObjectName(QStringLiteral("lineEditBleDeviceHandle"));
        lineEditBleDeviceHandle->setGeometry(QRect(620, 110, 100, 25));
        lineEditBleDeviceValue = new QLineEdit(groupBox_3);
        lineEditBleDeviceValue->setObjectName(QStringLiteral("lineEditBleDeviceValue"));
        lineEditBleDeviceValue->setGeometry(QRect(620, 140, 100, 25));
        label_9 = new QLabel(groupBox_3);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(550, 140, 60, 30));
        label_9->setFont(font);
        groupBox_4 = new QGroupBox(groupBox_3);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setGeometry(QRect(10, 150, 531, 161));
        groupBox_4->setFocusPolicy(Qt::NoFocus);
        groupBox_4->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        groupBox_4->setFlat(false);
        groupBox_4->setCheckable(false);
        radioButtonRedLed = new QRadioButton(groupBox_4);
        radioButtonRedLed->setObjectName(QStringLiteral("radioButtonRedLed"));
        radioButtonRedLed->setGeometry(QRect(20, 30, 170, 30));
        radioButtonYellowLed = new QRadioButton(groupBox_4);
        radioButtonYellowLed->setObjectName(QStringLiteral("radioButtonYellowLed"));
        radioButtonYellowLed->setGeometry(QRect(190, 30, 170, 30));
        radioButtonGreenLed = new QRadioButton(groupBox_4);
        radioButtonGreenLed->setObjectName(QStringLiteral("radioButtonGreenLed"));
        radioButtonGreenLed->setGeometry(QRect(360, 30, 170, 30));
        radioButtonBlinkingAllLeds = new QRadioButton(groupBox_4);
        radioButtonBlinkingAllLeds->setObjectName(QStringLiteral("radioButtonBlinkingAllLeds"));
        radioButtonBlinkingAllLeds->setGeometry(QRect(20, 120, 170, 30));
        radioButtonNoLed = new QRadioButton(groupBox_4);
        radioButtonNoLed->setObjectName(QStringLiteral("radioButtonNoLed"));
        radioButtonNoLed->setGeometry(QRect(190, 120, 170, 30));
        radioButtonBlinkingRedLed = new QRadioButton(groupBox_4);
        radioButtonBlinkingRedLed->setObjectName(QStringLiteral("radioButtonBlinkingRedLed"));
        radioButtonBlinkingRedLed->setGeometry(QRect(20, 60, 170, 30));
        radioButtonBlinkingRedLed->setChecked(true);
        radioButtonBlinkingYellowLed = new QRadioButton(groupBox_4);
        radioButtonBlinkingYellowLed->setObjectName(QStringLiteral("radioButtonBlinkingYellowLed"));
        radioButtonBlinkingYellowLed->setGeometry(QRect(190, 60, 170, 30));
        radioButtonBlinkingGreenLed = new QRadioButton(groupBox_4);
        radioButtonBlinkingGreenLed->setObjectName(QStringLiteral("radioButtonBlinkingGreenLed"));
        radioButtonBlinkingGreenLed->setGeometry(QRect(360, 60, 170, 30));
        radioButtonRedYellowLed = new QRadioButton(groupBox_4);
        radioButtonRedYellowLed->setObjectName(QStringLiteral("radioButtonRedYellowLed"));
        radioButtonRedYellowLed->setGeometry(QRect(20, 90, 170, 30));
        radioButtonRedGreenLed = new QRadioButton(groupBox_4);
        radioButtonRedGreenLed->setObjectName(QStringLiteral("radioButtonRedGreenLed"));
        radioButtonRedGreenLed->setGeometry(QRect(360, 90, 170, 30));
        radioButtonYellowGreenLed = new QRadioButton(groupBox_4);
        radioButtonYellowGreenLed->setObjectName(QStringLiteral("radioButtonYellowGreenLed"));
        radioButtonYellowGreenLed->setGeometry(QRect(190, 90, 170, 30));
        groupBox_6 = new QGroupBox(groupBox_3);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        groupBox_6->setGeometry(QRect(552, 210, 171, 101));
        pushButtonStartStreamingDevice = new QPushButton(groupBox_6);
        pushButtonStartStreamingDevice->setObjectName(QStringLiteral("pushButtonStartStreamingDevice"));
        pushButtonStartStreamingDevice->setGeometry(QRect(10, 22, 150, 30));
        pushButtonStopStreamingDevice = new QPushButton(groupBox_6);
        pushButtonStopStreamingDevice->setObjectName(QStringLiteral("pushButtonStopStreamingDevice"));
        pushButtonStopStreamingDevice->setGeometry(QRect(10, 60, 150, 30));
        pushButtonRelinkDevice = new QPushButton(groupBox_3);
        pushButtonRelinkDevice->setObjectName(QStringLiteral("pushButtonRelinkDevice"));
        pushButtonRelinkDevice->setGeometry(QRect(370, 30, 170, 30));
        labelBleDeviceRefreshRate = new QLabel(groupBox_3);
        labelBleDeviceRefreshRate->setObjectName(QStringLiteral("labelBleDeviceRefreshRate"));
        labelBleDeviceRefreshRate->setGeometry(QRect(130, 310, 100, 30));
        labelBleDeviceRefreshRate->setFont(font1);
        label_10 = new QLabel(groupBox_3);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(30, 310, 100, 30));
        label_10->setFont(font);

        retranslateUi(Dialog);

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QApplication::translate("Dialog", "BLE V&V Options", 0));
        label->setText(QApplication::translate("Dialog", "BLE Controller Status:", 0));
        labelBleStatus->setText(QApplication::translate("Dialog", "Linking", 0));
        groupBox->setTitle(QApplication::translate("Dialog", "BLE Central Controls", 0));
        pushButtonScan->setText(QApplication::translate("Dialog", "Scan", 0));
        pushButtonTerminateLinkRequests->setText(QApplication::translate("Dialog", "Terminate Link Requests", 0));
        pushButtonTerminateActiveLinks->setText(QApplication::translate("Dialog", "Terminate Active Links", 0));
        pushButtonReinitializeDongle->setText(QApplication::translate("Dialog", "Reinitialize Dongle", 0));
        pushButtonRelinkAll->setText(QApplication::translate("Dialog", "Relink All", 0));
        label_3->setText(QApplication::translate("Dialog", "BLE Filter String", 0));
        pushButtonStartImuStreaming->setText(QApplication::translate("Dialog", "Start IMU Streaming", 0));
        pushButtonStopImuStreaming->setText(QApplication::translate("Dialog", "Stop IMU Streaming", 0));
        pushButtonResetDongle->setText(QApplication::translate("Dialog", "Reset Dongle", 0));
        groupBox_5->setTitle(QApplication::translate("Dialog", "Data Format", 0));
        radioButtonOneDataPacket->setText(QApplication::translate("Dialog", "One Data Packet", 0));
        radioButtonTwoDataPackets->setText(QApplication::translate("Dialog", "Two Data Packets", 0));
        groupBox_7->setTitle(QApplication::translate("Dialog", "Connection Interval", 0));
        label_4->setText(QApplication::translate("Dialog", "Min", 0));
        label_5->setText(QApplication::translate("Dialog", "Max", 0));
        groupBox_2->setTitle(QApplication::translate("Dialog", "Options", 0));
        checkBoxBleLeds->setText(QApplication::translate("Dialog", "Enable LED Feedback", 0));
        checkBoxBleDeviceStringFiltering->setText(QApplication::translate("Dialog", "Enable Device String Filtering", 0));
        groupBox_3->setTitle(QApplication::translate("Dialog", "BLE Devices", 0));
        labelBleDeviceState->setText(QApplication::translate("Dialog", "Streaming IMU Data", 0));
        label_7->setText(QApplication::translate("Dialog", "Device State:", 0));
        pushButtonSendBleCommand->setText(QApplication::translate("Dialog", "Send BLE Command", 0));
        pushButtonTerminateLinkDevice->setText(QApplication::translate("Dialog", "Terminate Link", 0));
        pushButtonDevicePermanentLockdown->setText(QApplication::translate("Dialog", "Permanent Lockdown", 0));
        pushButtonDeviceSoftwareReset->setText(QApplication::translate("Dialog", "Software Reset", 0));
        label_8->setText(QApplication::translate("Dialog", "Handle", 0));
        label_9->setText(QApplication::translate("Dialog", "Value", 0));
        groupBox_4->setTitle(QApplication::translate("Dialog", "LED State", 0));
        radioButtonRedLed->setText(QApplication::translate("Dialog", "Solid Red", 0));
        radioButtonYellowLed->setText(QApplication::translate("Dialog", "Solid Yellow", 0));
        radioButtonGreenLed->setText(QApplication::translate("Dialog", "Solid Blue", 0));
        radioButtonBlinkingAllLeds->setText(QApplication::translate("Dialog", "Blinking All", 0));
        radioButtonNoLed->setText(QApplication::translate("Dialog", "None Blinking", 0));
        radioButtonBlinkingRedLed->setText(QApplication::translate("Dialog", "Blinking Red", 0));
        radioButtonBlinkingYellowLed->setText(QApplication::translate("Dialog", "Blinking Yellow", 0));
        radioButtonBlinkingGreenLed->setText(QApplication::translate("Dialog", "Blinking Blue", 0));
        radioButtonRedYellowLed->setText(QApplication::translate("Dialog", "Blinking Red-Yellow", 0));
        radioButtonRedGreenLed->setText(QApplication::translate("Dialog", "Blinking Blue-Red", 0));
        radioButtonYellowGreenLed->setText(QApplication::translate("Dialog", "Blinking Yellow-Blue", 0));
        groupBox_6->setTitle(QApplication::translate("Dialog", "Data Streaming", 0));
        pushButtonStartStreamingDevice->setText(QApplication::translate("Dialog", "Start", 0));
        pushButtonStopStreamingDevice->setText(QApplication::translate("Dialog", "Stop", 0));
        pushButtonRelinkDevice->setText(QApplication::translate("Dialog", "Link", 0));
        labelBleDeviceRefreshRate->setText(QApplication::translate("Dialog", "0", 0));
        label_10->setText(QApplication::translate("Dialog", "Refresh Rate:", 0));
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BLEDIALOGVV_H
