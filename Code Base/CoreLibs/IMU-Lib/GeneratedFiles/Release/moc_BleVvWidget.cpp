/****************************************************************************
** Meta object code from reading C++ file 'BleVvWidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../include/BleVvWidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'BleVvWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_TBleVvWidget_t {
    QByteArrayData data[41];
    char stringdata0[821];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TBleVvWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TBleVvWidget_t qt_meta_stringdata_TBleVvWidget = {
    {
QT_MOC_LITERAL(0, 0, 12), // "TBleVvWidget"
QT_MOC_LITERAL(1, 13, 38), // "SignalImuCalibrationLoadedSuc..."
QT_MOC_LITERAL(2, 52, 0), // ""
QT_MOC_LITERAL(3, 53, 22), // "LedFeedbackFlagChanged"
QT_MOC_LITERAL(4, 76, 42), // "EnableFilterStringDeviceLinki..."
QT_MOC_LITERAL(5, 119, 14), // "ScanForDevices"
QT_MOC_LITERAL(6, 134, 24), // "TerminateAllLinkRequests"
QT_MOC_LITERAL(7, 159, 23), // "TerminateAllActiveLinks"
QT_MOC_LITERAL(8, 183, 19), // "RelinkAllBleDevices"
QT_MOC_LITERAL(9, 203, 27), // "StartImuStreamingAllDevices"
QT_MOC_LITERAL(10, 231, 26), // "StopImuStreamingAllDevices"
QT_MOC_LITERAL(11, 258, 21), // "ReinitializeBleDongle"
QT_MOC_LITERAL(12, 280, 22), // "SendBleCommandToDevice"
QT_MOC_LITERAL(13, 303, 25), // "SendSoftwareResetToDevice"
QT_MOC_LITERAL(14, 329, 29), // "SendPermanentLockdownToDevice"
QT_MOC_LITERAL(15, 359, 23), // "StartStreamingOneDevice"
QT_MOC_LITERAL(16, 383, 22), // "StopStreamingOneDevice"
QT_MOC_LITERAL(17, 406, 23), // "SendNewLedStateToDevice"
QT_MOC_LITERAL(18, 430, 18), // "RelinkSingleDevice"
QT_MOC_LITERAL(19, 449, 25), // "TerminateLinkSingleDevice"
QT_MOC_LITERAL(20, 475, 17), // "DataFormatChanged"
QT_MOC_LITERAL(21, 493, 14), // "ResetBleDongle"
QT_MOC_LITERAL(22, 508, 19), // "ListViewItemChanged"
QT_MOC_LITERAL(23, 528, 26), // "BleControllerStatedChanged"
QT_MOC_LITERAL(24, 555, 8), // "newState"
QT_MOC_LITERAL(25, 564, 21), // "AddNewBleDeviceToList"
QT_MOC_LITERAL(26, 586, 15), // "newDeviceString"
QT_MOC_LITERAL(27, 602, 7), // "DWORD64"
QT_MOC_LITERAL(28, 610, 8), // "deviceID"
QT_MOC_LITERAL(29, 619, 13), // "deviceAddress"
QT_MOC_LITERAL(30, 633, 16), // "deviceConnHandle"
QT_MOC_LITERAL(31, 650, 16), // "connHandleExists"
QT_MOC_LITERAL(32, 667, 21), // "BleDeviceStateChanged"
QT_MOC_LITERAL(33, 689, 23), // "UpdateBleDeviceLedState"
QT_MOC_LITERAL(34, 713, 22), // "ConnIntervalTextEdited"
QT_MOC_LITERAL(35, 736, 26), // "ScanFilterStringTextEdited"
QT_MOC_LITERAL(36, 763, 20), // "ClearDeviceListAsync"
QT_MOC_LITERAL(37, 784, 22), // "SlotRefreshRateUpdated"
QT_MOC_LITERAL(38, 807, 5), // "index"
QT_MOC_LITERAL(39, 813, 4), // "quit"
QT_MOC_LITERAL(40, 818, 2) // "rc"

    },
    "TBleVvWidget\0SignalImuCalibrationLoadedSuccessfully\0"
    "\0LedFeedbackFlagChanged\0"
    "EnableFilterStringDeviceLinkingFlagChanged\0"
    "ScanForDevices\0TerminateAllLinkRequests\0"
    "TerminateAllActiveLinks\0RelinkAllBleDevices\0"
    "StartImuStreamingAllDevices\0"
    "StopImuStreamingAllDevices\0"
    "ReinitializeBleDongle\0SendBleCommandToDevice\0"
    "SendSoftwareResetToDevice\0"
    "SendPermanentLockdownToDevice\0"
    "StartStreamingOneDevice\0StopStreamingOneDevice\0"
    "SendNewLedStateToDevice\0RelinkSingleDevice\0"
    "TerminateLinkSingleDevice\0DataFormatChanged\0"
    "ResetBleDongle\0ListViewItemChanged\0"
    "BleControllerStatedChanged\0newState\0"
    "AddNewBleDeviceToList\0newDeviceString\0"
    "DWORD64\0deviceID\0deviceAddress\0"
    "deviceConnHandle\0connHandleExists\0"
    "BleDeviceStateChanged\0UpdateBleDeviceLedState\0"
    "ConnIntervalTextEdited\0"
    "ScanFilterStringTextEdited\0"
    "ClearDeviceListAsync\0SlotRefreshRateUpdated\0"
    "index\0quit\0rc"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TBleVvWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      31,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  169,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,  170,    2, 0x09 /* Protected */,
       4,    0,  171,    2, 0x09 /* Protected */,
       5,    0,  172,    2, 0x09 /* Protected */,
       6,    0,  173,    2, 0x09 /* Protected */,
       7,    0,  174,    2, 0x09 /* Protected */,
       8,    0,  175,    2, 0x09 /* Protected */,
       9,    0,  176,    2, 0x09 /* Protected */,
      10,    0,  177,    2, 0x09 /* Protected */,
      11,    0,  178,    2, 0x09 /* Protected */,
      12,    0,  179,    2, 0x09 /* Protected */,
      13,    0,  180,    2, 0x09 /* Protected */,
      14,    0,  181,    2, 0x09 /* Protected */,
      15,    0,  182,    2, 0x09 /* Protected */,
      16,    0,  183,    2, 0x09 /* Protected */,
      17,    0,  184,    2, 0x09 /* Protected */,
      18,    0,  185,    2, 0x09 /* Protected */,
      19,    0,  186,    2, 0x09 /* Protected */,
      20,    0,  187,    2, 0x09 /* Protected */,
      21,    0,  188,    2, 0x09 /* Protected */,
      22,    0,  189,    2, 0x09 /* Protected */,
      23,    1,  190,    2, 0x09 /* Protected */,
      25,    5,  193,    2, 0x09 /* Protected */,
      25,    4,  204,    2, 0x29 /* Protected | MethodCloned */,
      32,    1,  213,    2, 0x09 /* Protected */,
      33,    0,  216,    2, 0x09 /* Protected */,
      34,    0,  217,    2, 0x09 /* Protected */,
      35,    0,  218,    2, 0x09 /* Protected */,
      36,    0,  219,    2, 0x09 /* Protected */,
      37,    1,  220,    2, 0x09 /* Protected */,
      39,    1,  223,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 27, QMetaType::QString, QMetaType::Int, QMetaType::Bool,   26,   28,   29,   30,   31,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 27, QMetaType::QString, QMetaType::Int,   26,   28,   29,   30,
    QMetaType::Void, 0x80000000 | 27,   28,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   38,
    QMetaType::Void, QMetaType::Int,   40,

       0        // eod
};

void TBleVvWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TBleVvWidget *_t = static_cast<TBleVvWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->SignalImuCalibrationLoadedSuccessfully(); break;
        case 1: _t->LedFeedbackFlagChanged(); break;
        case 2: _t->EnableFilterStringDeviceLinkingFlagChanged(); break;
        case 3: _t->ScanForDevices(); break;
        case 4: _t->TerminateAllLinkRequests(); break;
        case 5: _t->TerminateAllActiveLinks(); break;
        case 6: _t->RelinkAllBleDevices(); break;
        case 7: _t->StartImuStreamingAllDevices(); break;
        case 8: _t->StopImuStreamingAllDevices(); break;
        case 9: _t->ReinitializeBleDongle(); break;
        case 10: _t->SendBleCommandToDevice(); break;
        case 11: _t->SendSoftwareResetToDevice(); break;
        case 12: _t->SendPermanentLockdownToDevice(); break;
        case 13: _t->StartStreamingOneDevice(); break;
        case 14: _t->StopStreamingOneDevice(); break;
        case 15: _t->SendNewLedStateToDevice(); break;
        case 16: _t->RelinkSingleDevice(); break;
        case 17: _t->TerminateLinkSingleDevice(); break;
        case 18: _t->DataFormatChanged(); break;
        case 19: _t->ResetBleDongle(); break;
        case 20: _t->ListViewItemChanged(); break;
        case 21: _t->BleControllerStatedChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: _t->AddNewBleDeviceToList((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< DWORD64(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< bool(*)>(_a[5]))); break;
        case 23: _t->AddNewBleDeviceToList((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< DWORD64(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 24: _t->BleDeviceStateChanged((*reinterpret_cast< DWORD64(*)>(_a[1]))); break;
        case 25: _t->UpdateBleDeviceLedState(); break;
        case 26: _t->ConnIntervalTextEdited(); break;
        case 27: _t->ScanFilterStringTextEdited(); break;
        case 28: _t->ClearDeviceListAsync(); break;
        case 29: _t->SlotRefreshRateUpdated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 30: _t->quit((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (TBleVvWidget::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TBleVvWidget::SignalImuCalibrationLoadedSuccessfully)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject TBleVvWidget::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_TBleVvWidget.data,
      qt_meta_data_TBleVvWidget,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *TBleVvWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TBleVvWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_TBleVvWidget.stringdata0))
        return static_cast<void*>(const_cast< TBleVvWidget*>(this));
    return QDialog::qt_metacast(_clname);
}

int TBleVvWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 31)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 31;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 31)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 31;
    }
    return _id;
}

// SIGNAL 0
void TBleVvWidget::SignalImuCalibrationLoadedSuccessfully()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
