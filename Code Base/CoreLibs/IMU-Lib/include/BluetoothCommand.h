//____________________________________________________________________________ 
// 
// (c) 2015, Mohamed Mahfouz. All Rights Reserved 
// 
// The contents of this file may not be disclosed, copied or duplicated in 
// any form, in whole or part, without the prior written permission of 
// Mohamed Mahfouz. 
//____________________________________________________________________________ 
// 
// Author: Joe Marrero (jmarrero@adextechnologies.com)
// Date: 2015/04/25
//____________________________________________________________________________
#pragma once
#include "cmd_def.h"
#include "apitypes.h"

#if 0
struct BluetoothCommand {
	ble_msg_idx id;

	/* These types must match the size set in the documentation! */
	union {
		struct { // ble_cmd_gap_discover_idx
			uint8 mode;
		} gap_discover_args;

		struct { // ble_cmd_gap_set_scan_parameters_idx
			uint16 interval;
			uint16 window;
			uint8 active; // 1=active, 0=passive
		} gap_set_scan_parameters_args;

		struct { // ble_cmd_gap_connect_direct_idx
			const bd_addr* address;
			uint8 addr_type;
			uint16 conn_interval_min;
			uint16 conn_interval_max;
			uint16 timeout;
			uint16 latency;
		} gap_connect_direct_args;

		struct { // ble_cmd_connection_disconnect_idx
			uint8 connection;
		} connection_disconnect_args;
		
		struct { // ble_cmd_attclient_read_by_group_type_idx
			uint8 connection;
			uint16 start;
			uint16 end;
			uint16 uuid_len;
			const uint8* uuid_data;
		} attclient_read_by_group_type_args;

		struct { // ble_cmd_attclient_find_information_idx
			uint8 connection;
			uint16 start;
			uint16 end;
		} attclient_find_information_args;

		struct { // ble_cmd_connection_get_rssi_idx
			uint8 connection;
		} connection_get_rssi_args;

		struct { // ble_cmd_connection_update_idx
			uint8 connection;
			uint16 interval_min;
			uint16 interval_max;
			uint16 latency;
			uint16 timeout;
		} connection_update_args;

		struct { // ble_cmd_attclient_attribute_write_idx
			uint8 connection;
			uint16 atthandle;
			uint16 data_len;
			const uint8* data;
		} attclient_attribute_write_args;

		struct { // ble_cmd_attclient_read_by_handle_idx
			uint8 connection;
			uint16 characteristic_handle;
		} attclient_read_by_handle_args;

		struct { // ble_cmd_attclient_read_long_idx
			uint8 connection;
			uint16 characteristic_handle;
		} attclient_read_long_args;

		struct { // ble_cmd_sm_set_bondable_mode_idx
			uint8 bondable;
		} sm_set_bondable_mode_args;

		struct { // ble_cmd_sm_set_parameters_idx
			uint8 mitm;
			uint8 min_key_size;
			uint8 io_capabilities;
		} sm_set_parameters_args;

		struct { // ble_cmd_sm_passkey_entry_idx
			uint8 handle;
			uint32 passkey;
		} sm_passkey_entry_args;

		struct { // ble_cmd_sm_encrypt_start_idx
			uint8 handle;
			uint8 bonding;
		} sm_encrypt_start_args;
	};
};
#endif
