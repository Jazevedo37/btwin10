//____________________________________________________________________________ 
// 
// (c) 2019, All Rights Reserved 
// 
// The contents of this file may not be disclosed, copied or duplicated in 
// any form, in whole or part, without the prior written permission of 
// Mohamed Mahfouz. 
//____________________________________________________________________________ 
// 
// Author: Dan Rebello (dan@streamserverlabs.com)
// Date: 2019/10/13
//____________________________________________________________________________
#pragma once

#include <QThread.h>
#include <queue>
#include "..\..\..\..\BLEServer\pipeprot.h"

//#define ENABLE_PAUSE_TIMEOUT     1

class BluetoothController;

class BluetoothCommunicationThread : public QThread
{
	Q_OBJECT

  public:
	BluetoothCommunicationThread(BluetoothController& controller);
	~BluetoothCommunicationThread();
	
	void stop();
	void pause(); 
	void unpause(const char* file, int line);

    bool enqueueCommandToServer(BluetoothBLESrvrCmd& cmd);
	BluetoothBLESrvrCmd& peekCommand();
	size_t queueSize() const;
	void clearQueue();
	bool isPaused();
	bool isStopped();
    void stopKeepAlive() { mKeepAlive = false; }

	// Finding characteristics can take several 
	// seconds.
	static const int MAX_TIMEOUT = 8000; 

  protected:
	void run();
	void dequeueCommand();
	void dispatchCommand(BluetoothBLESrvrCmd& cmd);
    bool StartPipeResponseRead(BluetoothBLESrvrCmd& cmd, HANDLE readEvent, DWORD *bytesRead);
    BTPipeMsgReturnCode ProcessPipeResponse(BluetoothBLESrvrCmd& cmd, DWORD len);

  protected:
	mutable QMutex mMutex;
	bool mStop;
	bool mPause;
    bool mKeepAlive;
	BluetoothController& mController;
	std::queue<BluetoothBLESrvrCmd> mCommandQueue;
	std::queue<BluetoothBLESrvrCmd> mDispatchQueue;
#if ENABLE_PAUSE_TIMEOUT
	double mPauseTime;
#endif

  public slots:
};

class BluetoothReadThread : public QThread
{
	Q_OBJECT

  public:
	BluetoothReadThread(BluetoothController& controller);
	~BluetoothReadThread();
	
	void stop();

  protected:
	void run();
	bool isStopped();

  protected:
	mutable QMutex mMutex;
	bool mStop;
	bool mPause;
	BluetoothController& mController;
};

class BluetoothDeviceRecoveryThread : public QThread
{
	Q_OBJECT

  public:
	BluetoothDeviceRecoveryThread(BluetoothController& controller);
	~BluetoothDeviceRecoveryThread();
	
    void stop();
    std::queue<DWORD64> mMACQueue;


  protected:
	void run();
	bool isStopped();

  protected:
	mutable QMutex mMutex;
	bool mStop;
	bool mPause;
	BluetoothController& mController;
};

