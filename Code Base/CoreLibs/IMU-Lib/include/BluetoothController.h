//____________________________________________________________________________ 
// 
// (c) 2019, All Rights Reserved 
// 
// The contents of this file may not be disclosed, copied or duplicated in 
// any form, in whole or part, without the prior written permission of 
// Mohamed Mahfouz. 
//____________________________________________________________________________ 
// 
// Author: Dan Rebello (dan@streamserverlabs.com)
// Date: 2019/10/13
//____________________________________________________________________________
#pragma once

#include <windows.h>
#include <QObject>
#include <QStringList>
#include <fstream>
#include "BluetoothImuDevice.h"
#include "BluetoothCommunicationThread.h"
#include "BluetoothStatusThread.h"
#include "CommonDefinitionsIMU.h"
#include "apitypes.h"

/* Provides Bluetooth Low Energy support through BLEServer */

class BluetoothController : public QObject
{ 
    Q_OBJECT

  public:

    BluetoothImuDevice* addDevice(DWORD64 macAddr, bd_addr btaddr, const char* name);
    void removeDevice(DWORD64 macAddr);
	typedef map<DWORD64, BluetoothImuDevice*> DeviceMap;			        ///< Defines DeviceMap as a map of BluetoothImuDevice* objects
	std::queue<BleDataPacket> mBleStorageQueue;								///< Queue containing all of the received IMU data packets before processing
	BleDataPacket* mCurrentDataPacket;										///< Pointer to the BleDataPacket structure contained in the IoHandler and used for passing data to the processing thread
    std::ifstream mConfigFileHandle;
    vector<DWORD64> mAllowedMACS;

	/// Flags configuring how the BBL will operate
	enum Flags {
		/// FL_AUTO_PILOT controls whether the controller will issue
		/// commands automatically.  This is enabled by default. If
		/// this is disabled then the client of the controller is
		/// responsible for issuing the appropriate commands in the
		/// various signals that are emitted.
		FL_AUTO_PILOT = (1 << 0),

		/// FL_RECONNECT_ON_DISCONNECT controls whether devices will
		/// be reconnected automatically when they become disconnected.
		/// If a disconnect command is issued to the device, then the
		/// device will not be reconnected.
		FL_RECONNECT_ON_DISCONNECT = (1 << 1),

		/// FL_DEBUG controls whether the controller will display debug
		/// information to stdout.
		FL_DEBUG = (1 << 2),
	};
//public:
	/// Bluetooth controller state enumeration
	enum State {
		ALL_SENSORS_DISCONNECTED,
		FINDING_SENSORS,
		CONNECTING_SENSORS,
		ALL_SENSORS_CONNECTED,
		LISTENING_MEASUREMENTS,
		//---------------
		STATE_COUNT
	};

	/// Command states sent from the VV widget
	enum BLE_VV_COMMAND
	{
		SCAN = 1,
		RELINK_ALL = 2,
		TERMINATE_ACTIVE_LINKS = 3,
		TERMINATE_LINK_REQUESTS = 4,
		START_IMU_STREAMING_ALL = 5,
		STOP_IMU_STREAMING_ALL = 6,
		REINITIALIZE_DONGLE = 7,
		RESET_DONGLE = 8,
		CHANGE_DATA_PACKET_FORMAT = 9,
		CHANGE_DEVICE_LED_STATE = 10,
		LOAD_DEVICE_CALIBRATION_DATA = 11,
		SAVE_DEVICE_CALIBRATION_DATA = 12,
		DEVICE_SOFTWARE_RESET = 13,
		RELINK_DEVICE = 14,
		TERMINATE_LINK_DEVICE = 15,
		PERMANENT_LOCKDOWN_DEVICE = 16,
		SEND_BLE_COMMAND = 17,
		START_IMU_STREAMING_DEVICE = 18,
		STOP_IMU_STREAMING_DEVICE = 19,
		CHANGE_STATE_CAL_DEV_ADDRESS_LOCKDOWN = 20,
		CHANGE_STATE_DEV_STRING_FILTERING = 21
	};

	/// BLE error codes
	enum ErrorCode {
		ERR_NONE = 0,
		ERR_OPENING_PORT_FAILED,
		ERR_SCAN_FAILED,
		ERR_SET_SCAN_PARAMETERS_FAILED,
		ERR_BAD_SERVICE_UUID,
		ERR_DEVICE_NOT_FOUND,
		ERR_DEVICE_ALREADY_CONNECTED,
		ERR_DEVICE_NOT_CONNECTED,
		ERR_DEVICE_CONNECT_FAILED,
		ERR_DEVICE_DISCONNECT_FAILED,
		ERR_DEVICE_CONNECTION_UPDATE_FAILED,
		ERR_DEVICE_READ_FAILED,
		ERR_DEVICE_WRITE_FAILED,
		ERR_FINDING_SERVICES_FAILED,
		ERR_SERVICE_NOT_FOUND,
		ERR_FINDING_CHARACTERISTICS_FAILED,
		ERR_CHARACTERISTIC_NOT_FOUND,
		ERR_CHARACTERISTIC_NOT_INITIALIZED,
		ERR_AUTHENTICATION_FAILED,
		ERR_PASSKEY_MISSING,
		//---------------
		ERROR_CODE_COUNT
	};


  public:

	/// Constructor
	BluetoothController();

	/// Destructor
	~BluetoothController();

	/// Get the current state for all flags as an 8 bit unsigned int
	/// \returns an 8 bit unsigned int with the current flag states
	uint8 flags() const;

	/// Set the current state for all flags by passing in an 8 bit unsigned int
	/// \param[in] flags: 8 bit unsigned int representing the new states for the Bluetooth Controller flags
	void setFlags(uint8 flags);

	/// Adds flags to the current flag state by bitwise OR-ing the passed in data
	/// \param[in] flags: 8 bit unsigned int containing the new flags to be enabled
	void addFlags(uint8 flags);

	/// Removes flags from the current flag state by bitwise AND-ing the complement of the passed in data
	/// \param[in] flags: 8 bit unsigned int containing the flags to be disabled
	void removeFlags(uint8 flags);

    /// \returns boolean - successfully/unsuccessfully opened device
	bool open();

    /// Stops data streaming, disconnects BLE devices, and closes 
    /// the connection 
	void close();

	/// Get whether or not the device is open
	bool isOpen() const;

	/// Initiate a scan request
	int scan(); 

    /// Get the DeviceList (map of current BluetoothImuDevices*
    /// objects) 
	/// \returns the DeviceList containing all BLE devices (connected or disconnected) found while running the software
	//
    DeviceMap& devices();

	/// Set a new value for the connection timeout
	/// \param[in] seconds: an integer containing the new connection timeout in seconds
	void setConnectionTimeout(int seconds);

	/// Get the current BluetoothController state
	/// \returns the current state of the BLE controller as a BluetoothController::State enumeration
	State state() const;

	/// Get the minor firmware version number for this Bluegiga dongle (controller)
	/// \returns the minor firmware version number for this Bluegiga dongle (controller)
	unsigned short minorVersion() const;

	/// Get the major firmware version number for this Bluegiga dongle (controller)
	/// \returns the major firmware version number for this Bluegiga dongle (controller)
	unsigned short majorVersion() const;

	/// Get the hardware version number for this Bluegiga dongle (controller)
	/// \returns the hardware version number for this Bluegiga dongle (controller)
	unsigned short hardwareVersion() const;

	/// Get the build version number for this Bluegiga dongle (controller)
	/// \returns the build version number for this Bluegiga dongle (controller)
	unsigned short buildVersion() const;

	/// Get the link layer version number for this Bluegiga dongle (controller)
	/// \returns the link layer version number for this Bluegiga dongle (controller)
	unsigned short linkLayerVersion() const;

	/// Get the protocol version number for this Bluegiga dongle (controller)
	/// \returns the protocol version number for this Bluegiga dongle (controller)
	unsigned short protocolVersion() const;

	/// Get the model name for this Bluegiga dongle (controller)
	/// \returns the model name as a QString for this Bluegiga dongle (controller)
	QString modelName() const;

	/// Clears the queue containing the BleDataPackets waiting to be processed
	void emptyBleDataPacketQueue();

	bool isUsingActiveScanning() const;

    HANDLE mCmdPipeHandle;
    bool mCmdPipeOpen;

	int GetBlePacket();
	void SetBleDataPacketPointer(BleDataPacket *dataPacket);
	BluetoothCommunicationThread& communicationThread();

	static const char* errorString(int code);
	bool reset(bool soft = true);
	void clearDevices();
	void stopThreads();
	void updateStreamingModeState();
	bool inStreamingMode() const;
	bool isInDisconnectedState() const;
    BTPipeMsgReturnCode SendPipeRequestAsync(HANDLE pipeHandle, const BTPipeMessage *msg);
    BTPipeMsgReturnCode SendPipeRequest2(const BTPipeMessage *msg, DWORD timeout);

signals:
	void errorOccurred(int errorCode); // An error occurred.
	void scanning(); // A scan was started.
	void deviceAdded(const BluetoothImuDevice& device);
	void deviceRemoved(const BluetoothImuDevice& device);
	void deviceListChanged();
	void stateChanged(int);
	void newDataPacketReady();
	void addNewDevice(QString, DWORD64, QString, int, bool);
	void updateBleDeviceString(QString, int, QString, int, bool);
	void newDeviceState(DWORD64);
	void newControllerState(int);
	void updateLedStateGui();
	void clearGuiDeviceList();
	void scanComplete(); // A scan was completed and the desired number of devices were found.
	void scanCanceled(); // A scan was canceled.
	void deviceFound(const BluetoothImuDevice& device); // A device was found during a scan.
	void controllerInfoChanged();  // Dongle's information has changed.

	void packetReceived(struct ble_header hdr);
	void packetSent(struct ble_header hdr);
	void stateChanged(State s);
	void deviceLockedDown();
    void done(int rc);

public slots:
	/// Slot called to terminate BLE links, kill all threads, and close the COM port when the application is about to quit
	virtual void endAll();
	/// Slot called when a BLE device is permanently locked down
	virtual void slotDeviceLockedDown();

public:
	void setState(State s);             // Current controller state
    BluetoothImuDevice* BluetoothController::findDeviceByIdx(int idx);
protected:
	BluetoothImuDevice* findDeviceByAddress(bd_addr address);
	BluetoothImuDevice* findDeviceByHandle(uint handle);
	int  receivePacket(BluetoothImuDevice *device);
	static void sendPacket(void* state, uint8 len1, uint8* data1, uint16 len2, uint8* data2);
	int BLEDisconnectCmdPipe();
	friend class BluetoothCommunicationThread;
    friend class BluetoothReadThread;
    friend class BluetoothDeviceRecoveryThread;
	friend class BluetoothStatusThread;
	friend class BluetoothImuDevice;
  protected:
	State mState;
	unsigned short mMinorVersion;
	unsigned short mMajorVersion;
	unsigned short mHardwareVersion;
	unsigned short mBuildVersion;

	QString mModelName;
	QString mTraceOutputFilename;
    bool mGoingDown;
	bool mInStreamingMode;
	mutable QMutex mMutex;														///< Mutex for multithreaded locking and unlocking of this device

	uint8 mFlags;
	BluetoothCommunicationThread mCommunicationThread;
    BluetoothReadThread mReadThread;
    BluetoothDeviceRecoveryThread mDeviceRecoveryThread;
	BluetoothStatusThread mStatusThread;
    BluetoothSrvrPipe mPipe;
public:
    DeviceMap mDevices;
};

inline uint8 BluetoothController::flags() const
{
	return mFlags;
}

inline unsigned short BluetoothController::minorVersion() const
{
	return mMinorVersion;
}

inline unsigned short BluetoothController::majorVersion() const
{
	return mMajorVersion;
}

inline unsigned short BluetoothController::hardwareVersion() const
{
	return mHardwareVersion;
}

inline unsigned short BluetoothController::buildVersion() const
{
	return mBuildVersion;
}

inline QString BluetoothController::modelName() const
{
	return mModelName;
}

inline BluetoothController::State BluetoothController::state() const
{
	return mState;
}


int bdaddr_compare(bd_addr first, bd_addr second);
std::string bdaddr2string(bd_addr bdaddr);
