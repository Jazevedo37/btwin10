//____________________________________________________________________________ 
// 
// (c) 2015, Mohamed Mahfouz. All Rights Reserved 
// 
// The contents of this file may not be disclosed, copied or duplicated in 
// any form, in whole or part, without the prior written permission of 
// Mohamed Mahfouz. 
//____________________________________________________________________________ 
// 
// Author: Mike Kuhn
// Date: 2015/04/02
//____________________________________________________________________________

#ifndef T_BLE_VV_WIDGET_H
#define T_BLE_VV_WIDGET_H

#include "ui_BleDialogVV.h"
#include "BluetoothImuDevice.h"
#include "BluetoothController.h"
#include "IoHandlerController.h"

#include <QDialog>
#include <QBoxLayout>
#include <QCheckBox>
#include <QComboBox>
#include <QPushButton>
#include <QLabel>
#include <QWidget>
#include <QTimer>
#include <QDir>
#include <QDateTime>
#include <QMutex>
#include <time.h> 

using namespace std;
/** \brief Bluetooth Low Energy verification and validation widget to provide GUI status, interaction, and feedback as the system handles BLE related functionality.
*/
class TBleVvWidget : public QDialog
{
	Q_OBJECT

public:

	/// Stores the BLE device LED states. Needed if the dongle has to reconnect
	struct DeviceLedState {
		int ledState;
		QString address;
	};

	/// Constructor
	/// \param[in] parent: pointer to parent widget
	/// \param[in] f: window flags
	/// \param[in] resourcesPath: resources path
	TBleVvWidget(QWidget* parent = 0, Qt::WindowFlags f = 0, const QString& resourcesPath = "");
    int GetListRow(DWORD64 mac);
    void RemoveDevice(int row);

	/// Destructor
	~TBleVvWidget();

	/// Utility function to clear all devices from BleVvWidget
	/// Needed when reinitializing Bluegiga (includes rescanning)
	void ClearDevices();

    typedef map<DWORD64, QListWidgetItem *> MACToQListWidgetMap;
    
    MACToQListWidgetMap     mMACToQListWidgetItem;
	Ui_Dialog				mUI;							///< Contains all of the GUI objects created when BleDialogVV.ui is processed by UIC						
	IoHandlerController*	mIOHandlerController;			///< Reference to the controller containing the IoHandler object in a separate thread
	QStringList				dataPassedToIoHandler;			///< Contains data to be passed to the IoHandler and returned from the IoHandler while performing various BLE actions.
	vector<QString>			mBleDeviceAddresses;			///< Stores the addresses for currently connected BLE devices
	vector<int>				mBleConnectionHandles;			///< Stores the connection handles for currently connected BLE devices
	vector<DeviceLedState>	mBleLedStates;					///< Stores the Led states for devices added to the BleVvWidget. Used when reinitializing the system.
	vector<int>				mBleDeviceStates;				///< Stores the current state for each BLE device
	vector<bool>			mBleCalibrationStates;			///< Stores whether or not each BLE device has been calibrated
	bool					mBleServerConnected;			///< Boolean specifying whether or not the BLE server is connected
	unsigned int			mNumComPortsConnected;			///< Maintains number of connected COM ports
	bool					mIsComportConnected;			///< Keeps status on whether the COM port is connected
    bool IsConnected();
    int TBleVvWidget::EnumerateImus();
    DWORD64 MACFromCurrentItem();
protected:

	/// Sets the BLE status to connected
	void SetConnected();

	/// Sets the BLE status to disconnected
	void SetDisconnected();

    /// Forces an attempt to connect to the BLE IMU not already 
    /// connected 
	void ConnectImu();

signals:

	/// Signal emitted to notify that calibration data loaded successfully for all IMUs
	void SignalImuCalibrationLoadedSuccessfully();

protected slots:

	/// Slot called if the state of mUI.checkBoxBleLeds changes. Used to turn on/off automatic LED feedback.
	virtual void LedFeedbackFlagChanged();

	/// Slot called if the state of mUI.checkBoxBleDeviceStringFiltering changes. Used to turn on/off using the filter string to filter out devices during scanning.
	virtual void EnableFilterStringDeviceLinkingFlagChanged();

	/// Slot called if mUI.pushButtonScan is clicked. Sends a scan command to the COM port thread.
	virtual void ScanForDevices();

	/// Slot called if mUI.pushButtonTerminateLinkRequests is clicked. Sends a terminate all link requests command to the COM port thread.
	virtual void TerminateAllLinkRequests();

	/// Slot called if mUI.pushButtonTerminateActiveLinks is clicked. Sends a terminate all active links command to the COM port thread.
	virtual void TerminateAllActiveLinks();

	/// Slot called if mUI.pushButtonRelinkAll is clicked. Sends a relink all command to the COM port thread to restart the automated BLE initialization process.
	virtual void RelinkAllBleDevices();

	/// Slot called if mUI.pushButtonStartImuStreaming is clicked. Sends a start IMU data streaming command to the COM port thread for all linked BLE devices.
	virtual void StartImuStreamingAllDevices();

	/// Slot called if mUI.pushButtonStopImuStreaming is clicked. Sends a stop IMU data streaming command to the COM port thread for all linked BLE devices.
	virtual void StopImuStreamingAllDevices();

	/// Slot called if mUI.pushButtonReinitializeDongle is clicked. Sends a reinitialize dongle command to the COM port thread.
	virtual void ReinitializeBleDongle();

	/// Slot called if mUI.pushButtonSendBleCommand is clicked. Sends a specific BLE command to a specific BLE device. Not yet implemented.
	virtual void SendBleCommandToDevice();

	/// Slot called if mUI.pushButtonDeviceSoftwareReset is clicked. Sends a software reset command to a specific BLE device. Not yet implemented.
	virtual void SendSoftwareResetToDevice();

	/// Slot called if mUI.pushButtonDevicePermanentLockdown is clicked. Sends a permanent lockdown command to the selected BLE device.
	virtual void SendPermanentLockdownToDevice();

	/// Slot called if mUI.pushButtonStartStreamingDevice is clicked. Sends a start IMU data streaming command to the selected BLE device.
	virtual void StartStreamingOneDevice();

	/// Slot called if mUI.pushButtonStopStreamingDevice is clicked. Sends a stop IMU data streaming command to the selected BLE device.
	virtual void StopStreamingOneDevice();

	/// Slot called if any of the LED radio buttons are clicked. Sends the new LED state to the selected BLE device.
	virtual void SendNewLedStateToDevice();

	/// Slot called if mUI.pushButtonRelinkDevice is clicked. Sends an establish link command to the selected BLE device.
	virtual void RelinkSingleDevice();

	/// Slot called if mUI.pushButtonTerminateLinkDevice is clicked. Sends a terminate link command to the selected BLE device.
	virtual void TerminateLinkSingleDevice();

	/// Slot called if any of the data format radio buttons are clicked. Changes the data format flag in the BLE Controller.
	virtual void DataFormatChanged();

	/// Slot called if mUI.pushButtonResetDongle is clicked. Sends a software reset command to the TI dongle. Not yet implemented.
	virtual void ResetBleDongle();

	/// Slot called if the currently selected item in mUI.listWidget changes. Updates the BleVvWidget GUI.
	virtual void ListViewItemChanged();

	/// Slot called from the COM port thread when the BLE Controller states changes.
	/// \param[in] newState: Int used to update the BLE Controller state.
	virtual void BleControllerStatedChanged(int newState);

    /// Slot called to add a new BLE device to mUI.listWidget.
    /// \param[in] deviceID: unsigned 64 bit Integer containing 
    /// index for this device in the BLE devices vector in the 
    /// Bluetooth controller. 
	/// \param[in] newDeviceString: QString that contains the description for the new BLE device.
	/// \param[in] deviceAddress: QString the BLE address for this device.
	/// \param[in] deviceConnHandle: Integer containing the current connection handle for this device.
	/// \param[in] connHandleExists: Boolean specifying whether or not the connection handle exists for this device.
	virtual void AddNewBleDeviceToList(QString newDeviceString, DWORD64 deviceID, QString deviceAddress, int deviceConnHandle, bool connHandleExists = false);

	/// Slot called from the COM port thread when a BLE Device's state changes.
	/// \param[in] deviceID: Int used to locate the device in the bleDevices vector in the BLE Controller.
	virtual void BleDeviceStateChanged(DWORD64 deviceID);

	/// Slot called from the COM port thread to update the BLE device description in mUI.listWidget.
	/// \param[in] newDeviceString: QString that contains the updated description for the BLE device.
	/// \param[in] deviceID: Int used to locate the device in the bleDevices vector in the BLE Controller.
	/// \param[in] deviceAddress: QString the BLE address for this device.
	/// \param[in] deviceConnHandle: Integer containing the current connection handle for this device.
	/// \param[in] connHandleExists: Boolean specifying whether or not the connection handle exists for this device.
//	virtual void UpdateBleDeviceString(QString newDeviceString, int deviceID, QString deviceAddress, int deviceConnHandle, bool connHandleExists = true);

	/// Slot called when the LED state changes for a linked BLE device. Updates the BleVvWidget GUI.
	virtual void UpdateBleDeviceLedState();

	/// Slot called when the min or max connection interval line edit objects have their text edited. 
	/// If the new text values are valid, they will be sent and stored in the BLE Controller.
	/// These values will only be used if the BLE Central reinitializes the BLE channel.
	virtual void ConnIntervalTextEdited();

	/// Slot called when the filter string text is edited (in mUI.lineEditScanFilterString). 
	/// If the use of a filter string during scanning is enabled (mUI.checkBoxBleDeviceStringFiltering), the new filter string is sent to the BLE Controller.
	virtual void ScanFilterStringTextEdited();

	/// Slot for clearing all of the device data structures. Needed if the dongle was removed or if the COM port was close for some reason.
	virtual void ClearDeviceListAsync();

	virtual void SlotRefreshRateUpdated(int index);

    virtual void quit(int rc);
};

#endif
