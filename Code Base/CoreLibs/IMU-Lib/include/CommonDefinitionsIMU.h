//____________________________________________________________________________ 
// 
// (c) 2014, Mohamed Mahfouz. All Rights Reserved 
// 
// The contents of this file may not be disclosed, copied or duplicated in 
// any form, in whole or part, without the prior written permission of 
// Mohamed Mahfouz. 
//____________________________________________________________________________ 
// 
// Author: Mike Kuhn
// Date: 2014/05/15
//____________________________________________________________________________

#define T_IS_LOGGING_TRACE  1

#ifndef T_COMMON_DEFINITIONS_IMU_H
#define T_COMMON_DEFINITIONS_IMU_H

#define LSB									0.00000063690636167407
#define FSR									7864320
#define LSB_4thGen							5.035400390625000e-05
#define MAG_SENSITIVITY						1.0						///< Magnitometer sensitivity 
#define GYRO_SENSITIVITY					0.0152587890625			///< Gyrometer sensitivity 
#define ACCEL_SENSITIVITY					0.0000608774			///< Accelorometer sensitivity 
#define ACCEL_SENSITIVITY_BLE				0.00006103515625		///< BLE board accelerometer sensitivity
#define sm_frame							10
#define NUM_VALUES_FOR_AG_CAL				50						///< Number of samples for Accelorometer/Gyrometer calibration
#define PACKET_LENGTH_3RD_GEN				54						///< 3rd generation packet length
#define PACKET_LENGTH_4TH_GEN				43						///< 4th generation packet length
#define UART_BUFFER_SIZE					10000					///< UART buffer size
#define MAX_NUMBER_IMUs						20						///< Maximum number of IMUs
#define SMALL_VAL_GYRO						0.1
#define CHANNEL_DATA_SIZE_3RD_GEN			15
#define CHANNEL_DATA_SIZE_4TH_GEN			9
#define UNIQUE_ID_SIZE						17
#define IMU_CONFIG_SIZE						5
#define K_P									25						///< Default proportional gain
#define K_I									5						///< Default integral gain
#define K_P_BLE								25						///< Default proportional gain BLE
#define K_I_BLE								5						///< Default integral gain BLE
#define SIGNAL_DATA_SIZE					9						///< Input data size (3 for Magnitometer, 3 for Gyrometer, 3 for Accelorometer)
#define ACCEL_LPF_BUFFER_SIZE				3
#define GYRO_LPF_BUFFER_SIZE				3
#define MAG_LPF_BUFFER_SIZE					3
#define ACCEL_DELAY_BUFFER_SIZE				0
#define NUM_VALUES_FOR_PSI_REG				50						///< Number of samples for PSI registration
#define DT_MIN_REFRESH_RATE					12
#define DT_MAX_REFRESH_RATE					300
#define DEFAULT_DT_VAL						0.025
#define IN_BLE_MODE							1
#define PACKET_LENGTH_BLE_BULBOSAUR			31
#define BLE_LARGE_BUFFER_SIZE				1010
#define INITIAL_NUM_PACKETS_BLE_VEC			400
#define BLE_MAX_PACKET_SIZE					500
#define NUM_ITER_BLE_SCANNING				8
#define BYTES_TO_READ_BLE_NORMAL			400
#define BYTES_TO_READ_BLE_LARGE				1000
#define SLEEP_SECONDS_ESTABLISH_LINK		2
#define SLEEP_SECONDS_POST_ESTAB_LINK		3
#define SLEEP_SECONDS_SCANNING_ITER			1
#define SLEEP_SECONDS_AFTER_PASSKEY			3
#define SLEEP_SECONDS_AFTER_GAP_AUTH		1
#define NUM_RAW_DATA_VALS_BLE				27
#define BAUD_RATE_4TH_GEN_IMU				921600					///< COM port baud rate for 4th gen IMU system (CC2500 transceiver)
#define BAUD_RATE_BLE_DONGLE				112500					///< COM port baud rate for BLE IMU system (CC2541 transceiver)
#define NUM_SAMPLES_DETECT_BLE_MOTION		10						///< Number of samples to use in calculating the standard deviation to detect BLE motion
#define ACCEL_THRESHOLD_BLE_MOTION			150						///< Threshold of the standard deviation for detecting motion in the iNEMO accelerometers.
#define GYRO_THRESHOLD_BLE_MOTION			50						///< Threshold of the standard deviation for detecting motion in the iNEMO gyrometers.
#define GYRO_THRESHOLD_VAL_ABOVE_MEAN_NOISE	10						///< Increased value above the mean gyro standard deviation value calculated while the IMU is stationary for detecting motion						
#define NUM_GYRO_SAMPLES_TO_COLLECT_BLE		50						///< Number of gyro samples to collect at the beginning of the automated BLE calibration process
#define LENGTH_SECONDS_MAG_CAL_BLE			80						///< Number of seconds that the mag calibration procedure lasts for automated BLE calibration
#define MIN_MAG_CAL_TIME_SECONDS_BLE		60						///< Minimum amount of time for the automated magnetic calibration that must be reached before changing states
#define MIN_ACCEL_CAL_TIME_SECONDS_BLE		15						///< Minimum amount of time for the automated accel calibration that must be reached before changing states
#define LENGTH_SECONDS_ACCEL_CAL_BLE		24						///< Number of seconds that the accel calibration procedure lasts for automated BLE calibration
#define LENGTH_SECONDS_PAUSE_CAL_BLE		2						///< Number of seconds of no motion (pause) between mag and accel calibration procedure during automated BLE calibration
#define MIN_POINTS_MAG_CAL_BLE				2000					///< Minimum number of mag data points that need to be collected per IMU during the automated calibration process. Not currently being used as a constraint
#define MIN_POINTS_ACCEL_CAL_BLE			600						///< Minimum number of accel data points that need to be collected per IMU during the automated calibration process. Not currently being used as a constraint
#define USE_LPF_ACCEL_GYRO_DATA_BLE_CAL		1						///< Flag specifying whether or not the accel and gyro data should go through a LPF during automated BLE calibration
#define USE_LPF_MAG_DATA_BLE_CAL			1						///< Flag specifying whether or not the mag data should go through a LPF during automated BLE calibration
#define	PROCESS_ONLY_TWO_BLE_PACKETS		1						///< Flag specifying whether or not only two data packets per BLE IMU should be collected. Testing is undergoing with the BLE firmware and having multiple BLE devices connected
#define NUM_DATA_VALS_BLE_PACKET			27						///< Number of data values in a BLE packet
#define USE_ONE_SAMPLE_LPF_BLE				0						///< Flag specifying to use the one sample low pass filter rather than the two sample low pass filter
#define PROCESS_ONLY_ONE_BLE_PACKET			0						///< Flag specifying whether or not to process only one BLE data packet. Only this flag or the two packet processing flag should be set at one time. 
#define ENABLE_V_AND_V_WINDOW				1						///< Flag specifying whether or not the V&V window is enabled in the Initialization Widget
#define CAL_DATA_FILE_MIN_NUM_COLUMNS		31						///< Minimum number of columns that are present in the calibration data files (csv files)
#define CAL_DATA_FILE_COLUMN_OFFSET1		1						///< Number of columns (contains raw data) before getting to the finalized calibration correction values for num columns option 1			
#define CAL_DATA_FILE_NUM_COL1				31						///< One option for the number of columns present in a calibration data file (option 1)
#define CAL_DATA_FILE_COLUMN_OFFSET2		42						///< Number of columns (contains raw data) before getting to the finalized calibration correction values for num columns option 2
#define CAL_DATA_FILE_NUM_COL2				72						///< One option for the number of columns present in a calibration data file (option 2)
#define CAL_DATA_FILE_COLUMN_OFFSET3		43						///< Number of columns (contains raw data) before getting to the finalized calibration correction values for num columns option 3
#define CAL_DATA_FILE_NUM_COL3				73						///< One option for the number of columns present in a calibration data file (option 3)
#define	CAL_DATA_FILE_ALIGN_MAG_COL_NUM		99						///< Number of columns in a calibration file containing the aligned mag data affine transformations
#define	CAL_DATA_FILE_NUM_COL_MAY_2016		127						///< Number of columns in a calibration file for latest Cubit processing revamp in May of 2016
#define CAL_DATA_MIN_NUM_DATA_COL			25						///< Minimum number of columns in a data row of the calibration file
#define	TARGET_NUM_BLE_IMUS_NEEDED			2						///< Number of BLE IMUs needed to use Navigator for surgical navigation
#define PACKET_SIZE_BLE_LINK_TIMEOUT		9						///< Packet size for a BLE terminate link timeout
//#define MIN_NUM_BLE_IMUS_NEEDED				1						///< Minimum number of BLE IMUs needed to proceed. Only used when the system is locking down and terminating.
#define MIN_BLE_MIN_CONN_INTERVAL_HEX		6						///< Minimum allowable BLE min connection interval
#define MAX_BLE_MIN_CONN_INTERVAL_HEX		80						///< Maximum allowable BLE min connection interval
#define MIN_BLE_MAX_CONN_INTERVAL_HEX		14						///< Minimum allowable BLE max connection interval
#define MAX_BLE_MAX_CONN_INTERVAL_HEX		80						///< Maximum allowable BLE max connection interval
#define LED_FEEDBACK_TIMEOUT_VAL			2						///< Number of GUI update cycles before sending current instrument IMU a new LED value for on device feedback
#define BLE_SLEEP_AFTER_TX_COMMAND_MS		50						///< Number of milliseconds to sleep after transmitting a BLE command
#define BLE_SLEEP_AFTER_TX_CMD_LONG_MS		350						///< Number of milliseconds to sleep after transmitting a BLE command
#define BLE_FILTER_STRING_SIZE				19						///< Number of ascii characters in the default BLE filter string used to filter devices during scanning
#define ACCEL_BIAS_OPTIMIZATION_THRESH		1E-6					///< Threshold used to determine when the accelerometer bias optimization has converged
#define MAX_NUM_ITER_ACCEL_BIAS_OPT			20						///< Maximum number of cycles for optimizing the accelerometer biases
#define USE_SECOND_ORDER_LPF_PARAMS			1						///< Flag specifying whether or not to use second order low pass filter parameters (versus first order)
#define SECOND_ORDER_LPF_BUFFER_SIZE		3						///< Buffer size for the second order accel low pass filter
#define SAVITZKY_GOLAY_BUFFER_SIZE			17						///< Buffer size for the Savitzky Golay filter
#define USE_BLUEGIGA_DONGLE					1						///< Flag specifying whether or not to use the Bluegiga dongle
#define DATA_QUEUE_MAX_SIZE					1000					///< Maximum number of values in the IMU data queue before it is emptied
#define APPLY_LPF_AFTER_SG_BEFORE_PROC		1						///< Flag specifying whether or not to apply the LPF after the Savitzky Golay filter but before processing the data
#define BLE_DISCONNECTED_TIMEOUT			35						///< Number of seconds of being in a disconnected state before the Bluegiga dongle should be reinitialized
#define THRESHOLD_STATIC_MAG_DIST			0.10					///< Threshold of mag vector magnitude below/above which is considered to have static distortion
#define	THRESHOLD_MAG_CAL_FAILED			5						///< Threshold for failing the magnetic calibration expressed as a percentage of mag cal points with static field distortion
#define PUSH_DATA_MAG_VER_WIDGET			1						///< Flag specifying whether or not various calibration data should be calculated and pushed to the mag verification widget in the GUI
#define USE_ELLIPSOID_CALIBRATION			1						///< Flag specifying whether or not to use the ellipsoid method when finalizing the IMU calibrations
#define NUM_SAMPLES_LIVE_MOTION_DETECT		16						///< Number of samples to use for live IMU motion detection
#define NUM_SAMPLES_LIVE_BUFFERS			17						///< Number of samples stored in the live mag, gyro, and accel buffers
#define	MIN_GYRO_STD_DEV_DETECT_THRESH		0.025					///< Minimum gyro threshold of the standard dev for motion detection
#define MAX_GYRO_STD_DEV_DETECT_THRESH		0.1						///< Maximum gyro threshold of the standard dev for motion detection
#define	MIN_GYRO_MEAN_DETECT_THRESH			0.0005					///< Minimum gyro threshold of the mean for motion detection
#define MAX_GYRO_MEAN_DETECT_THRESH			0.2						///< Maximum gyro threshold of the mean for motion detection
#define	MIN_ACCEL_STD_DEV_DETECT_THRESH		0.02					///< Minimum accel threshold of the standard dev for motion detection
#define MAX_ACCEL_STD_DEV_DETECT_THRESH		0.1						///< Maximum accel threshold of the standard dev for motion detection
#define	MIN_MAG_STD_DEV_DETECT_THRESH		0.005					///< Minimum mag threshold of the standard dev for motion detection
#define MAX_MAG_STD_DEV_DETECT_THRESH		0.3						///< Maximum mag threshold of the standard dev for motion detection
#define MIN_MAG_DIFF_STD_DEV_DETECT_THRESH	0.002					///< Minimum threshold of the standard dev of the vector magnitude difference between two magnetometers, used for motion detection
#define MAX_MAG_DIFF_STD_DEV_DETECT_THRESH	0.005					///< Maximum threshold of the standard dev of the vector magnitude difference between two magnetometers, used for motion detection
#define	GYRO_STD_DEV_RANGE_SCALE_FACTOR		0.60					///< Scale factor used in calculating the gyro std dev threshold for motion detection
#define GYRO_MEAN_RANGE_SCALE_FACTOR		0.40					///< Scale factor used in calculating the gyro mean threshold for motion detection
#define	ACCEL_STD_DEV_RANGE_SCALE_FACTOR	0.60					///< Scale factor used in calculating the accel std dev thresholds for motion detection
#define	MAG_STD_DEV_RANGE_SCALE_FACTOR		0.40					///< Scale factor used in calculating the accel std dev thresholds for motion detection
#define	NUM_STD_DEV_MAG_DIFF_THRESH			4						///< Number of standard deviations to make the threshold of the vector magnitude difference between two magnetometers
#define	NUM_STD_DEV_MAG_VEC_LENGTH_DIST		6						///< Number of standard deviations to create a sigma value that is ultimately used in creating the cost function for magnetic disturbance detection
#define	NUM_STD_DEV_MAG_DIST_COST_FUNC		5						///< Number of standard deviations used in calculating the threshold where the cost function will report magnetic disturbance detection
#define	NUM_SAMPLES_MAG_DIST_TIMEOUT		20						///< Number of data samples that must be received without mag distortion for the IMU to return to an undistorted state
#define NUM_MOTION_ANOMALY_SAMPLES_THRESH	1						///< Number of samples of motion anomaly data collected within the moving window to be flagged as mag distortion due to a motion anomaly
#define MIN_MAG_SANITY_CHECK_CAL_TIME_SEC	3						///< Minimum amount of time for the automated mag sanity check calibration that must be reached before changing states
#define	BLE_PASSKEY							123114					///< Passkey used for pairing/authenticating BLE devices
#define PROMPT_USER_MAG_CAL_SANITY_FAILED	0						///< Defines whether or not to prompt the user to proceed if mag cal sanity check fails
#define NUM_OF_AVERAGED_QUAT				25						///< Number of quaternions used for averaging (display)
#define NUM_OF_AVERAGED_IMU_DIR_VECS		7						///< Number of current IMU direction vectors used for averaging (display)
#define NUM_OF_AVERAGED_FINAL_ANGLE_VALS	5						///< Number of final app specific angles used for averaging (display)
#define VIEWER_UPDATE_INTERVAL_MS			100						///< Number of milliseconds between transformation updates for the 3-D viewers
#define CHECK_COM_PORT_INTERVAL_MS			1000					///< Number of milliseconds between checks of the COM port status
#define NUM_ITERS_COM_PORT_RESET			2						///< Number of timer timeout iterations (i.e., seconds) to wait before resetting the COM port
#define NUM_ITERS_IMU_TIMEOUT				0						///< Number of timer timeout iterations (i.e., seconds) to wait before timing out the IMU
#define NUM_ITERS_BEFORE_INSTRUMENT_CHANGE  8						///< Number of filtered quaternion updates (i.e., viewer updates) required before automatically switching instruments
#define ACCEL_THRESH_ONE					0.9						///< Accel threshold to determine if a given direction is at one (aligned with gravity)
#define ACCEL_THRESH_ZERO_UPPER				0.2						///< Upper accel threshold to determine if a given direction is at zero (orthogonal to gravity)
#define ACCEL_THRESH_ZERO_LOWER				-0.2					///< Lower accel threshold to determine if a given direction is at zero (orthogonal to gravity)
#define ACCEL_THRESH_MINUS_ONE				-0.9					///< Accel threshold to determine if a given direction is at minus one (aligned opposite to gravity)
#define NUM_DATA_POINTS_INSTRUMENT_CAL		25000					///< Number of data points to generate for the instrument cal normal vs. distorted datasets
#define MAG_DIST_COST_FUNC_SCALE_VAL		1000					///< Scale value used in applying the magnetic distortion cost function
#define MAG_DIST_COST_FUNC_THRESH_MIN		0.012					///< Minimum threshold value for the mag distortion detection threshold calculated during IMU calibration
#define	PRE_PROCESS_FILTER_STEADY_STATE		40						///< Number of samples to preprocess to ensure SG filters and buffers are in a steady state condition. Required in data calibration/reprocessing.
#define COMPILE_FOR_DELL_VENUE_TABLET		0						///< Flag used to specify whether or not to compile IMU-Lib for the Dell Venue 5055 tablet
#define BLE_DISCONNECTED_TIMEOUT_TABLET		200						///< Timeout in seconds for BLE disconnection when running Cubit on the Dell Venue 5055 tablet	
#define MAG_DIST_CF_THRESH_SCALE_FACTOR		1.1						///< Scale factor used in calculating the final magnetic distortion cost function threshold
#define SLEEP_WAITING_PERM_LOCKDOWN_MS		3000					///< Sleep time in milliseconds the system should wait before exiting when putting the IMUs into permanent lockdown
#define FIRST_ORDER_FILTER_BUFFER_SIZE		2						///< Number of typical elements in the buffer for a standard first order filter
#define SECOND_ORDER_FILTER_BUFFER_SIZE		3						///< Number of typical elements in the buffer for a standard second order filter
#define NUM_SIGMA_MAG_NORM_FILTERING		5						///< Number of standard deviations to use in filtering out erroneous mag norm data
#define NUM_SIGMA_QUAT_DIFF_SPIKE_FILTER	3						///< Number of standard deviations to use in filtering out spikes from the quaternion difference data collected during the mag sanity check
#define QUAT_DIFF_THRESH_OFFSET_DEGREES		5						///< Number of degrees offset above the max value measured during the mag sanity check portion of the IMU calibration procedure. Used in mag distortion detection.
#define ADJUSTED_DIP_ANGLE_OFFSET_RADIANS	0.13					///< Offset in radians for the adjusted dip angle to calculate the thresholds used in live mag distortion detection
#define NUM_SAMPLES_SENSOR_HEALTH_CHECK		30						///< Number of samples that must be exceeded when checking the state of health of a sensor to determine if an IMU is still operational
#define UNALIGNED_MAG_NORM_OFFSET			0.10					///< Offset in normalized units used to detect mag distortion anomalies that cause the norm of the unaligned processed mag data to exceed typical undistorted operating bounds
#define MAG_ELLIPSE_RADII_ANOMALY_LB		0.93					///< Lower bound used to detect mag distortion through the ellipsoid fitting radius anomaly test
#define MAG_ELLIPSE_RADII_ANOMALY_UB		1.07					///< Upper bound used to detect mag distortion through the ellipsoid fitting radius anomaly test	
#define MIN_GYRO_STD_DEV_THRESH_SPIKE		500						///< Minimum threshold used to detect motion spikes that occur when the cal unit stops during the mag sanity calibration steps within the automated IMU calibration procedure
#define OUTPUT_QUAT_EXP_AVE_FILT_WEIGHT		0.5						///< Weight used in applying an exponential average to the display/output quaternions
#define MAX_NUM_POD_CONNECTION_ATTEMPTS		4						///< Maximum number of connection attempts that can be tried before the software aborts
#define COUNTDOWN_WAIT_FOR_APP_TO_EXIT_MS	20000					///< Countdown used to allow the app to connect to BLE devices, place them in lockdown, and exit gracefully
#define WAIT_FOR_IMUS_TO_INITIALIZE_MS		1000					///< Time in ms used to wait for IMUs to reinitialize before starting a new IMU calibration
#define IMU_CALIBRATION_TIMEOUT_SEC			180						///< Timeout in seconds used to end an IMU calibration attempt if the data collection process cannot be completed
#define	NUM_SAMPLES_ACCEL_MEDIAN_FILTER		6						///< Number of samples to use in running the accelerometer median filter
#define NUM_SAMPLES_FIND_OPERATIONAL_ACCEL	5						///< Number of samples to store when finding which (if any) of the two accelerometers is operational

// Start Of IMU COM Port Packet
#define CPT_SOP2 'Z' // 0x0D carriage return

#endif // T_COMMON_DEFINITIONS_IMU_H