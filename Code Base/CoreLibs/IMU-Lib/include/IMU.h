//____________________________________________________________________________ 
// 
// (c) 2014, Mohamed Mahfouz. All Rights Reserved 
// 
// The contents of this file may not be disclosed, copied or duplicated in 
// any form, in whole or part, without the prior written permission of 
// Mohamed Mahfouz. 
//____________________________________________________________________________ 
// 
// Author: Michael Kuhn
// Date: 2014/04/20
//____________________________________________________________________________

#ifndef IMU_H
#define IMU_H

#include <QString>

#define ENABLE_LOG 0

#if 0
class IMU
{
public:

	/// Default constructor
	IMU();

	/// Destructor
	~IMU();

	IMU& operator =(const IMU& imu);

public:

	bool			mIsBleIMU;						///< Flag specifying whether or not this is a Bluetooth low energy IMU
	int				mBleID;							///< Index linking this IMU to its BluetoothLowEnergyDevice in the main vector contained in the BluetoothLowEnergyController class
	unsigned int	mImuID;							///< IMU ID as programed in the firmware
	int				mBleLedState;					///< Stores the state of the LED for this IMU for a BLE device.
	QString			mBleDevAddress;					///< Stores the BLE device address for this BLE IMU
	bool			mIsConnected;					///< Flag specifying whether or not this IMU is connected and streaming data
	int				mRSSI;							///< Current received signal strength for this BLE IMU
	unsigned int	mRefreshRate;					///< Stores refresh rate for this IMU
	unsigned int	mIterRefreshRate;				///< Iterator used for calculating refresh rate
	unsigned int	mMainIMUVectorIndex;			///< Stores index of this IMU in the main IMU vector. Needed since IoHandler now exists in its own thread.
	unsigned int	mDisplayPos;					///< Stores IMU connection index (e.g., first to connect, 2nd, 3rd, etc.)
	bool			mHaveAddedImuToGui;				///< Flag used to log whether or not this IMU has been added to the vector of IMUs 
	bool			mbIsAutomatedCalibrationComplete;///< Flag specifying whether or not the automated calibration is complete. Used in applying the calibration data to incoming raw data
	bool			mbHasStreamingDataTimedOut;		///< Flag specifying whether or not the streaming data timeout has occurred (happens after one second)
	bool			mbHasStreamingDataStopped;		///< Flag specifying whether or not data streaming for this IMU has stopped after calibration
	int				mStreamingDataTimeoutIter;		///< Iterator used for raising the streaming data timeout flag for detecting when an IMU stops streaming data
	double			mDt;							///< Stores the current time step between filtering of two consecutive IMU data packets
	unsigned int	mAliveTimerCounter;				///< Counter used in tracking IMU timeout
};
#endif
#endif // IMU_H