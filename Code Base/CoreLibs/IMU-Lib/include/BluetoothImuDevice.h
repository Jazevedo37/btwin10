//____________________________________________________________________________ 
// 
// (c) 2015, Mohamed Mahfouz. All Rights Reserved 
// 
// The contents of this file may not be disclosed, copied or duplicated in 
// any form, in whole or part, without the prior written permission of 
// Mohamed Mahfouz. 
//____________________________________________________________________________ 
// 
// Author: Joe Marrero (jmarrero@adextechnologies.com)
// Date: 2015/04/25
//____________________________________________________________________________
#pragma once

#include <QObject>
#include <QString>
#include <fstream>
#include "apitypes.h"
#include "cmd_def.h"
#include "..\..\..\BLEServer\pipeprot.h"

class BluetoothController;


extern const uint8 PRIMARY_SERVICE_UUID[];										///< UUID of any "primary service" entry is 0x2800, used when searching for services
extern const uint8 SECONDARY_SERVICE_UUID[];									///< UUID of any "secondary service" entry is 0x2801

extern const uint8 STREAMING_NOTIFICATIONS_ENABLE[];							///< Value for enabling data streaming notifications
extern const uint8 STREAMING_NOTIFICATIONS_DISABLE[];							///< Value for disabling data streaming notifications


extern const uint8 LED_STATE_ALL_OFF[];											///< Stores the value for turning off all LEDs
extern const uint8 LED_STATE_RED_ON[];											///< Stores the value for turning on the red LED
extern const uint8 LED_STATE_YELLOW_ON[];										///< Stores the value for turning on the yellow LED
extern const uint8 LED_STATE_GREEN_ON[];										///< Stores the value for turning on the green LED
extern const uint8 LED_STATE_RED_BLINK[];										///< Stores the value for turning on the red blinking LED
extern const uint8 LED_STATE_YELLOW_BLINK[];									///< Stores the value for turning on the yellow blinking LED
extern const uint8 LED_STATE_GREEN_BLINK[];										///< Stores the value for turning on the red and yellow blinking LEDs
extern const uint8 LED_STATE_RED_YELLOW_BLINK[];								///< Stores the value for turning on the yellow and green blinking LEDs
extern const uint8 LED_STATE_YELLOW_GREEN_BLINK[];								///< Stores the value for turning on the red and green blinking s
extern const uint8 LED_STATE_RED_GREEN_BLINK[];									///< Stores the value for turning on the red and green blinking LEDs
extern const uint8 LED_STATE_ALL_BLINK[];										///< Stores the value for turning on the all blinking LEDs
extern const uint8 LED_STATE_PERMANENT_LOCKDOWN[];								///< Stores the value for putting the IMU into permanent lockdown

using namespace std;

/** \brief Defines a single Bluetooth Low Energy device using the Bluegiga dongle, which is ultimately tied to an instance of the IMU class.
*/
class BluetoothImuDevice : public QObject
{
    friend class BluetoothController;

	Q_OBJECT

  public:
	static const short INVALID_CONNECTION_HANDLE = 255;							///< Initial connection handle value. Not a valid connection handle
	static const uint16 SERVICE_INFORMATION_UUID  = 0x180a;						///< UUID for device info services
	static const uint16 SERVICE_IMU_AND_LED_UUID  = 0xFFF0;						///< UUID for IMU and LED services

	// Characteristics in the information service
	static const uint16 CHARACTERISTIC_SYSTEM_ID_UUID         = 0x2a23;			///< UUID for system ID characteristic
	static const uint16 CHARACTERISTIC_MANUFACTURER_NAME_UUID = 0x2a29;			///< UUID for manufacturer name characteristic
	static const uint16 CHARACTERISTIC_SOFTWARE_REVISION_UUID = 0x2a28;			///< UUID for software revision characteristic (e.g., 000115 Rev.A)
	static const uint16 CHARACTERISTIC_HARDWARE_REVISION_UUID = 0x2a27;			///< UUID for hardware revision characteristic (e.g., 000076 Rev.A)
	static const uint16 CHARACTERISTIC_FIRMWARE_REVISION_UUID = 0x2a26;			///< UUID for firmware revision characteristic (e.g., 000116 Rev.A)
	static const uint16 CHARACTERISTIC_SERIAL_NUMBER_UUID     = 0x2a25;			///< UUID for serial number characteristic (e.g., 2015-03-00002)
	static const uint16 CHARACTERISTIC_MODEL_NUMBER_UUID      = 0x2a24;			///< UUID for model number characteristic (e.g., ADEX-0003)

	// Characteristics in the IMU and LED service
	static const uint16 CHARACTERISTIC_MEASUREMENT_UUID      = 0xFFF4;			///< UUID for finding handle for reading IMU measurements
	static const uint16 CHARACTERISTIC_LED_UUID              = 0xFFF6;			///< UUID for finding handle for LED state commands
	static const uint16 CHARACTERISTIC_STREAMING_CONFIG_UUID = 0x2902;			///< UUID for turning data streaming on/off


	static const uint16 INITIAL_MINIMUM_CONNECTION_INTERVAL    = 20;			///< Default initial minimum connection internval in units of 1.25ms
	static const uint16 INITIAL_MAXIMUM_CONNECTION_INTERVAL    = 40;			///< Default initial maximum connection internval in units of 1.25ms
	static const uint16 INITIAL_CONNECTION_SLAVE_LATENCY       = 2;				///< Default initial connection slave latency (how many connections intervals a slave may skip)
	static const uint16 INITIAL_CONNECTION_SUPERVISION_TIMEOUT = 100;			///< Default initial connection supervision timeout in units of 10ms
	static const uint16 UPDATED_MINIMUM_CONNECTION_INTERVAL    = 6;				///< Updated minimum connection internval in units of 1.25ms
	static const uint16 UPDATED_MAXIMUM_CONNECTION_INTERVAL    = 10;			///< Updated maximum connection internval in units of 1.25ms
	static const uint16 UPDATED_CONNECTION_SLAVE_LATENCY       = 2;				///< Updated connection slave latency (how many connections intervals a slave may skip)
	static const uint16 UPDATED_CONNECTION_SUPERVISION_TIMEOUT = 100;			///< Updated connection supervision timeout in units of 10ms
	
	/// BLE device state enumeration
	enum DeviceState {
		UNINITIALIZED = 0,         // Device is uninitialized or has been terminated
		FINDING_SERVICES,          // Device is discovering services
		FINDING_CHARACTERISTICS,   // Device is initializing handles to characteristics
		__READY,                   // Device is ready for streaming and other commands
		READING,				   // Device is reading data
		WRITING,				   // Device is writing data
		//-------------------
		DEVICE_STATE_COUNT
	};																			

	/// BLE device LED state enumeration
	enum LedState
	{
		NONE = 0,
		TURN_ON_RED_LED = 1,
		TURN_ON_YELLOW_LED = 2,
		TURN_ON_GREEN_LED = 3,
		TURN_OFF_ALL_LEDS = 4,
		PUT_INTO_PERMANENT_LOCKDOWN = 5,
		TURN_ON_RED_LED_BLINKING = 6,
		TURN_ON_YELLOW_LED_BLINKING = 7,
		TURN_ON_GREEN_LED_BLINKING = 8,
		TURN_ON_ALL_LEDS_BLINKING = 9,
		TURN_ON_RED_GREEN_LEDS_BLINKING = 10,
		TURN_ON_RED_YELLOW_LEDS_BLINKING = 11,
		TURN_ON_YELLOW_GREEN_LEDS_BLINKING = 12,
		//-------------------
		LED_STATE_COUNT
	};																			

  public:

	HANDLE mDataPipe;
    bool mDataPipeOpen;
    bool mInitComplete;                                                         // Set to true when initialization complete
    DWORD64    mMACAddr;                            // BT Device MAC, and handle
	/// Constructor
	/// \param[in] ctl: pass in the main BluetoothController object
	/// \param[in] address: the Bluetooth device address for this device
	/// \param[in] addressType: Bluetooth address type
	/// \param[in] name: string containing the name of this device
	/// \param[in] rssi: initial received signal strength for this device
	BluetoothImuDevice(BluetoothController& ctl, unsigned __int64 macAddr, const bd_addr& address, gap_address_type addressType, const QString& name, int rssi);
	~BluetoothImuDevice();
    void Init();
	int BLEDisconnectDataPipe(DWORD timeout);
	
	/// Connect to this device
	/// \param[in] queue: a boolean indicating whether to place this command in the queue or asynchronously force the connection attempt
    void connect();

	/// Check whether or not this device pipe is connected
	/// \returns a boolean indicating whether or not this device is connected
	bool isPipeConnected() const;

	/// Check whether or not this device is bonded
	/// \returns a boolean indicating whether or not this device is bonded
	bool isBonded() const;

	/// Disconnect from this device
	/// \param[in] queue: a boolean indicating whether to place this command in the queue or asynchronously force the device to disconnect
	void disconnect(bool queue = true);
	
	/// Get the BLE device address for this device
	/// \returns a bd_addr containing the BLE device address
	bd_addr address() const;

	/// Get the BLE device address as a QString for this device
	/// \returns a QString containing the BLE device address
	QString addressString() const;

	/// Get the GAP address type for this device
	/// \returns a gap_address_type for this device
	gap_address_type addressType() const;

	/// Get the connection handle for this device
	/// \returns a short with the connection handle for this device
	short connectionHandle() const;

	/// Get the connection handle for this device
	/// \returns an unsigned int 8 containing the connection handle for this device
	uint8 connectionHandleUint8() const;

	/// Get the bonding handle for this device
	/// \returns a short with the bonding handle for this device
	short bondingHandle() const;

	/// Get whether or not the address for this device is public
	/// \returns a boolean indicating whether or not the address for this device is public
	bool isAddressPublic() const;

	/// Get the name of this device as a QString
	/// \returns a QString containing the name of this device
	const QString& name() const;

	/// Get the manufacturer of this device as a QString
	/// \returns a QString containing the manufacturer of this device
	const QString& manufacturer() const;

	/// Get the software revision of this device as a QString
	/// \returns a QString containing the software revision for this device
	const QString& softwareRevision() const;

	/// Get the hardware revision for this device as a QString
	/// \returns a QString containing the hardware revision for this device
	const QString& hardwareRevision() const;

	/// Get the firmware revision for this device as a QString
	/// \returns a QString containing the firmware revision for this device
	const QString& firmwareRevision() const;

	/// Get the serial number for this device as a QString
	/// \returns a QString containing the serial number for this device
	const QString& serialNumber() const;

	/// Get the model number for this device as a QString
	/// \returns a QString containing the model number for this device
	const QString& modelNumber() const;

	/// Get a const reference to the BleDataPacket structure within this device
	/// \returns a const BleDataPacket reference for this device
	const BleDataPacket& data() const;

	/// Get the LED state for this device
	/// \returns an LedState type containing the current LED state for this device
	LedState ledState() const;

	/// Queue the command to request the manufacturer information for this device
	void requestManfacturer() const;

	/// Queue the command to request the software revision information for this device
	void requestSoftwareRevision() const;

	/// Queue the command to request the hardware revision for this device
	void requestHardwareRevision() const;

	/// Queue the command to request the firmware revision for this device
	void requestFirmwareRevision() const;

	/// Queue the command to request the serial number for this device
	void requestSerialNumber() const;

	/// Queue the command to request the model number for this device
	void requestModelNumber() const;

	/// Queue the command to request measurement data for this device
	void requestMeasurement() const;

    bool isDeviceConnected() { return(mIsDeviceConnected); }
    void setConnected() { mIsDeviceConnected = true; }
    void setDisconnected() { mIsDeviceConnected = false; }

	/// Queue the command to enable data streaming notifications for this device
	void enableStreaming();

	/// Queue the command to disable data streaming notifications for this device
	void disableStreaming();

	/// Get whether or not this device is currently streaming data
	/// \returns a boolean indicating whether or not this device is streaming data
	bool isStreaming() const;

	/// Forces a check of the streaming status of this device
	void checkStreaming() const;

	/// Check the streaming status byte
	/// \returns a boolean indicating the state of the streaming status byte
	bool checkStreamingByte() const;

	/// Queue the command to set the LED state for this device
	/// \param[in] state: an LedState type indicating the new desired LED state for this device
	void requestLedState(LedState state);

	/// Queue the command to get the current received signal strength level for this device (in dBm)
	void requestSignalStrength() const;

	/// Find the available services for this device
	/// \param[in] primary: a boolean indicating whether or not to look for primary services
	void findServices(bool primary = true) const;

	/// Find the available characteristics for a service for this device
	/// \param[in] service: a unsigned int containing the service UUID for which to use to find the corresponding characteristics
	void findCharacteristicsForService(uint16 service) const;

	/// Update the link layer connection settings for this device
	/// \param[in] interval_min: an unsigned int containing the updated minimum connection interval (in ms)
	/// \param[in] interval_max: an unsigned int containing the updated maximum connection interval (in ms)
	/// \param[in] latency: an unsigned int containing the updated latency (in ms)
	/// \param[in] timeout: an unsigned int containing the updated timeout (in ms)
	void updateConnection(uint16 interval_min, uint16 interval_max, uint16 latency, uint16 timeout) const;

	/// Queue the command to put this device into the permanent lockdown state
	void lockdown();

    char *BLEReadUUIDString(unsigned int uuid, char *ptr, unsigned int len);
    int BLEReadUUIDBytes(unsigned int uuid, char *ptr, unsigned int len);
    int BLEEnableNotification(unsigned int uuid);
	int BLEDisableNotification(unsigned int uuid, DWORD timeout);

	/// Get the current signal strength level for this device in dBm
	/// \returns an integer containing the current signal strength level for this device
	int signalStrength() const;

	/// Get the current device state for this device
	/// \returns a DeviceState object containing the current device state
	DeviceState state() const;

	/// Get whether or not this device has been added to the GUI (BleVvWidget)
	/// \returns a boolean indicating whether or not this device has been added to the GUI
	bool hasDeviceBeenAddedToGui() const;

	/// Set whether or not this device has been added to the GUI
	/// \param[in] newState: a boolean updating whether or not this device has been added to the GUI
	void setStateDeviceAddedToGui(bool newState);

	/// Set the BLE device list index for this device (its index in the BluetoothImuDevice vector)
	/// \param[in] index: an integer containing the device list index for this device
	void setDeviceListIndex(int index);

	/// Get the received signal strength for this device in dBm
	/// \returns an integer containing the current signal strength level for this device
	int getRSSI() const;

	/// Update the current LED state for this device. Needed if this device was previously added to the GUI
	/// \param[in] state: new LedState for this device (does not send a BLE command to this device)
	void updateLedState(LedState state);

	/// Get the device list index for this device (its index in the BluetoothImuDevice vector contained in the main BluetoothController)
	/// \returns an 8 bit unsigned int containing the index 
    unsigned __int64 BluetoothImuDevice::getDeviceHandle() const;

	/// Set the device state for this device
	/// \param[in] state: a DeviceState object containing the updated state for this device
	void setState(DeviceState state);

	/// Request passkey verification from this device. Use during pairing
	void requestPasskeyVerification() const;

	/// Finalize pairing state for this device
	void pairingCompleted(bool success);

	/// Confirm that the last attribute write command for this device succeeded
	void confirmWriteAttributeSucceeded();

signals:

	/// Signal specifying that the signal strenght has changed
	/// \param[in] device: passes a const reference to this device
	void signalStrengthChanged(const BluetoothImuDevice& device) const;

	/// Signal specifying that the device has started to connect
	/// \param[in] device: passes a const reference to this device
	void connecting(const BluetoothImuDevice& device ) const;

	/// Signal specifying that the device has successfully connected
	/// \param[in] device: passes a const reference to this device
	void connected(const BluetoothImuDevice& device) const;

	/// Signal specifying that the device has started to disconnect
	/// \param[in] device: passes a const reference to this device
	void disconnecting(const BluetoothImuDevice& device) const;

	/// Signal specifying that the device has disconnected
	/// \param[in] device: passes a const reference to this device
	void disconnected(const BluetoothImuDevice& device) const;

	/// Signal specifying that the device streaming state has changed
	/// \param[in] device: passes a const reference to this device
	void streamingStateChanged(const BluetoothImuDevice& device) const;

	/// Signal specifying that new data is ready
	/// \param[in] device: passes a const reference to this device
	void dataReady(const BluetoothImuDevice& device) const;

	/// Signal specifying that the device state has changed
	/// \param[in] device: passes a const reference to this device
	void stateChanged(const BluetoothImuDevice& device) const;

	/// Signal specifying that services were found for this device
	/// \param[in] device: passes a const reference to this device
	void servicesFound(const BluetoothImuDevice& device) const;

	/// Signal specifying that characteristics were found
	/// \param[in] device: passes a const reference to this device
	void characteristicsFound(const BluetoothImuDevice& device) const;

	/// Signal specifying that information related to this device (e.g., name) has changed
	/// \param[in] device: passes a const reference to this device
	void infoChanged(const BluetoothImuDevice& device) const;

	/// Signal specifying that the LED state has changed
	/// \param[in] device: passes a const reference to this device
	void ledStateChanged(const BluetoothImuDevice& device) const;

	/// Signal indicating that this device has been locked down
	void deviceLockedDown();

  protected:
	/// Uninitialize/terminate this device
	void uninitialize();

  protected:
	BluetoothController& mController;											///< Reference to the main BluetoothController object
	mutable QMutex mMutex;														///< Mutex for multithreaded locking and unlocking of this device
																				///
																				///
	bd_addr mAddress; 															///< Device address (i.e., "AE:3F:7C:BC:DA:F9")
	gap_address_type mAddressType;												///< GAP address type, 0 == public, 1 == random
	uint8 mConnectionHandle;													///< Connection handle for this device
	uint8 mBondingHandle;														///< Bonding handle for this device. Will be 0xFF if the device is not bonded.
	bool mIsPipeConnected;															///< Boolean specifying whether or not this device is connected
    bool mIsDeviceConnected;
	bool mIsStreaming;															///< Boolean specifying whether or not this device is streaming data
	bool mbIsPaired;															///< Boolean specifying whether or not this device is paired
	bool mbHasDeviceBeenAddedToGui;												///< Boolean specifying whether or not this device has been added to the GUI
	LedState mLedState;															///< Current LED state
	int mRSSI;																	///< Current received signal strength

	QString mName;																///< Device name read in from the device
	QString mManufacturer;														///< Manufacturer string read in from the device
	QString mSoftwareRevision;													///< Software revision read in from the device
	QString mHardwareRevision;													///< Hardware revision read in from the device
	QString mFirmwareRevision;													///< Firmware revision read in from the device
	QString mSerialNumber;														///< Serial number read in from the device
	QString mModelNumber;														///< Model number read in from the device
	uint8 mStreamingStatusByte;													///< Byte specifying the streaming status

	DeviceState mState;															///< Stores the current state of the device
	BleDataPacket mData;														///< Current IMU data packet
    std::ofstream mTraceOutputHandle;
	bool mInitializedTraceOutput;
};
