//____________________________________________________________________________ 
// 
// (c) 2015, Mohamed Mahfouz. All Rights Reserved 
// 
// The contents of this file may not be disclosed, copied or duplicated in 
// any form, in whole or part, without the prior written permission of 
// Mohamed Mahfouz. 
//____________________________________________________________________________ 
// 
// Author: Joe Marrero (jmarrero@adextechnologies.com)
// Date: 2015/04/25
//____________________________________________________________________________
#pragma once
#include <cstdint>

namespace timer {
	int64_t clock();
	double milliseconds();
	double seconds();
} // namespace timer