//____________________________________________________________________________ 
// 
// (c) 2014, Mohamed Mahfouz. All Rights Reserved 
// 
// The contents of this file may not be disclosed, copied or duplicated in 
// any form, in whole or part, without the prior written permission of 
// Mohamed Mahfouz. 
//____________________________________________________________________________ 
// 
// Author: Michael Kuhn
// Date: 2014/04/20
//____________________________________________________________________________

#ifndef IOHANDLER_H
#define IOHANDLER_H

#include "BluetoothController.h"
#include "IMU.h"

#include <QStringList>
#include <QMessageBox>
#include <QMainWindow>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <qdom.h>
#include <qdebug.h>
#include <qtimer.h>

using namespace std;
using std::vector;

class IoHandler : public QObject
{
	Q_OBJECT

public:

	enum
	{
		IO_ERROR,
		IO_SUCCESS
	};

	/// Constructor
	IoHandler(const QString& resourcesPath, QObject* pParent = NULL);

	/// Destructor
	virtual ~IoHandler();

    int OpenCmdPipe();
    int CloseCmdPipe();

	/// Method for getting the number of active IMUs
	/// \returns number of active IMUs
	unsigned int GetNumActiveIMUs();

	/// Sets parent QWidget
	/// \param[in] pParent: pointer to parent widget
	void SetParentWidget(QObject* pParent);

	/// Called by Hip-IMU to update the color of the blinking LEDs on the BLE IMUs
	/// \param[in] currentColorIndex: index of the LED color to send to BLE device. Enumerated in BluetoothLowEnergyDevice::SEND_COMMAND
	/// \param[in] imuBleId: Index of BLE device in the bleDevices vector in BluetoothLowEnergyController
	void UpdateBleImuLeds(int currentColorIndex, unsigned int imuBleId);

	/// Start a new BLE action. Used to allow GUI interaction with the BLE COM port thread
	/// \param[in] taskID: Index of the BLE action to be performed. Enumerated in BluetoothLowEnergyController::BLE_VV_COMMAND
	/// \param[inout] data: QStringList containing data to be passed in for a BLE command. Can also be filled with data as a result of a BLE action.
	/// \param[in] bleDevID: Optional. Index of a specific BLE device in the bleDevices vector in BluetoothLowEnergyController. Used for certain BLE actions.
	/// \param[in] flag: Optional. Boolean used to update various BLE flags. Used for certain BLE actions.
	void StartNewBleAction(int taskID, QStringList& data, DWORD64 bleDevID, bool flag = false);

	/// Gets the current minimum and maximum connection interval values (hex values) used in configuring the BLE link at the BLE Central.
	/// \param[out] min: Filled with the current minimum connection interval value
	/// \param[out] max: Filled with the current maximum connection interval value
	void GetConnectionIntervalVals(unsigned int &min, unsigned int &max);

	/// Sets the current minimum and maximum connection interval values (hex values) used in configuring the BLE link at the BLE Central.
	/// These values will only be used in configuring the BLE channel if the BLE Central is reinitialized.
	/// \param[in] min: New minimum connection interval value
	/// \param[in] max: New maximum connection interval value
	void SetConnectionIntervalVals(unsigned int min, unsigned int max);

	/// Sets a new scanning filter string to use in filtering out BLE devices at the scanning phase.
	/// This filter string is only used if the BluetoothLowEnergyController::mbFilterDeviceInfoStringsWhileScanning flag has been set to true.
	/// \param[in] newFilterString: New scanning filter string to use in filtering which BLE devices are linked/paired
	void SetScanningFilterString(QString newFilterString);

	/// Gets the current scanning filter string used in filtering out BLE devices at the scanning phase.
	/// This filter string is only used if the BluetoothLowEnergyController::mbFilterDeviceInfoStringsWhileScanning flag has been set to true.
	/// \returns a QString containing the current filter string used during the scanning process.
	QString GetScanningFilterString();

	/// Sets the BluetoothLowEnergyController::mbFilterDeviceInfoStringsWhileScanning flag.
	/// If true, then the filter string will be used to filter out devices during the scanning process.
	/// \param[in] newState: Boolean used to enable/disable using the filter string during the scanning process.
	void ToggleUseOfScanningFilterString(bool newState);

signals:

	/// Emitted if the packet received is zero length
	void EmptyPacket();

	/// Signal specifying that new data has been received by all connected IMUs
	void NewDataReceivedAllIMUs();

	/// Signal specifying that new IMU is connected and should be added to the current IMU list
	/// \param[in] index: IMU index
	void AddCurrentIMU(int index);

	/// Signal specifying that all current IMU data (IMU list) have been updated
	/// (should update the application shared data current IMU list)
	void UpdateIMUData();

	/// Emitted to indicate that the connected IMUs have been successfully locked down
	void SignalImusSuccessfullyLockedDown();

	void SignalRefreshRateUpdated(int index);

	public slots:

	/// Handles new packet from COMPortThreadIMU (emits PacketReady() signal)
	void HandlePacket();

	/// Slot to log BLE errors either by outputting to the command prompt or to a file
	void LogBleErrors(int errorCode);

	/// Slot called when an IMU is successfully placed into permanent lockdown
//	void SlotImuSuccessfullyLockedDown();

protected:

	/// Main function for processing IMU data packets. Will look for 2 packets if 3rd gen IMUs are connected or 1 packet if 4th gen
	/// IMUs are connected. Calls initDataProc()...also checks for calibration states and emits dataReceived() signal
	void HandleData();

private:

	/// Function that handles processing a new BLE data packet
	/// \returns an integer containing the current IMU index in the mCurrentIMUs vector
	int HandleNewDataPacket();

public:
	int  mNumIMUsConnected;										///< Number of IMUs currently connected via the wireless IMU receive
	QTimer mMainIoHandlerTimer;									///< Timer in IoHandler thread for calculating data refresh rate and dt
	vector<int> mCurrentIndicesIMUs;							///< Stores the indices for currently connected IMUs
	QString mPacketIOHandler;									///< QString that stores the latest 4th gen IMU data from the COM port thread (Modified by ComPortThreadIMU)
	BleDataPacket mBlePacketIOHandler;							///< Stores the latest BLE data coming from the COM port thread (Modified by ComPortThreadIMU)
	BluetoothController *mBLEController;    					///< Main BLE controller object
	vector<bool> mNewIMUDataReceived;							///< Vector of booleans to determine whether data has been received by each IMU or not
protected:
	QMainWindow* mpParent;										///< Parent window
	QString mResourcesPath;										///< Resources path to load ED_List.xml 
	QMutex mMutex;												///< Main mutex used to keep the IOHandler thread safe
};

#endif