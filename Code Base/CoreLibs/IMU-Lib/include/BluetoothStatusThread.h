//____________________________________________________________________________ 
// 
// (c) 2015, Mohamed Mahfouz. All Rights Reserved 
// 
// The contents of this file may not be disclosed, copied or duplicated in 
// any form, in whole or part, without the prior written permission of 
// Mohamed Mahfouz. 
//____________________________________________________________________________ 
// 
// Author: Joe Marrero (jmarrero@adextechnologies.com)
// Date: 2015/04/25
//____________________________________________________________________________
#pragma once

#include <QThread.h>

class BluetoothController;

class BluetoothStatusThread : public QThread
{
	Q_OBJECT

  public:
	BluetoothStatusThread(BluetoothController& controller, int updateFrequency = 3000 );
	void stop();
	void setUpdateFrequency(int f);

  protected:
	void run();
	bool isStopped();
	void refresh();

protected:
	mutable QMutex mMutex;
	bool mStop;
	double mLastUpdate;
	int mUpdateFrequency;
	BluetoothController& mController;
};