//____________________________________________________________________________ 
// 
// (c) 2014, Mohamed Mahfouz. All Rights Reserved 
// 
// The contents of this file may not be disclosed, copied or duplicated in 
// any form, in whole or part, without the prior written permission of 
// Mohamed Mahfouz. 
//____________________________________________________________________________ 
// 
// Author: Michael Kuhn
// Date: 2014/05/31
//____________________________________________________________________________

#ifndef IOHANDLER_CONTROLLER_H
#define IOHANDLER_CONTROLLER_H

#include "iohandler.h"

#include <QThread>

using namespace std;

/** \brief Controls the threading of IoHandler to allow multi threaded support of COM port communication and data processing of IMU data.
 *	Contains QThread object that is attached to the main IoHandler for processing IMU data
 */
class IoHandlerController : public QObject
{
    Q_OBJECT
	
public:

	/// Constructor. Starts IoHandler thread.
	/// \param[in] resourcesPath: path to the resources folder (defined in an env variable)
	IoHandlerController(const QString& resourcesPath);

	/// Destructor. Stops IoHandler thread.
	~IoHandlerController();

public slots:

	/// Force thread to end using aboutToQuit() signal from QApplication
	virtual void EndThread();


public:

	QThread mIOHandlerThread;		///< Thread used for encapsulating the IoHandler real-time data processing into its own thread
	IoHandler mIOHandler;			///< Main object for real-time collection of IMU data
};

#endif // IOHANDLER_CONTROLLER_H