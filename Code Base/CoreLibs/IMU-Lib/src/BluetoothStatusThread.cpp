//____________________________________________________________________________ 
// 
// (c) 2015, Mohamed Mahfouz. All Rights Reserved 
// 
// The contents of this file may not be disclosed, copied or duplicated in 
// any form, in whole or part, without the prior written permission of 
// Mohamed Mahfouz. 
//____________________________________________________________________________ 
// 
// Author: Joe Marrero (jmarrero@adextechnologies.com)
// Date: 2015/04/25
//____________________________________________________________________________


#include <qdebug.h>
#include <iostream>
#include "BluetoothStatusThread.h"
#include "BluetoothController.h"
#include "Timer.h"
#include "IMU.h"

BluetoothStatusThread::BluetoothStatusThread(BluetoothController& controller, int updateFrequency)
	: QThread()
	, mMutex()
	, mStop(false)
	, mLastUpdate(0.0)
	, mUpdateFrequency(updateFrequency)
	, mController(controller)
{
}

void BluetoothStatusThread::stop()
{
	QMutexLocker locker(&mMutex);

	if (mController.flags() & BluetoothController::FL_DEBUG && ENABLE_LOG)
	{
		std::cout << "[BluetoothStatusThread] Stop called from thread " << currentThreadId() << std::endl;
	}

	mStop = true;
}

bool BluetoothStatusThread::isStopped()
{
	QMutexLocker locker(&mMutex);
	return mStop;
}

void BluetoothStatusThread::setUpdateFrequency(int f)
{
	QMutexLocker locker(&mMutex);
	mUpdateFrequency = f;

	if (mController.flags() & BluetoothController::FL_DEBUG && ENABLE_LOG)
	{
		std::cout << "[BluetoothStatusThread] Update frequency updated to " << f <<  "ms" << std::endl;
	}
}

void BluetoothStatusThread::run()
{
	// Initialization
	{
		QMutexLocker locker(&mMutex);
		mStop = false;
	}

	while (!isStopped())
	{
		const double now   = timer::milliseconds();
		const double delta = now - mLastUpdate;

		if (delta >= mUpdateFrequency)
		{
			refresh();
			mLastUpdate = now;
		}

		msleep(100);
	}
}

void BluetoothStatusThread::refresh()
{
	const BluetoothController::DeviceMap& devices = mController.devices();

	if (devices.size() > 0)
	{
		if (mController.flags() & BluetoothController::FL_DEBUG && ENABLE_LOG)
		{
			std::cout << "[BluetoothStatusThread] Refreshing..." << std::endl;
		}

		for (auto it = devices.begin(); it != devices.end(); it++)
		{
			const BluetoothImuDevice* device = it->second;

			if (device->isPipeConnected())
			{
				device->requestSignalStrength();
			}
		} // for
	} //if
}