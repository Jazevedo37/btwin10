#include "iohandler.h"
#include <QTextStream>
#include <QDateTime> 
#include <QDir>
#include <QMutexLocker>
#include <QFileDialog>
#include <QDomDocument>
#include <QDomElement>
#include <QDate>
#include "..\..\..\..\BLEServer\pipeprot.h"

IoHandler::IoHandler(const QString& resourcesPath, QObject *pParent) :
    mResourcesPath(resourcesPath)
{
	mBLEController = new BluetoothController();

	// make sure controller is not open 
	mBLEController->close();

	// timer for calculating IMU refresh rates and dt
	mMainIoHandlerTimer.setInterval(1000);
	mMainIoHandlerTimer.start();

	connect(mBLEController, SIGNAL(newDataPacketReady()), this, SLOT(HandlePacket()));
	connect(mBLEController, SIGNAL(errorOccurred(int)), this, SLOT(LogBleErrors(int)));
//	connect(mBLEController, SIGNAL(deviceLockedDown()), this, SLOT(SlotImuSuccessfullyLockedDown()));

	mBLEController->SetBleDataPacketPointer(&mBlePacketIOHandler);
}

IoHandler::~IoHandler()
{
	delete mBLEController;
}

int IoHandler::OpenCmdPipe()
{
	bool isPipeOpen = false;

	isPipeOpen = mBLEController->open();
	if (isPipeOpen)
	{
		if (mBLEController->state() == BluetoothController::ALL_SENSORS_DISCONNECTED ||
			mBLEController->state() == BluetoothController::FINDING_SENSORS)
		{
			mBLEController->communicationThread().clearQueue();
			mBLEController->communicationThread().unpause(__FILE__, __LINE__);
		}
		return IO_SUCCESS;
	}
	else
	{
		return IO_ERROR;
	}
}

int IoHandler::CloseCmdPipe()
{
	mBLEController->close();
	return 1;
}

void IoHandler::HandlePacket()
{
}

void IoHandler::HandleData()
{
}

int IoHandler::HandleNewDataPacket()
{
	return(0);
}

void IoHandler::SetParentWidget(QObject* pParent)
{
	mpParent = (QMainWindow*)pParent;
}


void IoHandler::StartNewBleAction(int taskID, QStringList& data, DWORD64 bleDevID, bool flag)
{
//	printf("StartNewBleAction: taskId=%d blDevID=%lX\n", taskID, bleDevID);

	if (taskID == BluetoothController::SCAN)
	{
		mBLEController->scan();
		return;
	}
	else if (taskID == BluetoothController::RELINK_ALL)
	{
		QMutex mutex;
		mutex.lock();
		mutex.unlock();
		return;
	}
	else if (taskID == BluetoothController::TERMINATE_ACTIVE_LINKS)
	{
		for (auto it = mBLEController->devices().begin(); it != mBLEController->devices().end(); it++)
		{
			BluetoothImuDevice *currentDevice = it->second;
			currentDevice->disconnect();
		}
		return;
	}
	else if (taskID == BluetoothController::TERMINATE_LINK_REQUESTS)
	{
		QMutex mutex;
		mutex.lock();
		mutex.unlock();
		return;
	}
	else if (taskID == BluetoothController::START_IMU_STREAMING_ALL)
	{
		for (auto it = mBLEController->devices().begin(); it != mBLEController->devices().end(); it++)
		{
			BluetoothImuDevice *currentDevice = it->second;
			currentDevice->enableStreaming();
		}
		return;
	}
	else if (taskID == BluetoothController::STOP_IMU_STREAMING_ALL)
	{
		for (auto it = mBLEController->devices().begin(); it != mBLEController->devices().end(); it++)
		{
			BluetoothImuDevice *currentDevice = it->second;
			currentDevice->disableStreaming();
		}
		return;
	}
	else if (taskID == BluetoothController::REINITIALIZE_DONGLE)
	{
		// disconnect devices and then perform a hard reset
		for (auto it = mBLEController->devices().begin(); it != mBLEController->devices().end(); it++)
		{
			BluetoothImuDevice *currentDevice = it->second;
			currentDevice->disconnect();
		}

		Sleep(BLE_SLEEP_AFTER_TX_CMD_LONG_MS);
		mBLEController->reset(false);
		return;
	}
	else if (taskID == BluetoothController::RESET_DONGLE)
	{
		// disconnect devices and then perform a hard reset
		for (auto it = mBLEController->devices().begin(); it != mBLEController->devices().end(); it++)
		{
			BluetoothImuDevice *currentDevice = it->second;
			currentDevice->disconnect();
		}

		Sleep(BLE_SLEEP_AFTER_TX_CMD_LONG_MS);
		mBLEController->reset(false);
		return;
	}
	else if (taskID == BluetoothController::CHANGE_DATA_PACKET_FORMAT)
	{
		QMutex mutex;
		mutex.lock();
		mutex.unlock();
		return;
	}
	
	// make sure the bleDevID is valid
    auto it = mBLEController->devices().find(bleDevID);
    if(it == mBLEController->devices().end())
	{
		printf("StartNewBleAction: returning, bad bleDevID %d.\n", bleDevID);
		return;
	}
	
	if (taskID == BluetoothController::CHANGE_DEVICE_LED_STATE)
	{
		mBLEController->devices()[bleDevID]->requestLedState((BluetoothImuDevice::LedState)data[0].toInt());
	}
	else if (taskID == BluetoothController::DEVICE_SOFTWARE_RESET)
	{
		QMutex mutex;
		mutex.lock();
		mutex.unlock();
	}
	else if (taskID == BluetoothController::RELINK_DEVICE)
	{
		printf("RELINK_DEVICE: bleDevID = %lX devices %d\n", bleDevID, mBLEController->devices().size());
		if (!mBLEController->devices()[bleDevID]->isPipeConnected())
		{
			mBLEController->devices()[bleDevID]->connect();
		}
	}
	else if (taskID == BluetoothController::TERMINATE_LINK_DEVICE)
	{
		printf("TERMINATE_LINK_DEVICE: bleDevID = %lX devices = %d\n", bleDevID, mBLEController->devices().size());
		if (mBLEController->devices()[bleDevID]->isPipeConnected())
		{
			mBLEController->devices()[bleDevID]->disconnect();
		}
	}
	else if (taskID == BluetoothController::PERMANENT_LOCKDOWN_DEVICE)
	{
		mBLEController->devices()[bleDevID]->lockdown();
		
	}
	else if (taskID == BluetoothController::SEND_BLE_COMMAND)
	{
		QMutex mutex;
		mutex.lock();
		mutex.unlock();
	}
	else if (taskID == BluetoothController::START_IMU_STREAMING_DEVICE)
	{
		mBLEController->devices()[bleDevID]->enableStreaming();
	}
	else if (taskID == BluetoothController::STOP_IMU_STREAMING_DEVICE)
	{
		mBLEController->devices()[bleDevID]->disableStreaming();
	}
	else if (taskID == BluetoothController::CHANGE_STATE_CAL_DEV_ADDRESS_LOCKDOWN)
	{
	}
	else if (taskID == BluetoothController::CHANGE_STATE_DEV_STRING_FILTERING)
	{
	}
}

#if 1
void IoHandler::GetConnectionIntervalVals(unsigned int &min, unsigned int &max)
{
	unsigned int minConnInterval(0), maxConnInterval(0);
	min = minConnInterval;
	max = maxConnInterval;
}

void IoHandler::SetConnectionIntervalVals(unsigned int min, unsigned int max)
{
	// make sure they are valid connection interval parameters
	if (min <= max)
	{
		if (min >= MIN_BLE_MIN_CONN_INTERVAL_HEX && min <= MAX_BLE_MIN_CONN_INTERVAL_HEX && max >= MIN_BLE_MAX_CONN_INTERVAL_HEX && max <= MAX_BLE_MAX_CONN_INTERVAL_HEX)
		{
		}
	}
}
#endif

void IoHandler::SetScanningFilterString(QString newFilterString)
{
}

QString IoHandler::GetScanningFilterString()
{
	QString tempString("");
	return tempString;
}

void IoHandler::ToggleUseOfScanningFilterString(bool newState)
{
}

void IoHandler::LogBleErrors(int errorCode)
{
	std::cout << ("BLE error occurred: " + QString(BluetoothController::errorString(errorCode)).toStdString()) << "\n";
}

#if 0
void IoHandler::SlotImuSuccessfullyLockedDown()
{
}
#endif