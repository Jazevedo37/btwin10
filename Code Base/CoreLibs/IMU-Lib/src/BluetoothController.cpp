//____________________________________________________________________________ 
// 
// (c) 2019, All Rights Reserved 
// 
// The contents of this file may not be disclosed, copied or duplicated in 
// any form, in whole or part, without the prior written permission of 
// Mohamed Mahfouz. 
//____________________________________________________________________________ 
// 
// Author: Dan Rebello (dan@streamserverlabs.com)
// Date: 2019/10/13
//____________________________________________________________________________
#include <iostream>
#include <sstream>
#include <iomanip>
#include <qdebug>
#include <assert.h>
#include <functional>
#include <QDateTime> 
#include "BluetoothController.h"
#include "CommonDefinitionsIMU.h"
#include "IMU.h"


BluetoothController::BluetoothController()
	: mCmdPipeOpen(false)
	, mState(ALL_SENSORS_DISCONNECTED)
	, mMinorVersion(0)
	, mMajorVersion(0)
	, mHardwareVersion(0)
	, mBuildVersion(0)
	, mModelName("Unknown")
	, mDevices()
	, mFlags(FL_AUTO_PILOT | FL_RECONNECT_ON_DISCONNECT)
	, mCommunicationThread(*this)
	, mReadThread(*this)
	, mDeviceRecoveryThread(*this)
	, mStatusThread(*this, 15 * 1000)
	, mInStreamingMode(false)
	, mMutex(QMutex::Recursive)	
    , mGoingDown(false)
{
	
	printf("Looking for btconfig.txt.\n");
	mConfigFileHandle.open("./btconfig.txt");
    if (mConfigFileHandle.is_open()) {
        char tmpbuf[256];
		int x=0;

        while (mConfigFileHandle.good()) {
            mConfigFileHandle.getline(&tmpbuf[0], sizeof(tmpbuf), '\n');
            int cnt = mConfigFileHandle.gcount();
			if(cnt >= 17)
			{
				printf("MAC #%d: %s\n", x++, tmpbuf);
				unsigned int mac[6];
				sscanf(tmpbuf,"%2x:%2x:%2x:%2x:%2x:%2x", &mac[5], &mac[4], &mac[3], &mac[2], &mac[1],&mac[0]);
				unsigned __int64 macAdr = (unsigned __int64)mac[5] << 40 | (unsigned __int64)mac[4] << 32 | mac[3] << 24 | mac[2] << 16 | mac[1] << 8 | mac[0];
				printf("Adding MAC 0x%llx\n", macAdr);
				mAllowedMACS.push_back(macAdr);
			}   		   
		}
    }

    printf("Adding MAC 0x%llx\n", 0x7C010A35E59F);
    mAllowedMACS.push_back(0x7C010A35E59F);
    printf("Adding MAC 0x%llx\n", 0x7C010A35E5BF);
    mAllowedMACS.push_back(0x7C010A35E5BF);
	mFlags |= FL_DEBUG;
}


BluetoothController::~BluetoothController()
{
	if (isOpen())
	{
		close();
	}

	if (mDevices.size() > 0)
	{
		clearDevices();
	}

    if (mConfigFileHandle.is_open()) {
		mConfigFileHandle.close();
    }

}

void BluetoothController::setFlags(uint8 flags)
{
	mFlags = flags;
}

void BluetoothController::addFlags(uint8 flags)
{
	mFlags |= flags;
}

void BluetoothController::removeFlags(uint8 flags)
{
	mFlags &= ~flags;
}

bool BluetoothController::open()
{
	bool result = false;

	if (!isOpen())
	{
		bool isOpened = mPipe.open(&mCmdPipeHandle);
		mCmdPipeOpen = isOpened;
		if (isOpened) {
			reset(false);

			mCommunicationThread.start();
			mReadThread.start();
			mStatusThread.start();
		}
		else
		{
			emit errorOccurred(ERR_OPENING_PORT_FAILED);				
		}

		result = isOpened;
	} // if not open

	// return true if connection is already open
	if (isOpen())
	{
		result = true;
	}

	return result;
}

void BluetoothController::close()
{
	if (isOpen())
	{
		const bool willReconnectOnDisconnect = flags() & FL_RECONNECT_ON_DISCONNECT;

		if (willReconnectOnDisconnect)
		{
			// We temporarily disable autoconnect to prevent devices from
			// trying to reconnect as we forcefully disconnect all of the devices.
			removeFlags(FL_RECONNECT_ON_DISCONNECT);
		}

		mMutex.lock();
        for (map<DWORD64,BluetoothImuDevice*>::iterator it = mDevices.begin(); it != mDevices.end(); it++) {
            BluetoothImuDevice *device = it->second;
			if (device->isStreaming())
			{
				device->disableStreaming();
			}
			device->disconnect();
			device->setState(BluetoothImuDevice::UNINITIALIZED);
			emit newDeviceState(device->mMACAddr);
		}
		mMutex.unlock();

		if (flags() & FL_DEBUG && ENABLE_LOG)
		{
			std::cout << QThread::currentThreadId() << " Closing!" << std::endl;
		}

        BLEDisconnectCmdPipe();

        printf("Closing Command pipe.\n");
		// close the pipe
		mPipe.close(mCmdPipeHandle);
		mCmdPipeOpen = false;
		
		if (willReconnectOnDisconnect)
		{
			// Restore the flag to the previous setting
			addFlags(FL_RECONNECT_ON_DISCONNECT);
		}
	}
}

bool BluetoothController::isOpen() const
{
	return mCmdPipeOpen;
}


bool BluetoothController::reset(bool soft)
{
	return true; 
}

static const char* bluetoothControllerStateStrings[] =
{
	"ALL_SENSORS_DISCONNECTED",
	"CONNECTING_SENSORS",
	"ALL_SENSORS_CONNECTED",
	"LISTENING_MEASUREMENTS"
};

void BluetoothController::setState(State s)
{
	if (flags() & FL_DEBUG && ENABLE_LOG)
	{
		/*std::cout << "DEBUG: State changed: "
			<< bluetoothControllerStateStrings[mState] << " --> " << bluetoothControllerStateStrings[s] << std::endl;*/
	}
	mState = s;

	// Notifiy others of state change.
	emit newControllerState(mState);
	emit stateChanged(mState);
}

BluetoothCommunicationThread& BluetoothController::communicationThread()
{
	return mCommunicationThread;
}

BluetoothImuDevice* BluetoothController::findDeviceByAddress(bd_addr address)
{
	BluetoothImuDevice* result = NULL;

	for (auto it = mDevices.begin(); it != mDevices.end(); ++it)
	{
		BluetoothImuDevice* device = it->second;

		if (bdaddr_compare(address, device->address()) == 0)
		{
			result = device;
			break;
		}
	}

	return result;
}


BluetoothImuDevice* BluetoothController::addDevice(DWORD64 macAddr, bd_addr btaddr, const char* name)
{
	BluetoothImuDevice* device = NULL;

    printf("%s: MAC %llX name=%s\n", __FUNCTION__, macAddr, name);

	mMutex.lock();
	// Only add unique devices.
	if (!findDeviceByAddress(btaddr))
	{
        printf("%s: MAC %llX name=%s not found, adding.\n", __FUNCTION__, macAddr, name);
		device = new BluetoothImuDevice(*this, macAddr, btaddr, gap_address_type_public,
			name ? name : "Unknown", 0);
		mDevices[macAddr] = device;
		connect(device, SIGNAL(deviceLockedDown()), this, SLOT(slotDeviceLockedDown()));

		QString devAddressStr(device->addressString());
		emit addNewDevice(devAddressStr,device->mMACAddr,device->addressString(),0,false);
        device->setStateDeviceAddedToGui(true);
		mMutex.unlock();

		device->Init();
        device->mInitComplete = true;
	}
	else mMutex.unlock();
	return device;
}

void BluetoothController::removeDevice(DWORD64 macAddr)
{   									  
	printf("%s: MAC %llX\n", __FUNCTION__, macAddr);

	if (macAddr == 0)
		return;

	mMutex.lock();
	if (mDevices.find(macAddr) == mDevices.end()) {
        mMutex.unlock();
		return;
	}

	BluetoothImuDevice* device = mDevices[macAddr];
    if (device->mMACAddr == macAddr) 
	{
		printf("BluetoothController: Removing device %llX.\n", macAddr);
		// If we are connected, make sure to stop streaming and disconnect the device.
		if (device->isPipeConnected())
		{
			if (device->isStreaming())
			{
				device->disableStreaming();
			}
			device->disconnect();
		}
        printf("%s: Calling delete\n", __FUNCTION__);
		delete device;
        printf("%s: Calling erase\n", __FUNCTION__);
        mDevices.erase(macAddr);
        mMutex.unlock();
        printf("%s: Done\n", __FUNCTION__);
		return;
	}
	mMutex.unlock();
}

void BluetoothController::clearDevices()
{
    mMutex.lock();
	for (map<DWORD64,BluetoothImuDevice*>::iterator it = mDevices.begin(); it != mDevices.end(); )
	{
		BluetoothImuDevice* device = it->second;
        removeDevice(device->mMACAddr);
		it = mDevices.begin();
	}
	mDevices.clear();

	mMutex.unlock();
}

BluetoothController::DeviceMap& BluetoothController::devices()
{
	return mDevices;
}

int BluetoothController::scan() // initiate a scan
{
	BTPipeMessage msg;
	BTPipeMsgReturnCode rc;
	int numDevices = 0;
	char *formatMAC(unsigned long long mac, char *ptr, int len);

	printf("%s:\n", __FUNCTION__);

	setState(BluetoothController::State::ALL_SENSORS_DISCONNECTED);
	if (!isOpen()) {
		printf("Command pipe not connected, aborting scan.\n");
		return 0;
	}

	// update controller state
	mState = State::FINDING_SENSORS;
	emit newControllerState(mState);

	for (auto i = mAllowedMACS.begin(); i < mAllowedMACS.end(); i++) {
		memset(&msg, 0, sizeof(BTPipeMessage));
		msg.msgId = BT_SET_MAC_ADDRESSES;
		msg.param[0] = 1;
		printf("Sending MAC 0x%12.12llx to server\n", *i);
		msg.param[1] = *i >> 32;
		msg.param[2] = *i & 0xFFFFFFFF;
        rc = SendPipeRequest2(&msg, INFINITE);
		printf("BT_SET_MAC_ADDRESSES result = %d.\n", rc);
	}

	setState(BluetoothController::State::FINDING_SENSORS);
	printf("\nSending BT_SCAN:\n");
	msg.msgId = BT_SCAN;
    rc = SendPipeRequest2(&msg, INFINITE);
	numDevices = rc == 0 ? msg.param[0] : 0;
	printf("Scan result = %d found %d devices.\n", rc, numDevices);

    
	int connectedDevices = 0;
	if (rc == 0) {
		unsigned int connectStatus = 0;
        DWORD64 macAddrs[2];

        // Save the scanned MAC addresses, note there is a maximum of two devices
		for (int x = 1, index=0; x < 1+numDevices*2 && x < 1+2*2; x+=2) {
            macAddrs[index++] = (unsigned __int64)msg.param[x] << 32 | msg.param[x+1];
        }

		DWORD64 btAdr = 0;
		for (int x = 0; x < numDevices; x++) {
			setState(BluetoothController::State::CONNECTING_SENSORS);
			memset(&msg, 0, sizeof(BTPipeMessage));
			msg.msgId = BT_DEVICE_INFO;
            msg.macAddr = macAddrs[x];
			printf("Sending BT_DEVICE_INFO for MAC %llX\n", msg.macAddr);
            rc = SendPipeRequest2(&msg, INFINITE);
			printf("Device Info result = %d\n", rc);
			bd_addr btaddr;
			if (rc == 0) {
				btAdr = (DWORD64)msg.param[0] << 32 | msg.param[1];
				connectStatus = msg.param[2];
				char macAdr[20];
				formatMAC(btAdr, macAdr, sizeof(macAdr));
				printf("Device %llX bluetooth address: %s\n", msg.macAddr, macAdr);
				for (int x = 0; x < 6; x++)
					*((uint8 *)&btaddr + x) = (btAdr >> x * 8) & 0xFF;
			}

            if (connectStatus) {
				printf("Device %llX connected, adding.\n", btAdr);
				BluetoothImuDevice *btdev = addDevice(btAdr, btaddr, "TechMah Medical");

                if (btdev) 
					btdev->setState(BluetoothImuDevice::__READY);
                connectedDevices++;
			}
			else printf("Device %llX is not connected.\n", btAdr);
		}

        if (connectedDevices == 2) {
            setState(BluetoothController::State::ALL_SENSORS_CONNECTED);
        }
	}
	return(connectedDevices);
}

static inline void print_received_packet(const uint8 *packet, std::ofstream &fileHandle)
{
	if (T_IS_LOGGING_TRACE)
	{
		QString outputStr("RX <= [ ");
		char buffer[200];
		//printf("RX <= [ ");
		for (int i = 0; i < sizeof(struct ble_header); i++)
		{   // display first 4 bytes, always present in every packet
			//printf("%02X ", packet[i]);
			sprintf(buffer, "%02X ", packet[i]);
			outputStr += QString(buffer);
		}

		const struct ble_header* hdr = reinterpret_cast<const struct ble_header*>(packet);

		if (hdr->lolen > 0)
		{
			//printf("| ");
			outputStr += QString("| ");
			// display remaining data payload
			for (int i = 0; i < hdr->lolen; i++)
			{
				//printf("%02X ", packet[i + sizeof(struct ble_header)]);
				sprintf(buffer, "%02X ", packet[i + sizeof(struct ble_header)]);
				outputStr += QString(buffer);
			}
		}
		//printf("]\n");
		outputStr += "\n";

		if (fileHandle.is_open())
		{
			fileHandle.write(outputStr.toStdString().data(), outputStr.length());
		}
	}
}


int BluetoothController::receivePacket(BluetoothImuDevice *device)
{
	BTPipeMsgReturnCode ReadDataPipe(HANDLE pipeHandle, unsigned char *buffer, unsigned int buflen, DWORD *bytesRead);
	extern HANDLE gDataPipeHandle;
	
	unsigned char tmpbuf[128];
	DWORD bytesRead;
	
    if (device && ReadDataPipe(device->mDataPipe, tmpbuf, sizeof(tmpbuf), &bytesRead) != BT_FAIL) {
        if (bytesRead) {
            print_received_packet(tmpbuf, device->mTraceOutputHandle);
			printf("MAC: %llX\n", device->mMACAddr);
			for (unsigned int x = 0; x < bytesRead; x++) {
				unsigned char val = tmpbuf[x];
				if (x % 16 == 0) printf("\n");
				printf("%2.2X ", val & 0xFF);
			}
			printf("\n");
        }   			
	}
		
	return 0;
}

static inline void print_sent_packet(const struct ble_header *hdr, uint8 *data, std::ofstream &fileHandle)
{
	if (T_IS_LOGGING_TRACE)
	{
		QString outputStr("TX => [ ");
		char buffer[200];
		//printf("TX => [ ");
		for (int i = 0; i < 4; i++)
		{   // display first 4 bytes, always present in every packet
			//printf("%02X ", ((uint8 *)hdr)[i]);

			sprintf(buffer, "%02X ", ((uint8 *)hdr)[i]);
			outputStr += QString(buffer);
		}
		if (hdr->lolen > 0)
		{
			//printf("| ");
			outputStr += "| ";
			// display remaining data payload
			for (int i = 0; i < hdr->lolen; i++)
			{
				//printf("%02X ", ((uint8 *)hdr)[i + 4]);

				sprintf(buffer, "%02X ", ((uint8 *)hdr)[i + 4]);
				outputStr += QString(buffer);
			}
		}
		//printf("]\n");
		outputStr += "]\n";

		if (fileHandle.is_open())
		{
			fileHandle.write(outputStr.toStdString().data(), outputStr.length());
		}
	}
}


void BluetoothController::sendPacket(void* state, uint8 len1, uint8* data1, uint16 len2, uint8* data2)
{
}

int bdaddr_compare(bd_addr first, bd_addr second)
{
	for (int i = 0; i < sizeof(bd_addr); i++)
	{
		if (first.addr[i] != second.addr[i]) return 1;
	}
	return 0;
}

std::string bdaddr2string(bd_addr bdaddr)
{
	std::ostringstream oss;

	oss << std::uppercase << std::setfill('0') << std::setw(2) << std::hex
		<< bdaddr.addr[5] << ":"
		<< bdaddr.addr[4] << ":"
		<< bdaddr.addr[3] << ":"
		<< bdaddr.addr[2] << ":"
		<< bdaddr.addr[1] << ":"
		<< bdaddr.addr[0];
	return oss.str();
}


static const char* BLUETOOTH_CONTROLLER_ERROR_STRINGS[BluetoothController::ERROR_CODE_COUNT] = {
	"None",
	"Opening port failed",
	"Scan failed",
	"Setting scan parameters failed",
	"Bad service UUID",
	"Device not found",
	"Device is already connected", // 5
	"Device is not connected",
	"Connecting device failed",
	"Disconnecting device failed",
	"Updating connection parameters failed",
	"Reading from device failed", // 10
	"Writing to device failed",
	"Failed to find services on device",
	"Service not found",
	"Failed to find characteristics on device",
	"Characteristic not found", // 15
	"Characteristic(s) not initialized", 
	"Pairing/authentication Failed",
	"Passkey (PIN) for pairing/authentication missing"
};

const char* BluetoothController::errorString(int code)
{
	if (code < 0 || code >= ERROR_CODE_COUNT)
	{
		return "Unknown";
	}
	
	return BLUETOOTH_CONTROLLER_ERROR_STRINGS[code];
}

int BluetoothController::GetBlePacket()
{
#if 0
	int length;
	QMutex mutex;

	//Read the packet from the Queue
	mutex.lock();
	if (mBleStorageQueue.size() > 0)
	{
		try
		{
			*mCurrentDataPacket = mBleStorageQueue.front();
			length = 1;
			mBleStorageQueue.pop();
		}
		catch (const std::exception& e)
		{
		//	T_LOG_DEBUG(e.what());
			printf("%s\n",e.what());
		}
	}
	else
	{
		length = 0; 		 
	}
	mutex.unlock();
	return length;
#else
	return(0);
#endif
}

void BluetoothController::SetBleDataPacketPointer(BleDataPacket *dataPacket)
{
	mCurrentDataPacket = dataPacket;
}

void BluetoothController::endAll()
{
	close();

	// allow a little time to end everything
	Sleep(BLE_SLEEP_AFTER_TX_CMD_LONG_MS);

	mStatusThread.stop();
	mStatusThread.wait(); 		// Wait for the thread to join the main thread.

	mCommunicationThread.stop();
	mReadThread.stop();
	mDeviceRecoveryThread.wait();// wait for scan thread to stop, if running
	mCommunicationThread.wait(); // Wait for the thread to join the main thread

}

void BluetoothController::stopThreads()
{
	mDeviceRecoveryThread.wait();// wait for scan thread to stop, if running
	mStatusThread.stop();
	mStatusThread.wait(); 		// Wait for the thread to join the main thread.

	mCommunicationThread.stop();
	mReadThread.stop();
	mCommunicationThread.wait(); // Wait for the thread to join the main thread.
}

void BluetoothController::updateStreamingModeState()
{
	int numDevices = mDevices.size();
/*	if (numDevices >= mDesiredDevices)
	{
		bool allDevicesStreaming = true;
		for (int i = 0; i < numDevices; ++i)
		{
			BluetoothImuDevice *currentDevice = mDevices[i];
			if (!currentDevice->isStreaming())
			{
				allDevicesStreaming = false;
			}
		}

		if (allDevicesStreaming)
		{
			mInStreamingMode = true;
		}
    }
    */
}

bool BluetoothController::inStreamingMode() const
{
	return mInStreamingMode;
}

bool BluetoothController::isInDisconnectedState() const
{
	int numDevices = mDevices.size();
/*	if (numDevices >= mDesiredDevices)
	{
		bool allDevicesStreaming = true;
		for (int i = 0; i < numDevices; ++i)
		{
			BluetoothImuDevice *currentDevice = mDevices[i];
			if (currentDevice->state() == BluetoothImuDevice::UNINITIALIZED)
			{
				allDevicesStreaming = false;
			}
		}

		if (allDevicesStreaming)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	else
	{
		return true;
    }
    */
	return false;
}

void BluetoothController::slotDeviceLockedDown()
{
//	emit deviceLockedDown();
}

void BluetoothController::emptyBleDataPacketQueue()
{
	std::queue<BleDataPacket> empty;
	std::swap(mBleStorageQueue, empty);
}

int BluetoothController::BLEDisconnectCmdPipe() {
	BTPipeMessage msg;
	BTPipeMsgReturnCode rc;

	printf("BLEDisconnectingCmdPipe:\n");
	memset(&msg, 0, sizeof(BTPipeMessage));
	msg.msgId = BT_DISCONNECTING_CMD_PIPE;
	rc = SendPipeRequest(mCmdPipeHandle, &msg);
	printf("BLEDisconnectingCmdPipe: Complete.\n");
	return(0);
}

BTPipeMsgReturnCode BluetoothController::SendPipeRequestAsync(HANDLE pipeHandle, const BTPipeMessage *msg) {
	DWORD bytesWritten;

    if(msg->msgId == BT_DISCONNECTING_DATA_PIPE)
    {
        printf("%s: ID %d MAC %llX\n", __FUNCTION__, msg->msgId, msg->macAddr);
    }

	if (!WriteFile(pipeHandle, msg, sizeof(BTPipeMessage), &bytesWritten, NULL)) {
		int rc = GetLastError();
		printf("WriteFile error %d.\n", rc);
        if (rc == ERROR_PIPE_NOT_CONNECTED || rc == ERROR_NO_DATA) {
			printf("%s: Pipe communication failed, rc=%d for ID %d MAC %llX, exiting.\n", __FUNCTION__, rc, msg->msgId, msg->macAddr);
            mGoingDown = true;
			emit done(rc);
        }
		return(BT_FAIL);
	}

	if (bytesWritten != sizeof(BTPipeMessage)) {
		printf("Pipe message corrupton during write: %d bytes written to pipe.\n", bytesWritten);
		return(BT_FAIL);
	}

	return(BT_SUCCESS);
}



BTPipeMsgReturnCode BluetoothController::SendPipeRequest2(const BTPipeMessage *msg, DWORD timeout) {
	BluetoothBLESrvrCmd cmd;
    memset(&cmd, 0, sizeof(BluetoothBLESrvrCmd));
	memcpy(&cmd.msg, msg, sizeof(BTPipeMessage));
    HANDLE readEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	cmd.triggerEvent = true;
	cmd.responseEvent = readEvent;
	cmd.msg.param[15] = 0x55AA1234;
	cmd.ptr = &cmd.msg;

    if (communicationThread().isStopped() || mGoingDown) 
        return(BT_FAIL);

    communicationThread().enqueueCommandToServer(cmd);

//	printf("%s: cmd %d: id %x Wait ptr %x\n", __FUNCTION__, cmd.msg.msgId, cmd.msg.param[15], &cmd.msg);
	if (msg->msgId != BT_DISCONNECTING_CMD_PIPE) 
	{
		if(WaitForSingleObject(readEvent, timeout) == WAIT_OBJECT_0)     // Wait for command to complete
		{ 
            printf("%s: msgId %d MAC %llX done\n", __FUNCTION__, msg->msgId, msg->macAddr);
		}
        else {
            if (timeout != 0) {
                printf("%s: Timeout! msgId %d MAC %llX\n", __FUNCTION__, msg->msgId, msg->macAddr);
                return(BT_FAIL);
            }
        }
	} 
	else 
	{
        while (communicationThread().queueSize()) {	// Wait for BT_DISCONNECTING_CMD_PIPE to go out
			printf("Waiting for dispatch queue to empty.\n");
			Sleep(200);
        }
		Sleep(100);
        CloseHandle(readEvent);
		return(BT_SUCCESS);
	}

	CloseHandle(readEvent);
	memcpy((void *)msg, &cmd.msg, sizeof(BTPipeMessage));
//	printf("SendPipeRequest2: cmd %d rc %d\n", cmd.msg.msgId, cmd.msg.rc);

	switch (msg->msgId) {
	case BT_SCAN:
	case BT_DEVICE_INFO:
	case BT_SET_LED:
	case BT_SET_MAC_ADDRESSES:
	case BT_READ_CHARACTERISTIC:
	case BT_WRITE_CHARACTERISTIC:
	case BT_ENABLE_NOTIFICATION:
	case BT_DISABLE_NOTIFICATION:
	case BT_DISCONNECTING_CMD_PIPE:
	case BT_DISCONNECTING_DATA_PIPE:
		return(msg->rc);

	default:
		printf("Invalid pipe response %d!\n", msg->msgId);
	}
	return(BT_FAIL);
}

