//____________________________________________________________________________
//
// (c) 2019. All Rights Reserved
//
// The contents of this file may not be disclosed, copied or duplicated in
// any form, in whole or part, without the prior written permission of
// Mohamed Mahfouz.
//____________________________________________________________________________
//
// Author: Dan Rebello (dan@streamserverlabs.com)
// Date: 2019/10/13
//____________________________________________________________________________
#include <QDebug>
#include <iostream>
#include <cassert>
#include <QtWidgets/QApplication>
#include "BleVvWidget.h"
#include "BluetoothCommunicationThread.h"
#include "BluetoothController.h"
#include "timer.h"

BluetoothCommunicationThread::BluetoothCommunicationThread(BluetoothController& controller)
	: mMutex()
	, mStop(false)
	, mPause(false)
	, mController(controller)
	, mCommandQueue()
    , mKeepAlive(true)
#if ENABLE_PAUSE_TIMEOUT
	, mPauseTime(0.0)
#endif
{
}

BluetoothCommunicationThread::~BluetoothCommunicationThread()
{
}

BluetoothReadThread::BluetoothReadThread(BluetoothController& controller)
	: mMutex()
	, mStop(false)
	, mController(controller)
{
}

BluetoothReadThread::~BluetoothReadThread()
{
}

BluetoothDeviceRecoveryThread::BluetoothDeviceRecoveryThread(BluetoothController& controller)
	: mMutex()
	, mStop(false)
	, mController(controller)
{
}

BluetoothDeviceRecoveryThread::~BluetoothDeviceRecoveryThread()
{
}

void BluetoothDeviceRecoveryThread::run() {
    bd_addr btaddr;
	
    if (mMACQueue.size() >= 1) {
        DWORD64 mac = mMACQueue.front();
        mMACQueue.pop();
        for (int x = 0; x < 6; x++)
            *((uint8 *)&btaddr + x) = (mac >> x * 8) & 0xFF;

        printf("DeviceRecoveryThread: Adding MAC %llX\n", mac);
        BluetoothImuDevice *btdev = mController.addDevice(mac, btaddr, "TechMah Medical");

        if (btdev) 
            btdev->setState(BluetoothImuDevice::__READY);
    }
}

void BluetoothCommunicationThread::stop()
{
	if (mController.flags() & BluetoothController::FL_DEBUG)
	{
		std::cout << "[BluetoothCommunicationThread] stopped from " << currentThreadId() << std::endl;
	}
	QMutexLocker locker(&mMutex);
	mStop = true;
}

void BluetoothReadThread::stop()
{
	if (mController.flags() & BluetoothController::FL_DEBUG)
	{
		std::cout << "[BluetoothReadThread] stopped from " << currentThreadId() << std::endl;
	}
	QMutexLocker locker(&mMutex);
	mStop = true;
}

void BluetoothCommunicationThread::pause()
{
	QMutexLocker locker(&mMutex);

	if (!mPause)
	{
		mPause = true;
#if ENABLE_PAUSE_TIMEOUT
		mPauseTime = timer::milliseconds();
#endif
		if (mController.flags() & BluetoothController::FL_DEBUG)
		{
			std::cout << "[BluetoothCommunicationThread] paused queue" << std::endl;
		}
	}
}

void BluetoothCommunicationThread::unpause(const char* file, int line)
{
	QMutexLocker locker(&mMutex);
	if (mPause)
	{
		dequeueCommand();

		if (mController.flags() & BluetoothController::FL_DEBUG)
		{
			std::cout << "[BluetoothCommunicationThread] unpaused queue at" << file << " (line=" << line << ")" << std::endl;
		}
		mPause = false;
	}
}

bool BluetoothCommunicationThread::isStopped()
{
	QMutexLocker locker(&mMutex);
	return mStop;
}

bool BluetoothCommunicationThread::isPaused()
{
	QMutexLocker locker(&mMutex);

#if ENABLE_PAUSE_TIMEOUT
	const double now = timer::milliseconds();
	const double delta = now - mPauseTime;
	if (mPause && delta >= MAX_TIMEOUT)
	{
		if (mController.flags() & BluetoothController::FL_DEBUG)
		{
			std::cout << "[BluetoothCommunicationThread] WARNING: Forcefully unpausing queue because it execeeded" << MAX_TIMEOUT << "ms" << std::endl;
		}
		// We've been paused for way too long
		// so we forcefully unpause the queue.
		unpause(__FILE__, __LINE__);
	}
#endif
	return mPause;
}

void BluetoothCommunicationThread::run()
{
    bool cmdPipeConnected = true;
    BluetoothBLESrvrCmd keepAliveCmd;

	// Initialization
	{
		QMutexLocker locker(&mMutex);
		mStop = false;
	}

	double current_time_in_ms = timer::milliseconds();
	double past_time_in_ms = current_time_in_ms;
    
    HANDLE readEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    DWORD tick, lastTick=0;

    while (!isStopped())
	{
        tick = GetTickCount();
        if (mKeepAlive && tick - lastTick >= 500) {
            lastTick = tick;
            memset(&keepAliveCmd, 0, sizeof(keepAliveCmd));
            keepAliveCmd.msg.msgId = BT_KEEP_ALIVE;
            keepAliveCmd.triggerEvent = false;
//            printf("Sending keep-alive.\n");
            enqueueCommandToServer(keepAliveCmd);          // Send Keep alive
        }

        size_t count = mCommandQueue.size();
		current_time_in_ms = timer::milliseconds();
        if (count) 
		{ 
			BluetoothBLESrvrCmd& cmd = mCommandQueue.front();

            //Previous code had a 30msec sleep, presumably for command issuing problems. We want to maintain this
            //separation between commands, but allow received packets to be processed in the meantime.

            if (!cmd.dispatched && cmdPipeConnected &&
				count > 0 && !isPaused() && current_time_in_ms - past_time_in_ms > 30) {
                if (mController.flags() & BluetoothController::FL_DEBUG)
                {
//                    std::cout << "[BluetoothCommunicationThread] Sending command " << cmd.msg.msgId << std::endl;
                }
				dispatchCommand(cmd);
				if (cmd.msg.msgId != BT_DISCONNECTING_CMD_PIPE)
                    mDispatchQueue.push(cmd);
                else cmdPipeConnected = false;
                dequeueCommand();
				past_time_in_ms = current_time_in_ms;
			}

            // Note that this receive packet processing does not pass response packets
            // back to the caller (we receive and toss them here).  
            // This functionality may need to be added at some point.  So far this is only
            // used to set LED states, so responses are not critical.
            
            if (mDispatchQueue.size() > 0) {    // A command has been dispatched, start a readEvent for
                BluetoothBLESrvrCmd& responseCmd = mDispatchQueue.front();
                if (!responseCmd.asyncReadStarted) {
                    DWORD len;
                    if (StartPipeResponseRead(responseCmd, readEvent, &len)) {
                        ProcessPipeResponse(responseCmd, len);
                        if (responseCmd.ptr) 
                            memcpy(responseCmd.ptr, &responseCmd.msg, sizeof(BTPipeMessage));
                        mDispatchQueue.pop();
                        
                        if (responseCmd.triggerEvent) 
                            SetEvent(responseCmd.responseEvent);
                    }
                }
                else {  // Asynchronous read case
                    if(WaitForSingleObject(readEvent, 0)==WAIT_OBJECT_0) {
                        ResetEvent(readEvent);
                        DWORD len;
                        GetOverlappedResult(mController.mCmdPipeHandle, &responseCmd.mOverlapped, &len, 0);
                        ProcessPipeResponse(responseCmd, len);
                        if (responseCmd.ptr) 
                            memcpy(responseCmd.ptr, &responseCmd.msg, sizeof(BTPipeMessage));
                        mDispatchQueue.pop();
                        if (responseCmd.triggerEvent) 
                            SetEvent(responseCmd.responseEvent);
                    }
                }
			}
		}
		//msleep(2);
	}

	// Deinitialization
	{
 		const BluetoothController::DeviceMap& devices = mController.devices();
		// We need to stop streaming and disconnect from all devices immediately.
		for (auto it = devices.begin(); it != devices.end(); it++) {
		//for_each(devices.begin(), devices.end(), [&](DWORD64, deviceID, BluetoothImuDevice* device) {
			BluetoothImuDevice *device = it->second;
			if (device->isPipeConnected())
			{
				device->disconnect(false); // send immediately.
			}
		}
	}
}

void BluetoothReadThread::run() {

    // Initialization
    {
        QMutexLocker locker(&mMutex);
        mStop = false;
    }

    // process any received stream packets
    while (!mStop) {
        mController.mMutex.lock();
		for (auto it = mController.mDevices.begin(); it != mController.mDevices.end(); it++) {
			BluetoothImuDevice *pInst = it->second;
//			printf("%s: pInst=%8.8x\n", __FUNCTION__, pInst);

			if (pInst && pInst->isPipeConnected())
				mController.receivePacket(pInst);
		}
        mController.mMutex.unlock();
//		msleep(1);
    }
}

void BluetoothCommunicationThread::dispatchCommand(BluetoothBLESrvrCmd& cmd)
{
    mController.SendPipeRequestAsync(mController.mCmdPipeHandle, (BTPipeMessage *)&cmd.msg); 
	cmd.dispatched = true;
}

bool BluetoothCommunicationThread::StartPipeResponseRead(BluetoothBLESrvrCmd& cmd, HANDLE readEvent, DWORD *bytesRead) {
	bool rc;

    memset(&cmd.mOverlapped, 0, sizeof(OVERLAPPED));
    cmd.mOverlapped.hEvent = readEvent;
    rc = ReadFile(mController.mCmdPipeHandle, (BTPipeMessage *)&cmd.msg, sizeof(BTPipeMessage), bytesRead, &cmd.mOverlapped);
    if (!rc && GetLastError() == ERROR_IO_PENDING)
        cmd.asyncReadStarted = true;
	return(rc);
}

BTPipeMsgReturnCode BluetoothCommunicationThread::ProcessPipeResponse(BluetoothBLESrvrCmd& cmd, DWORD len) {
    unsigned int connectCnt;
	const BTPipeMessage *msg = &cmd.msg;

//    if (mController.flags() & BluetoothController::FL_DEBUG)
//        std::cout << "Response for msgId " << msg->msgId << " rc=" << msg->rc << std::endl;

    if (len != sizeof(BTPipeMessage)) {
        std::cout << "Invalid message size!" << endl;
    }

	int deviceCnt;
	unsigned int input;
    bool recoveryThreadStarted = false;

	switch (msg->msgId) {
	case BT_SCAN:
	case BT_DEVICE_INFO:
	case BT_SET_LED:
	case BT_SET_MAC_ADDRESSES:
	case BT_READ_CHARACTERISTIC:
	case BT_WRITE_CHARACTERISTIC:
	case BT_ENABLE_NOTIFICATION:
	case BT_DISABLE_NOTIFICATION:
	case BT_DISCONNECTING_CMD_PIPE:
    case BT_DISCONNECTING_DATA_PIPE:
        return(msg->rc);

    case BT_KEEP_ALIVE:        
        extern TBleVvWidget *gpWidget;
        connectCnt = 0;
		input = msg->param[0];
        deviceCnt = input < 2 ? input : 2;
//		printf("Keep-Alive Response: %d devices", deviceCnt);
        for(int x=0; x < deviceCnt*3; x+=3) {
//            printf(" %d", x);
			DWORD64 mac = (DWORD64)msg->param[1+x] << 32 | msg->param[1+x+1];	// The MAC address of the device is first
//printf("KA resp: devCnt %d MAC %llX\n", deviceCnt, mac);
            mController.mMutex.lock();
            if (mController.mDevices.find(mac) != mController.mDevices.end()) {
                BluetoothImuDevice *device = mController.mDevices[mac];
                if (msg->param[1+x+2] == 0) {								// The device status
                    device->setDisconnected();
                }
                else {
                    device->setConnected(); 
                    connectCnt++;
                }
            } 
			else {
                // Device not found, Start scan thread if needed...
                if (!recoveryThreadStarted) {
                    printf("%llX Not found, status %d", mac, msg->param[1+x+2]);
                    if (msg->param[1+x+2]) {
                        printf(" Starting Device Recovery Thread...\n");
                        mController.mDeviceRecoveryThread.mMACQueue.push(mac);
                        mController.mDeviceRecoveryThread.start();
                        recoveryThreadStarted = true;
                    }
                    else printf("\n");
                }
                else
                    printf("%llX Not found, scan thread already running...\n", mac);
            }
            mController.mMutex.unlock();
        }

        if (connectCnt < 2) {
            for (map<DWORD64,BluetoothImuDevice*>::iterator it = mController.mDevices.begin(); it != mController.mDevices.end(); it++) {
                mController.mMutex.lock();
                BluetoothImuDevice* device = it->second;
                printf("pipeC %d devCon %d\n", device->isPipeConnected(), device->isDeviceConnected());

                if (device->mInitComplete && !device->isDeviceConnected()) {
                    DWORD64 mac = device->mMACAddr;
                    int row = gpWidget->GetListRow(mac);
                    printf("Removing device MAC %llX\n", mac);
                    mController.removeDevice(mac);                

                    printf("Removing device MAC %llX from list row %d\n", mac, row);
                    gpWidget->RemoveDevice(row);
                    printf("Removing device MAC %llX (done)\n", mac);
                    printf("Removing device idx %llX (deleted)\n", mac);
                    mController.mMutex.unlock();   
                    break;  // when missing, only remove one device per keep alive
                }
                mController.mMutex.unlock();   
            }
        }
		return(msg->rc);

	default:
		printf("Invalid pipe response %d!\n", msg->msgId);
	}
	return(BT_FAIL);
}

bool BluetoothCommunicationThread::enqueueCommandToServer(BluetoothBLESrvrCmd& cmd)
{
	QMutexLocker locker(&mMutex);
	bool result = false;

//	printf("enqueueCommandToServer: msgId %d mstop %d\n", cmd.msg.msgId, mStop);
	if (!mStop)
	{
		cmd.dispatched = false;
		cmd.asyncReadStarted = false;
		mCommandQueue.push(cmd);
		result = true;
		if (mController.flags() & BluetoothController::FL_DEBUG)
		{
//			std::cout << "[BluetoothCommunicationThread] Command " << cmd.msg.msgId << " enqueued." << std::endl;
		}
	}
	else
	{
		if (mController.flags() & BluetoothController::FL_DEBUG)
		{
//			std::cout << "[BluetoothCommunicationThread] Command " << cmd.msg.msgId << " ignored since queue is stopped." << std::endl;
		}
	}

	return result;
}


BluetoothBLESrvrCmd& BluetoothCommunicationThread::peekCommand() 
{
	QMutexLocker locker(&mMutex);

//	if (mCommandQueue.size() > 0)
	{
//		printf("peekCommand: cmdQSz %d\n", mCommandQueue.size());
		return mCommandQueue.front();
	}
/*	else
	{
		if (mController.flags() & BluetoothController::FL_DEBUG)
		{
			std::cout << "[BluetoothCommunicationThread] ERROR: peekCommand() called on empty queue!" << std::endl;
		}

		throw std::exception("Queue is empty but trying to peek command");
	} */
}

void BluetoothCommunicationThread::dequeueCommand()
{
	QMutexLocker locker(&mMutex);

	if (mCommandQueue.size())
	{
		mCommandQueue.pop();
	}
}

size_t BluetoothCommunicationThread::queueSize() const
{
	QMutexLocker locker(&mMutex);
	return mCommandQueue.size();
}

void BluetoothCommunicationThread::clearQueue()
{
	QMutexLocker locker(&mMutex);
	while (!mCommandQueue.empty())
	{
		mCommandQueue.pop();
	}
}
