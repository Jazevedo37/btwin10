//____________________________________________________________________________ 
// 
// (c) 2015, Mohamed Mahfouz. All Rights Reserved 
// 
// The contents of this file may not be disclosed, copied or duplicated in 
// any form, in whole or part, without the prior written permission of 
// Mohamed Mahfouz. 
//____________________________________________________________________________ 
// 
// Author: Joe Marrero (jmarrero@adextechnologies.com)
// Date: 2015/04/25
//____________________________________________________________________________
#include <Windows.h>
#include "Timer.h"

namespace timer {

	inline int64_t frequency()
	{
		LARGE_INTEGER li;
		QueryPerformanceFrequency(&li);
		return static_cast<int64_t>(li.QuadPart);
	}

	inline int64_t clock()
	{
		LARGE_INTEGER li;
		QueryPerformanceCounter(&li);
		return static_cast<int64_t>(li.QuadPart);
	}

	const double clockFrequency = frequency() / 1000.0;

	double milliseconds()
	{
		return clock() / clockFrequency;
	}

	double seconds()
	{
		return milliseconds() * 1000.0;
	}


} // namespace timer