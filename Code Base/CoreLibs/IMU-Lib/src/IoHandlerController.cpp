#include "IoHandlerController.h"

IoHandlerController::IoHandlerController(const QString& resourcesPath)
	: mIOHandler(resourcesPath)
{
	mIOHandler.moveToThread(&mIOHandlerThread);
	mIOHandlerThread.start();
}

IoHandlerController::~IoHandlerController()
{
	mIOHandlerThread.quit();
	mIOHandlerThread.wait();
}

void IoHandlerController::EndThread()
{
	mIOHandlerThread.quit();
	mIOHandlerThread.wait();
}