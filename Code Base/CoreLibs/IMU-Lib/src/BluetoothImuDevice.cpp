//____________________________________________________________________________ 
// 
// (c) 2019, All Rights Reserved 
// 
// The contents of this file may not be disclosed, copied or duplicated in 
// any form, in whole or part, without the prior written permission of 
// Mohamed Mahfouz. 
//____________________________________________________________________________ 
// 
// Author: Dan Rebello (dan@streamserverlabs.com)
// Date: 2019/10/13
//____________________________________________________________________________
#include <sstream>
#include <iomanip>
#include <QDateTime>
#include "BluetoothImuDevice.h"
#include "BluetoothController.h"
#include "BluetoothCommunicationThread.h"
#include "apitypes.h"
#include "Timer.h"


const uint8 PRIMARY_SERVICE_UUID[]   = { 0x00, 0x28 };					///< UUID of any "primary service" entry is 0x2800, used when searching for services
const uint8 SECONDARY_SERVICE_UUID[] = { 0x01, 0x28 };					///< UUID of any "secondary service" entry is 0x2801

const uint8 STREAMING_NOTIFICATIONS_ENABLE[]  = { 0x01, 0x00 };			///< Value to enable data streaming notifications
const uint8 STREAMING_NOTIFICATIONS_DISABLE[] = { 0x00, 0x00 };			///< Value to disable data streaming notifications
const uint8 StartIMUs[] = { 0x1D };
const uint8 StopIMUs[] = { 0x1E };


const uint8 LED_STATE_ALL_OFF[]            = { 0x00 };					///< Value to turn off all LEDs
const uint8 LED_STATE_RED_ON[]             = { 0x01 };					///< Value to turn on the red LED
const uint8 LED_STATE_YELLOW_ON[]          = { 0x02 };					///< Value to turn on the yellow LED
const uint8 LED_STATE_GREEN_ON[]           = { 0x03 };					///< Value to turn on the green LED
const uint8 LED_STATE_RED_BLINK[]          = { 0x04 };					///< Value to turn on the red blinking LED
const uint8 LED_STATE_YELLOW_BLINK[]       = { 0x05 };					///< Value to turn on the yellow blinking LED
const uint8 LED_STATE_GREEN_BLINK[]        = { 0x06 };					///< Value to turn on the green blinking LED
const uint8 LED_STATE_RED_YELLOW_BLINK[]   = { 0x07 };					///< Value to turn on the red and yellow blinking LEDs
const uint8 LED_STATE_YELLOW_GREEN_BLINK[] = { 0x08 };					///< Value to turn on the yellow and green blinking LEDs
const uint8 LED_STATE_RED_GREEN_BLINK[]    = { 0x0a };					///< Value to turn on the red and green blinking LEDs
const uint8 LED_STATE_ALL_BLINK[]          = { 0x0b };					///< Value to turn on the all LEDs blinking
const uint8 LED_STATE_PERMANENT_LOCKDOWN[] = { 0x09 };					///< Value to put the IMU into permanent lockdown



BluetoothImuDevice::BluetoothImuDevice(BluetoothController& ctl, unsigned __int64 macAddr, const bd_addr& address, gap_address_type addressType, const QString& name, int rssi)
	: QObject(nullptr)
	, mController(ctl)
	, mAddress(address)
	, mAddressType(addressType)
	, mIsStreaming(false)
	, mLedState(TURN_ON_RED_LED_BLINKING)
	, mRSSI(rssi)
	, mName(name)
	, mState(UNINITIALIZED)
	, mData()
	, mbHasDeviceBeenAddedToGui(false)
	, mInitializedTraceOutput(false)
    , mMACAddr(macAddr)
    , mIsPipeConnected(false)
    , mIsDeviceConnected(false)
    , mInitComplete(false)
{

    char tmp[128];
    sprintf(tmp, "\\\\.\\pipe\\btserver-data%llX", mMACAddr);
    if ((mDataPipe = CreateFileA((LPCSTR)tmp, GENERIC_READ | GENERIC_WRITE,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL)) == INVALID_HANDLE_VALUE) {

        printf("Error %d opening data pipe %s.\n", GetLastError(), tmp);
        return;
    }
    else printf("DATA Pipe %s opened for server MAC %llX.\n", tmp, mMACAddr);

    mIsPipeConnected = true;
    mDataPipeOpen = true;

	if (T_IS_LOGGING_TRACE) {
        QDateTime myDateTime = QDateTime::currentDateTime();
        QString dateTimeStr = myDateTime.toString("hh_mm_ss__MM_dd_yyyy");
        QString mTraceOutputFilename;

        char macstr[20];
        formatMAC(mMACAddr, macstr, sizeof(macstr));
        macstr[2] = '-';
        macstr[5] = '-';
        macstr[8] = '-';
        macstr[11] = '-';
        macstr[14] = '-';
        mTraceOutputFilename = "./BLE-Packet-Data-Stream-at " + dateTimeStr + "-" + macstr + ".txt";
        QByteArray qba;
        qba.append(mTraceOutputFilename);
        mTraceOutputHandle.open(mTraceOutputFilename.toStdString().data());
        if (!mTraceOutputHandle.is_open()) 
            printf("%s: %s open failed.\n", __FUNCTION__, qba.data());

        mInitializedTraceOutput = true;
    }                       
}

void BluetoothImuDevice::Init() {
	char tmpbuff[256];
    char *ptr = tmpbuff;

	if( (ptr = BLEReadUUIDString(BluetoothImuDevice::CHARACTERISTIC_MODEL_NUMBER_UUID, ptr, sizeof(tmpbuff)) )) {
		mModelNumber = mModelNumber.fromUtf8(ptr, -1);
        printf("Model: %s\n", tmpbuff);
        if( (ptr = BLEReadUUIDString(BluetoothImuDevice::CHARACTERISTIC_SERIAL_NUMBER_UUID, ptr, sizeof(tmpbuff)) )) {
            mSerialNumber  = mSerialNumber.fromUtf8(ptr, -1);
            printf("Serial: %s\n", tmpbuff);
            if( (ptr = BLEReadUUIDString(BluetoothImuDevice::CHARACTERISTIC_FIRMWARE_REVISION_UUID, ptr, sizeof(tmpbuff)) )) {
                mFirmwareRevision = mFirmwareRevision.fromUtf8(ptr, -1);
                printf("Firmware Revision: %s\n", tmpbuff);
                if( (ptr = BLEReadUUIDString(BluetoothImuDevice::CHARACTERISTIC_HARDWARE_REVISION_UUID, ptr, sizeof(tmpbuff)) )) {
                    mHardwareRevision = mHardwareRevision.fromUtf8(ptr, -1);
                    printf("Hardware Revision: %s\n", tmpbuff);
                    if( (ptr = BLEReadUUIDString(BluetoothImuDevice::CHARACTERISTIC_SOFTWARE_REVISION_UUID, ptr, sizeof(tmpbuff)) )) {
                        mSoftwareRevision = mSoftwareRevision.fromUtf8(ptr, -1);
                        printf("Software Revision: %s\n", tmpbuff);
                        if( (ptr = BLEReadUUIDString(BluetoothImuDevice::CHARACTERISTIC_MANUFACTURER_NAME_UUID, ptr, sizeof(tmpbuff)) )) {
                            mManufacturer = mManufacturer.fromUtf8(ptr, -1);
                            printf("Manufacturer Name: %s\n", tmpbuff);
                        }
                    }
                }
            }
        }
    }
}

BluetoothImuDevice::~BluetoothImuDevice() {

    if (mDataPipeOpen) {
        printf("\n%s: Destructor: Device %llX Closing data pipe.\n", __FUNCTION__, mMACAddr);
        CloseHandle(mDataPipe);
        mDataPipeOpen = false;
        if(mIsPipeConnected)
            BLEDisconnectDataPipe(0);
        if (T_IS_LOGGING_TRACE)
            mTraceOutputHandle.close();
    }
    else 
        printf("\n%s: Destructor: Device %llX Data pipe already closed.\n", __FUNCTION__, mMACAddr);
}

bd_addr BluetoothImuDevice::address() const
{
	//QMutexLocker locker(&mMutex);
	return mAddress;
}

QString BluetoothImuDevice::addressString() const
{
	// initialize ostringstream
	std::ostringstream oss;
	oss << std::uppercase << std::setfill('0') << std::setw(2) << std::hex;

	int numBytes = 6;
	for (int i = 0; i < numBytes; ++i)
	{
		char byteStr[3];
		sprintf(byteStr, "%02x", static_cast<int>(mAddress.addr[numBytes - 1 - i]));

		if (i == numBytes-1)
		{
			oss << byteStr;
		}
		else
		{
			oss << byteStr << ":";
		}
	}

	return QString::fromStdString(oss.str());
}

gap_address_type BluetoothImuDevice::addressType() const
{
	//QMutexLocker locker(&mMutex);
	return mAddressType;
}

short BluetoothImuDevice::connectionHandle() const
{
	//QMutexLocker locker(&mMutex);
	return mConnectionHandle;
}

unsigned __int64 BluetoothImuDevice::getDeviceHandle() const
{
    return(mMACAddr);
}

bool BluetoothImuDevice::isAddressPublic() const
{
	//QMutexLocker locker(&mMutex);

	// True means that the address is public.
	// False means that the address is random.
	return mAddressType == gap_address_type_public;
}

const BleDataPacket& BluetoothImuDevice::data() const
{
	//QMutexLocker locker(&mMutex);
	return mData;
}

BluetoothImuDevice::LedState BluetoothImuDevice::ledState() const
{
	return mLedState;
}

void BluetoothImuDevice::connect() 
{
	if (!isPipeConnected())
	{
		// Notify others that this device is starting to connect.
        emit connecting(*this);

        char tmp[128];
        sprintf(tmp, "\\\\.\\pipe\\btserver-data%llX", mMACAddr);
        if ((mDataPipe = CreateFileA((LPCSTR)tmp, GENERIC_READ | GENERIC_WRITE,
            FILE_SHARE_READ | FILE_SHARE_WRITE, 
            NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL)) == INVALID_HANDLE_VALUE) {

            printf("Error %d opening data pipe %s.", GetLastError(), tmp);
            return;
        }
        else printf("DATA Pipe %s opened for server MAC %llX.\n", tmp, mMACAddr);

        mIsPipeConnected = true;
        mDataPipeOpen = true;
        setState(__READY);
    }
	else
	{
		emit mController.errorOccurred(BluetoothController::ERR_DEVICE_ALREADY_CONNECTED);
	}
}

void BluetoothImuDevice::disconnect(bool queue)
{
	if (isPipeConnected())
	{
		if (isStreaming())
		{
			// Disable streaming before disconnecting.
			disableStreaming();
			Sleep(200);			// Let any data in progress finish
		}
        BLEDisconnectDataPipe(0);

        CloseHandle(mDataPipe);
        mDataPipeOpen = false;

		// update controller state
        setState(UNINITIALIZED);
	}
	else
	{
		emit mController.errorOccurred(BluetoothController::ERR_DEVICE_NOT_CONNECTED);
	}
}

bool BluetoothImuDevice::isPipeConnected() const
{
    return(mIsPipeConnected);
}

void BluetoothImuDevice::checkStreaming() const
{
	if (isPipeConnected())
	{
	}
	else
	{
		emit mController.errorOccurred(BluetoothController::ERR_DEVICE_NOT_CONNECTED);
	}
}

void BluetoothImuDevice::enableStreaming()
{
	if (isPipeConnected())
	{
		BLEEnableNotification(0xFFF4);

		// update controller state
//		mController.setState(BluetoothController::LISTENING_MEASUREMENTS);
//		emit mController.newControllerState(mController.state());
		mIsStreaming = true;

		printf("Starting IMU Streaming %llX...\n", mMACAddr);
	}
}

void BluetoothImuDevice::disableStreaming()
{
	if (isPipeConnected())
	{
		printf("Stopping IMU Streaming %llX...\n", mMACAddr);
		BLEDisableNotification(0xFFF4, INFINITE);
		mIsStreaming = false;
		Sleep(200);			// Let pipe flush out.
	}
}

bool BluetoothImuDevice::isStreaming() const
{
	return mIsStreaming;
}

bool BluetoothImuDevice::checkStreamingByte() const
{
	//QMutexLocker locker(&mMutex);
	return (mStreamingStatusByte == 0x01);
}

void BluetoothImuDevice::requestLedState(LedState state)
{
	size_t size = 0;
	const uint8* value = NULL;

	switch (state) {
		case TURN_ON_RED_LED:
			size = sizeof(LED_STATE_RED_ON);
			value = LED_STATE_RED_ON;
			break;
		case TURN_ON_YELLOW_LED:
			size = sizeof(LED_STATE_YELLOW_ON);
			value = LED_STATE_YELLOW_ON;
			break;
		case TURN_ON_GREEN_LED:
			size = sizeof(LED_STATE_GREEN_ON);
			value = LED_STATE_GREEN_ON;
			break;
		case TURN_OFF_ALL_LEDS:
			size = sizeof(LED_STATE_ALL_OFF);
			value = LED_STATE_ALL_OFF;
			break;
		case PUT_INTO_PERMANENT_LOCKDOWN:
			size = sizeof(LED_STATE_PERMANENT_LOCKDOWN);
			value = LED_STATE_PERMANENT_LOCKDOWN;
			break;
		case TURN_ON_RED_LED_BLINKING:
			size = sizeof(LED_STATE_RED_BLINK);
			value = LED_STATE_RED_BLINK;
			break;
		case TURN_ON_YELLOW_LED_BLINKING:
			size = sizeof(LED_STATE_YELLOW_BLINK);
			value = LED_STATE_YELLOW_BLINK;
			break;
		case TURN_ON_GREEN_LED_BLINKING:
			size = sizeof(LED_STATE_GREEN_BLINK);
			value = LED_STATE_GREEN_BLINK;
			break;
		case TURN_ON_ALL_LEDS_BLINKING:
			size = sizeof(LED_STATE_ALL_BLINK);
			value = LED_STATE_ALL_BLINK;
			break;
		case TURN_ON_RED_GREEN_LEDS_BLINKING:
			size = sizeof(LED_STATE_RED_GREEN_BLINK);
			value = LED_STATE_RED_GREEN_BLINK;
			break;
		case TURN_ON_RED_YELLOW_LEDS_BLINKING:
			size = sizeof(LED_STATE_RED_YELLOW_BLINK);
			value = LED_STATE_RED_YELLOW_BLINK;
			break;
		case TURN_ON_YELLOW_GREEN_LEDS_BLINKING:
			size = sizeof(LED_STATE_YELLOW_GREEN_BLINK);
			value = LED_STATE_YELLOW_GREEN_BLINK;
			break;
		case NONE: // fall-through
			default:
			return;
	}

    BluetoothBLESrvrCmd cmd;
    memset(&cmd, 0, sizeof(BluetoothBLESrvrCmd));
    cmd.msg.msgId = BT_WRITE_CHARACTERISTIC;
    cmd.msg.macAddr = mMACAddr;
    cmd.msg.param[0] = BluetoothImuDevice::CHARACTERISTIC_LED_UUID;
    cmd.msg.buffer[0] = (unsigned char)value[0];
    cmd.msg.bufLen = 1;
	mController.communicationThread().enqueueCommandToServer(cmd);
}

void BluetoothImuDevice::requestSignalStrength() const
{
	if (isPipeConnected())
	{
	}
	else
	{
		emit mController.errorOccurred(BluetoothController::ERR_DEVICE_NOT_CONNECTED);
	}
}

void BluetoothImuDevice::updateConnection(uint16 interval_min, uint16 interval_max, uint16 latency, uint16 timeout) const
{
	if (isPipeConnected())
	{
	}
	else
	{
		emit mController.errorOccurred(BluetoothController::ERR_DEVICE_NOT_CONNECTED);
	}
}

#if 1
void BluetoothImuDevice::lockdown()
{
//	T_LOG_DEBUG("Putting IMU in Lockdown");
//	mbTryingToLockdownImu = true;
//	requestLedState(PUT_INTO_PERMANENT_LOCKDOWN);
//	requestLedState(PUT_INTO_PERMANENT_LOCKDOWN);
}
#endif

int BluetoothImuDevice::signalStrength() const
{
	//QMutexLocker locker(&mMutex);
	return mRSSI;
}

BluetoothImuDevice::DeviceState BluetoothImuDevice::state() const
{
	//QMutexLocker locker(&mMutex);
	return mState;
}

void BluetoothImuDevice::setState(DeviceState state)
{
	if (state != mState)
	{
		mState = state;

		// Notify others that the state has changed.
		//emit stateChanged(*this);

		// make sure this device has been added to the GUI
		if (!mbHasDeviceBeenAddedToGui)
		{
            printf("%s: HasDeviceBeenAddedToGui: %s.\n", __FUNCTION__, mbHasDeviceBeenAddedToGui ? "true" : "false");
			QString deviceString;
			deviceString = addressString();
            printf("Add to GUI: deviceHandle %llx\n", getDeviceHandle());
			emit mController.addNewDevice(deviceString, mMACAddr, addressString(), connectionHandle(), true);
		}
		emit mController.newDeviceState(mMACAddr);
	}
}

void BluetoothImuDevice::uninitialize()
{
	//QMutexLocker locker(&mMutex);
	mState                       = UNINITIALIZED;
	emit mController.newDeviceState(mMACAddr);
}

bool BluetoothImuDevice::hasDeviceBeenAddedToGui() const
{
	return mbHasDeviceBeenAddedToGui;
}

void BluetoothImuDevice::setStateDeviceAddedToGui(bool newState)
{
    printf("%s: State=%s.\n", __FUNCTION__, newState ? "true" : "false");
	mbHasDeviceBeenAddedToGui = newState;
}

int BluetoothImuDevice::getRSSI() const
{
	return mRSSI;
}

void BluetoothImuDevice::updateLedState(LedState state)
{
	mLedState = state;
}

char *BluetoothImuDevice::BLEReadUUIDString(unsigned int uuid, char *ptr, unsigned int len) {
	BTPipeMessage msg;
	BTPipeMsgReturnCode rc;

    printf("%s: Reading UIID %x:\n", __FUNCTION__, uuid);
	memset(&msg, 0, sizeof(BTPipeMessage));
	msg.msgId = BT_READ_CHARACTERISTIC;
	msg.macAddr = mMACAddr;
	msg.param[0] = uuid;
	
	rc = mController.SendPipeRequest2(&msg, INFINITE);
	
	if (rc == 0 && msg.bufLen && msg.bufLen+1 < len) {
		memcpy(ptr, (const char *)msg.buffer, msg.bufLen);
		*(ptr + msg.bufLen) = 0;							// Terminate it
	}
    if (rc) {
        printf("%s: SendPipeRequest2 Error 0x%X\n", __FUNCTION__, rc);
		*ptr = 0;											// Terminate string
    }
    return (ptr);
}

int BluetoothImuDevice::BLEReadUUIDBytes(unsigned int uuid, char *ptr, unsigned int len) {
	BTPipeMessage msg;
	BTPipeMsgReturnCode rc;

	//printf("Reading UIID %x:\n", uuid);
	memset(&msg, 0, sizeof(BTPipeMessage));
	msg.msgId = BT_READ_CHARACTERISTIC;
	msg.macAddr = mMACAddr;
	msg.param[0] = uuid;
	rc = mController.SendPipeRequest2(&msg, INFINITE);

	if (rc == 0 && msg.bufLen && msg.bufLen <= len) {
		memcpy(ptr, (const char *)msg.buffer, len < msg.bufLen ? len : msg.bufLen);		
	}
	return(msg.bufLen);
}

int BluetoothImuDevice::BLEEnableNotification(unsigned int uuid) {
	BTPipeMessage msg;
	BTPipeMsgReturnCode rc;

	printf("BLEEnableNotification: 0x%x\n", uuid);
	memset(&msg, 0, sizeof(BTPipeMessage));
	msg.msgId = BT_ENABLE_NOTIFICATION;
	msg.macAddr = mMACAddr;
	msg.param[0] = uuid;
	rc = mController.SendPipeRequest2(&msg, INFINITE);
	printf("BLEEnableNotification: Complete.\n");
	return(0);
}

int BluetoothImuDevice::BLEDisableNotification(unsigned int uuid, DWORD timeout) {
	BTPipeMessage msg;
	BTPipeMsgReturnCode rc;

	printf("BLEDisableNotification: MAC %llX UUID 0x%x\n", mMACAddr, uuid);
	memset(&msg, 0, sizeof(BTPipeMessage));
	msg.msgId = BT_DISABLE_NOTIFICATION;
	msg.macAddr = mMACAddr;
	msg.param[0] = uuid;
	rc = mController.SendPipeRequest2(&msg, timeout);
	printf("BLEDisableNotification: Complete.\n");
	return(0);
}

int BluetoothImuDevice::BLEDisconnectDataPipe(DWORD timeout) {
	BTPipeMessage msg;
	BTPipeMsgReturnCode rc;

	mIsPipeConnected = false;

	printf("BLEDisconnectDataPipe: MAC %llX\n", mMACAddr);
	memset(&msg, 0, sizeof(BTPipeMessage));
	msg.msgId = BT_DISCONNECTING_DATA_PIPE;
	msg.macAddr = mMACAddr;
	rc = SendPipeRequest(mController.mCmdPipeHandle, &msg);
	printf("BLEDisconnectDataPipe: MAC %llX Complete.\n", mMACAddr);
	return(0);
}


