#include "BleVvWidget.h"
#include "..\..\..\BLEServer\pipeprot.h"
#include <iostream>

namespace
{
	static const char* bluetoothDeviceStateStrings[] =
	{
		"Unitialized/Terminated",
		"Finding Services",
		"Finding Characteristics",
		"Ready",
		"Reading Data",
		"Writing Data",
		"Device State Count"
	};
}

TBleVvWidget::TBleVvWidget(QWidget* parent, Qt::WindowFlags f, const QString& resourcesPath) : QDialog(parent, f),
mIsComportConnected(false)
{
    printf("%s:\n", __FUNCTION__);
	mUI.setupUi(this);

	if (PROCESS_ONLY_ONE_BLE_PACKET)
	{
		mUI.radioButtonOneDataPacket->setChecked(true);
	}
	else if (PROCESS_ONLY_TWO_BLE_PACKETS)
	{
		mUI.radioButtonTwoDataPackets->setChecked(true);
	}

    qRegisterMetaType<DWORD64>("DWORD64");

	// get access to the main IoHandlerController object
	//mIOHandlerController = mImuData->GetIoHandlerController();
	mIOHandlerController = new IoHandlerController("./resources");


	// default states
	mUI.checkBoxBleLeds->setChecked(true);
	mUI.checkBoxBleDeviceStringFiltering->setChecked(false);

	// get current min and max connection intervals
	unsigned int minVal(0), maxVal(0);
	mUI.lineEditMaxConnInterval->setText(QString::number(maxVal));
	mUI.lineEditMinConnInterval->setText(QString::number(minVal));

	// get the current filter string
	mUI.lineEditScanFilterString->setText("");

	// connect the signals and slots
	connect(mUI.pushButtonScan, SIGNAL(clicked()), this, SLOT(ScanForDevices()));
	connect(mUI.pushButtonDevicePermanentLockdown, SIGNAL(clicked()), this, SLOT(SendPermanentLockdownToDevice()));
	connect(mUI.pushButtonDeviceSoftwareReset, SIGNAL(clicked()), this, SLOT(SendSoftwareResetToDevice()));
	connect(mUI.pushButtonReinitializeDongle, SIGNAL(clicked()), this, SLOT(ReinitializeBleDongle()));
	connect(mUI.pushButtonRelinkAll, SIGNAL(clicked()), this, SLOT(RelinkAllBleDevices()));
	connect(mUI.pushButtonRelinkDevice, SIGNAL(clicked()), this, SLOT(RelinkSingleDevice()));
	connect(mUI.pushButtonResetDongle, SIGNAL(clicked()), this, SLOT(ResetBleDongle()));
	connect(mUI.pushButtonSendBleCommand, SIGNAL(clicked()), this, SLOT(SendBleCommandToDevice()));
	connect(mUI.pushButtonStartImuStreaming, SIGNAL(clicked()), this, SLOT(StartImuStreamingAllDevices()));
	connect(mUI.pushButtonStopImuStreaming, SIGNAL(clicked()), this, SLOT(StopImuStreamingAllDevices()));
	connect(mUI.pushButtonTerminateActiveLinks, SIGNAL(clicked()), this, SLOT(TerminateAllActiveLinks()));
	connect(mUI.pushButtonTerminateLinkDevice, SIGNAL(clicked()), this, SLOT(TerminateLinkSingleDevice()));
	connect(mUI.pushButtonTerminateLinkRequests, SIGNAL(clicked()), this, SLOT(TerminateAllLinkRequests()));
	connect(mUI.pushButtonStartStreamingDevice, SIGNAL(clicked()), this, SLOT(StartStreamingOneDevice()));
	connect(mUI.pushButtonStopStreamingDevice, SIGNAL(clicked()), this, SLOT(StopStreamingOneDevice()));

	connect(mUI.checkBoxBleDeviceStringFiltering, SIGNAL(stateChanged(int)), this, SLOT(EnableFilterStringDeviceLinkingFlagChanged()));
	connect(mUI.checkBoxBleLeds, SIGNAL(stateChanged(int)), this, SLOT(LedFeedbackFlagChanged()));
/*
	connect(mUI.radioButtonBlinkingAllLeds, SIGNAL(toggled(bool)), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonGreenLed, SIGNAL(toggled(bool)), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonNoLed, SIGNAL(toggled(bool)), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonRedLed, SIGNAL(toggled(bool)), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonBlinkingGreenLed, SIGNAL(toggled(bool)), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonBlinkingRedLed, SIGNAL(toggled(bool)), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonBlinkingYellowLed, SIGNAL(toggled(bool)), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonRedGreenLed, SIGNAL(toggled(bool)), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonRedYellowLed, SIGNAL(toggled(bool)), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonYellowGreenLed, SIGNAL(toggled(bool)), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonYellowLed, SIGNAL(toggled(bool)), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonOneDataPacket, SIGNAL(toggled(bool)), this, SLOT(DataFormatChanged()));
	connect(mUI.radioButtonTwoDataPackets, SIGNAL(toggled(bool)), this, SLOT(DataFormatChanged()));
*/

	connect(mUI.radioButtonBlinkingAllLeds, SIGNAL(clicked()), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonGreenLed, SIGNAL(clicked()), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonNoLed, SIGNAL(clicked()), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonRedLed, SIGNAL(clicked()), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonBlinkingGreenLed, SIGNAL(clicked()), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonBlinkingRedLed, SIGNAL(clicked()), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonBlinkingYellowLed, SIGNAL(clicked()), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonRedGreenLed, SIGNAL(clicked()), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonRedYellowLed, SIGNAL(clicked()), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonYellowGreenLed, SIGNAL(clicked()), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonYellowLed, SIGNAL(clicked()), this, SLOT(SendNewLedStateToDevice()));
	connect(mUI.radioButtonOneDataPacket, SIGNAL(clicked()), this, SLOT(DataFormatChanged()));
	connect(mUI.radioButtonTwoDataPackets, SIGNAL(clicked()), this, SLOT(DataFormatChanged()));

	connect(mUI.listWidget, SIGNAL(clicked(const QModelIndex &)), this, SLOT(ListViewItemChanged()));
	connect(mUI.lineEditMinConnInterval, SIGNAL(textEdited(QString)), this, SLOT(ConnIntervalTextEdited()));
	connect(mUI.lineEditMaxConnInterval, SIGNAL(textEdited(QString)), this, SLOT(ConnIntervalTextEdited()));
	connect(mUI.lineEditScanFilterString, SIGNAL(textEdited(QString)), this, SLOT(ScanFilterStringTextEdited()));

	// connect the controller signals and slots
	connect(mIOHandlerController->mIOHandler.mBLEController, SIGNAL(addNewDevice(QString, DWORD64, QString, int, bool)), 
		this, SLOT(AddNewBleDeviceToList(QString, DWORD64, QString, int, bool)));
//	connect(mIOHandlerController->mIOHandler.mBLEController, SIGNAL(updateBleDeviceString(QString, int, QString, int, bool)),
//		this, SLOT(UpdateBleDeviceString(QString, int, QString, int, bool)));
	connect(mIOHandlerController->mIOHandler.mBLEController, SIGNAL(newDeviceState(DWORD64)),
		this, SLOT(BleDeviceStateChanged(DWORD64)));
	connect(mIOHandlerController->mIOHandler.mBLEController, SIGNAL(newControllerState(int)),
		this, SLOT(BleControllerStatedChanged(int)));
	connect(mIOHandlerController->mIOHandler.mBLEController, SIGNAL(updateLedStateGui()),
		this, SLOT(UpdateBleDeviceLedState()));
	connect(mIOHandlerController->mIOHandler.mBLEController, SIGNAL(clearGuiDeviceList()),
		this, SLOT(ClearDeviceListAsync()));


	connect(&mIOHandlerController->mIOHandler, SIGNAL(SignalRefreshRateUpdated(int)),
		this, SLOT(SlotRefreshRateUpdated(int)));
	connect(mIOHandlerController->mIOHandler.mBLEController, SIGNAL(done(int)),
			this, SLOT(quit(int)), Qt::QueuedConnection);

	// make sure controller is setup to correct default for Bluegiga
	BleControllerStatedChanged(mIOHandlerController->mIOHandler.mBLEController->state());

	// set initial state of controls in the GUI
	mUI.labelBleDeviceState->setText("");
	mUI.labelBleDeviceRefreshRate->setText("");
	mUI.labelBleStatus->setText("");

	// default LED setting
	mUI.radioButtonBlinkingRedLed->setChecked(true);

	// initialize the QStringList used to pass data to the IoHandler
	QString newDataString;
	dataPassedToIoHandler.append(newDataString);

	mIOHandlerController->mIOHandler.SetParentWidget(this);

	SetDisconnected();

	QStringList temp;

	// connect the IMU
	ConnectImu();
}

TBleVvWidget::~TBleVvWidget()
{
	delete mIOHandlerController;
}

void TBleVvWidget::SetConnected()
{
	mBleServerConnected = true;
}

void TBleVvWidget::SetDisconnected()
{
	mBleServerConnected = false;
}

bool TBleVvWidget::IsConnected() {
    return mBleServerConnected;
}

void TBleVvWidget::ConnectImu()
{
	if (IsConnected() == false)
	{
		if (mIOHandlerController->mIOHandler.OpenCmdPipe())
		{
			SetConnected();
		}
		else
		{
			if (mNumComPortsConnected > 0)
			{
				mIOHandlerController->mIOHandler.CloseCmdPipe();
			}
			SetDisconnected();
		}
	}
}


int TBleVvWidget::EnumerateImus()
{
	return(mIOHandlerController->mIOHandler.mBLEController->scan());
}

void TBleVvWidget::LedFeedbackFlagChanged()
{
}

DWORD64 TBleVvWidget::MACFromCurrentItem() {
	int macadr[6];
	DWORD64 macAddr = 0;

    if (mUI.listWidget->currentItem()) {
		if(mUI.listWidget->currentItem()->text().toLatin1().constData())
            sscanf((char *)mUI.listWidget->currentItem()->text().toLatin1().constData(),"%x:%x:%x:%x:%x:%x",
                   &macadr[0],&macadr[1],&macadr[2],&macadr[3],&macadr[4],&macadr[5]);

        for(int x=0; x<6; x++) 
            macAddr |= (DWORD64)(macadr[x] & 0xFF) << (5-x)*8;
    }
    printf("%s: MACADDR = %llX\n", __FUNCTION__, macAddr);

    if (macAddr == 0) {
        printf("%s: MACADDR = %llX!!!\n", __FUNCTION__, macAddr);   
    }
    return (macAddr);
}

void TBleVvWidget::EnableFilterStringDeviceLinkingFlagChanged()
{
	bool newFlagState = mUI.checkBoxBleDeviceStringFiltering->isChecked();
	DWORD64 macadr = MACFromCurrentItem();
	mIOHandlerController->mIOHandler.StartNewBleAction(BluetoothController::CHANGE_STATE_DEV_STRING_FILTERING, dataPassedToIoHandler, macadr, newFlagState);
}

void TBleVvWidget::ScanForDevices()
{
	// clear devices
	ClearDevices();
	DWORD64 macadr = 0;		// Starting over, no MACs yet.
	mIOHandlerController->mIOHandler.StartNewBleAction(BluetoothController::SCAN, dataPassedToIoHandler, macadr);
}

void TBleVvWidget::TerminateAllLinkRequests()
{
	DWORD64 macadr = MACFromCurrentItem();
	mIOHandlerController->mIOHandler.StartNewBleAction(BluetoothController::TERMINATE_LINK_REQUESTS, dataPassedToIoHandler, macadr);
}

void TBleVvWidget::TerminateAllActiveLinks()
{
	DWORD64 macadr = MACFromCurrentItem();
	mIOHandlerController->mIOHandler.StartNewBleAction(BluetoothController::TERMINATE_ACTIVE_LINKS, dataPassedToIoHandler, macadr);
}

void TBleVvWidget::RelinkAllBleDevices()
{
	DWORD64 macadr = MACFromCurrentItem();
	mIOHandlerController->mIOHandler.StartNewBleAction(BluetoothController::RELINK_ALL, dataPassedToIoHandler, macadr);
}

void TBleVvWidget::StartImuStreamingAllDevices()
{
	if (mUI.listWidget->count() > 0)
	{
        DWORD64 macadr = MACFromCurrentItem();
		mIOHandlerController->mIOHandler.StartNewBleAction(BluetoothController::START_IMU_STREAMING_ALL, dataPassedToIoHandler, macadr);
	}
}

void TBleVvWidget::StopImuStreamingAllDevices()
{
	if (mUI.listWidget->count() > 0)
	{
        DWORD64 macadr = MACFromCurrentItem();
		mIOHandlerController->mIOHandler.StartNewBleAction(BluetoothController::STOP_IMU_STREAMING_ALL, dataPassedToIoHandler, macadr);
	}
}

void TBleVvWidget::ReinitializeBleDongle()
{
	DWORD64 macadr = MACFromCurrentItem();
	mIOHandlerController->mIOHandler.StartNewBleAction(BluetoothController::REINITIALIZE_DONGLE, dataPassedToIoHandler, macadr);
}

void TBleVvWidget::SendBleCommandToDevice()
{
	if (mUI.listWidget->count() > 0)
	{
        DWORD64 macadr = MACFromCurrentItem();
		mIOHandlerController->mIOHandler.StartNewBleAction(BluetoothController::SEND_BLE_COMMAND, dataPassedToIoHandler, macadr);
	}
}

void TBleVvWidget::SendSoftwareResetToDevice()
{
	if (mUI.listWidget->count() > 0)
	{
        DWORD64 macadr = MACFromCurrentItem();
		mIOHandlerController->mIOHandler.StartNewBleAction(BluetoothController::PERMANENT_LOCKDOWN_DEVICE, dataPassedToIoHandler, macadr);
	}
}

void TBleVvWidget::SendPermanentLockdownToDevice()
{
	if (mUI.listWidget->count() > 0)
	{
        DWORD64 macadr = MACFromCurrentItem();
		mIOHandlerController->mIOHandler.StartNewBleAction(BluetoothController::PERMANENT_LOCKDOWN_DEVICE, dataPassedToIoHandler, macadr);
	}
}

void TBleVvWidget::StartStreamingOneDevice()
{
	if (mUI.listWidget->count() > 0)
	{
        DWORD64 macadr = MACFromCurrentItem();
		mIOHandlerController->mIOHandler.StartNewBleAction(BluetoothController::START_IMU_STREAMING_DEVICE, dataPassedToIoHandler, macadr);
	}
}

void TBleVvWidget::StopStreamingOneDevice()
{
	if (mUI.listWidget->count() > 0)
	{
        DWORD64 macadr = MACFromCurrentItem();
		mIOHandlerController->mIOHandler.StartNewBleAction(BluetoothController::STOP_IMU_STREAMING_DEVICE, dataPassedToIoHandler, macadr);
	}
}

void TBleVvWidget::SendNewLedStateToDevice()
{
//    printf("%s: \n", __FUNCTION__);
	if (mUI.listWidget->count() > 0)
	{
//        printf("widget cnt %d\n", mUI.listWidget->count());
		int numStateNewLedCommand = 0;
		if (mUI.radioButtonRedLed->isChecked())
		{
			numStateNewLedCommand = BluetoothImuDevice::TURN_ON_RED_LED;
		}
		else if (mUI.radioButtonYellowLed->isChecked())
		{
			numStateNewLedCommand = BluetoothImuDevice::TURN_ON_YELLOW_LED;
		}
		else if (mUI.radioButtonGreenLed->isChecked())
		{
			numStateNewLedCommand = BluetoothImuDevice::TURN_ON_GREEN_LED;
		}
		else if (mUI.radioButtonBlinkingRedLed->isChecked())
		{
			numStateNewLedCommand = BluetoothImuDevice::TURN_ON_RED_LED_BLINKING;
		}
		else if (mUI.radioButtonBlinkingYellowLed->isChecked())
		{
			numStateNewLedCommand = BluetoothImuDevice::TURN_ON_YELLOW_LED_BLINKING;
		}
		else if (mUI.radioButtonBlinkingGreenLed->isChecked())
		{
			numStateNewLedCommand = BluetoothImuDevice::TURN_ON_GREEN_LED_BLINKING;
		}
		else if (mUI.radioButtonBlinkingAllLeds->isChecked())
		{
			numStateNewLedCommand = BluetoothImuDevice::TURN_ON_ALL_LEDS_BLINKING;
		}
		else if (mUI.radioButtonRedGreenLed->isChecked())
		{
			numStateNewLedCommand = BluetoothImuDevice::TURN_ON_RED_GREEN_LEDS_BLINKING;
		}
		else if (mUI.radioButtonRedYellowLed->isChecked())
		{
			numStateNewLedCommand = BluetoothImuDevice::TURN_ON_RED_YELLOW_LEDS_BLINKING;
		}
		else if (mUI.radioButtonYellowGreenLed->isChecked())
		{
			numStateNewLedCommand = BluetoothImuDevice::TURN_ON_YELLOW_GREEN_LEDS_BLINKING;
		}
		else if (mUI.radioButtonNoLed->isChecked())
		{
			numStateNewLedCommand = BluetoothImuDevice::TURN_OFF_ALL_LEDS;
		}
		dataPassedToIoHandler[0] = QString::number(numStateNewLedCommand);

        DWORD64 macadr = MACFromCurrentItem();
		mIOHandlerController->mIOHandler.StartNewBleAction(BluetoothController::CHANGE_DEVICE_LED_STATE, dataPassedToIoHandler, macadr);
	}
}

void TBleVvWidget::RelinkSingleDevice()
{
	if (mUI.listWidget->count() > 0)
	{
        DWORD64 macadr = MACFromCurrentItem();
		mIOHandlerController->mIOHandler.StartNewBleAction(BluetoothController::RELINK_DEVICE, dataPassedToIoHandler, macadr);
	}
}

void TBleVvWidget::TerminateLinkSingleDevice()
{
	if (mUI.listWidget->count() > 0)
	{
        DWORD64 macadr = MACFromCurrentItem();
		mIOHandlerController->mIOHandler.StartNewBleAction(BluetoothController::TERMINATE_LINK_DEVICE, dataPassedToIoHandler, macadr);
	}
}

void TBleVvWidget::DataFormatChanged()
{
	bool useTwoMessageDataPacketFlag = true;
	if (mUI.radioButtonOneDataPacket->isChecked())
	{
		useTwoMessageDataPacketFlag = false;
	}

	DWORD64 macadr = MACFromCurrentItem();
	mIOHandlerController->mIOHandler.StartNewBleAction(BluetoothController::CHANGE_DATA_PACKET_FORMAT, dataPassedToIoHandler, macadr, useTwoMessageDataPacketFlag);
}

void TBleVvWidget::ResetBleDongle()
{
	DWORD64 macadr = MACFromCurrentItem();
	mIOHandlerController->mIOHandler.StartNewBleAction(BluetoothController::RESET_DONGLE, dataPassedToIoHandler, macadr);
}

void TBleVvWidget::ListViewItemChanged()
{
    DWORD64 macadr;

    printf("%s: \n", __FUNCTION__);
	// load calibration and device states
	int numDevicesInBleMap = mIOHandlerController->mIOHandler.mBLEController->devices().size();
	if (mUI.listWidget->currentRow() < 0 || mUI.listWidget->currentRow() >= numDevicesInBleMap)
	{
		return;
	}
	
	if( (macadr = MACFromCurrentItem())==0)
        return;

	if (auto it = mIOHandlerController->mIOHandler.mBLEController->devices().find(macadr) == mIOHandlerController->mIOHandler.mBLEController->devices().end())
		return;

	BluetoothImuDevice::DeviceState deviceState = mIOHandlerController->mIOHandler.mBLEController->devices()[macadr]->state();

	switch (deviceState)
	{
	case BluetoothImuDevice::UNINITIALIZED:
		mUI.labelBleDeviceState->setText("Unitialized/Terminated");
		mUI.labelBleDeviceRefreshRate->setText("");
		break;

	case BluetoothImuDevice::FINDING_SERVICES:
		mUI.labelBleDeviceState->setText("Finding Services");
		mUI.labelBleDeviceRefreshRate->setText("");
		break;

	case BluetoothImuDevice::FINDING_CHARACTERISTICS:
		mUI.labelBleDeviceState->setText("Finding Characteristics");
		mUI.labelBleDeviceRefreshRate->setText("");
		break;

	case BluetoothImuDevice::__READY:
		mUI.labelBleDeviceState->setText("Ready");
		mUI.labelBleDeviceRefreshRate->setText("");
		break;

	case BluetoothImuDevice::READING:
		mUI.labelBleDeviceState->setText("Reading Data");
		mUI.labelBleDeviceRefreshRate->setText("");
		break;

	case BluetoothImuDevice::WRITING:
		mUI.labelBleDeviceState->setText("Writing Data");
		mUI.labelBleDeviceRefreshRate->setText("");
		break;

	case BluetoothImuDevice::DEVICE_STATE_COUNT:
		mUI.labelBleDeviceState->setText("Device State Count");
		mUI.labelBleDeviceRefreshRate->setText("");
		break;

	default:
		mUI.labelBleDeviceState->setText("");
		mUI.labelBleDeviceRefreshRate->setText("");
		break;
	}

	// change LED state
	int ledState = mIOHandlerController->mIOHandler.mBLEController->devices()[macadr]->ledState();
	switch (ledState)
	{
	case BluetoothImuDevice::TURN_ON_RED_LED:
		mUI.radioButtonRedLed->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_ON_YELLOW_LED:
		mUI.radioButtonYellowLed->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_ON_GREEN_LED:
		mUI.radioButtonGreenLed->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_ON_RED_LED_BLINKING:
		mUI.radioButtonBlinkingRedLed->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_ON_YELLOW_LED_BLINKING:
		mUI.radioButtonBlinkingYellowLed->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_ON_GREEN_LED_BLINKING:
		mUI.radioButtonBlinkingGreenLed->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_ON_ALL_LEDS_BLINKING:
		mUI.radioButtonBlinkingAllLeds->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_ON_RED_GREEN_LEDS_BLINKING:
		mUI.radioButtonRedGreenLed->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_ON_RED_YELLOW_LEDS_BLINKING:
		mUI.radioButtonRedYellowLed->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_ON_YELLOW_GREEN_LEDS_BLINKING:
		mUI.radioButtonYellowGreenLed->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_OFF_ALL_LEDS:
		mUI.radioButtonNoLed->setChecked(true);
		break;

	default:
		mUI.radioButtonNoLed->setChecked(true);
		break;
	}
}

void TBleVvWidget::BleControllerStatedChanged(int newState)
{
	QString currentStateText = mUI.labelBleStatus->text();
	if (newState == BluetoothController::ALL_SENSORS_DISCONNECTED)
	{
		mUI.labelBleStatus->setText("No Devices Connected");
	}
	else if (newState == BluetoothController::FINDING_SENSORS)
	{
		mUI.labelBleStatus->setText("Scanning");
	}
	else if (newState == BluetoothController::CONNECTING_SENSORS)
	{
		mUI.labelBleStatus->setText("Connecting");
	}
	else if (newState == BluetoothController::ALL_SENSORS_CONNECTED)
	{
		mUI.labelBleStatus->setText("All Sensors Connected");
	}
	else if (newState == BluetoothController::LISTENING_MEASUREMENTS)
	{
		mUI.labelBleStatus->setText("Listening");
	}
	else if (newState == BluetoothController::STATE_COUNT)
	{
		mUI.labelBleStatus->setText("State Count");
	}

	// if the state truly changed, log it in the debugger
	if (QString::compare(currentStateText, mUI.labelBleStatus->text()) != 0 && ENABLE_LOG)
	{
		std::cout << ("BLE Controller state changed: " + currentStateText.toStdString() + " --> " + mUI.labelBleStatus->text().toStdString()) << std::endl;
	}
}


void TBleVvWidget::AddNewBleDeviceToList(QString newDeviceString, DWORD64 deviceID, QString deviceAddress, int deviceConnHandle, bool connHandleExists)
{
    printf("%s: connHandleExists %d\n", __FUNCTION__, connHandleExists);

	// find device using the device address
	bool foundDevice = false;
	int index = -1;
	for (unsigned int i = 0; i < mBleDeviceAddresses.size(); i++)
	{
		if (QString::compare(mBleDeviceAddresses[i], deviceAddress) == 0)
		{
			foundDevice = true;
			index = i;
		}
	}

    printf("%s: mac %llX foundDevice %d index %x connHandleExists %d\n", __FUNCTION__, deviceID, foundDevice, index, connHandleExists);
	// if device is found, see if any of the information has changed
	if (foundDevice)
	{
		// update the device string in this case and update connection handle if needed
		QString newDevString;
		if (connHandleExists)
		{
			newDevString = mBleDeviceAddresses[index] + " Connection Handle: " + QString::number(deviceConnHandle);
#if 0
			if (index < (int)mBleConnectionHandles.size())
			{
				mBleConnectionHandles[index] = deviceConnHandle;
			}
			else
			{
				for (int q = 0; q < index + 1 - (int)mBleConnectionHandles.size(); ++q)
				{
					int dummyInt(-1);
					mBleConnectionHandles.push_back(dummyInt);
				}
				mBleConnectionHandles[index] = deviceConnHandle;
			}
#endif
		}
		else
		{
			newDevString = mBleDeviceAddresses[index];
		}

		if (index < mUI.listWidget->count())
		{
			(mUI.listWidget->item(index))->setText(newDevString);
		}
	}
	else
	{
		// add the device
		// update the device string in this case and update connection handle if needed
		QString newDevString;
		if (connHandleExists)
		{
			newDevString = deviceAddress + " Connection Handle: " + QString::number(deviceConnHandle);

			// add connection handle to conn handle vec
#if 0
            mBleConnectionHandles.push_back(deviceConnHandle);
#endif
		}
		else
		{
			newDevString = deviceAddress;
		}

		// add device address to vec
		mBleDeviceAddresses.push_back(deviceAddress);
		mBleDeviceStates.push_back(0);

        QListWidgetItem *pWidgetItem = new QListWidgetItem(newDevString);
		mUI.listWidget->addItem(pWidgetItem);
        mMACToQListWidgetItem[deviceID] = pWidgetItem;
        printf("%s: Added device MAC %llX to row %d\n", __FUNCTION__, deviceID, mUI.listWidget->row(pWidgetItem));

		// see if this device was added previously to retrieve the LED state
		bool foundDeviceLed = false;
		int indexLed = -1;
		for (unsigned int i = 0; i < mBleLedStates.size(); ++i)
		{
			if (QString::compare(mBleLedStates[i].address, deviceAddress) == 0)
			{
				foundDeviceLed = true;
				indexLed = i;
			}
		}

		// if found, update the LED state of this device
		auto it = mIOHandlerController->mIOHandler.mBLEController->devices().find(deviceID);
		if (foundDeviceLed && 
			it != mIOHandlerController->mIOHandler.mBLEController->devices().end())
		{
			mIOHandlerController->mIOHandler.mBLEController->devices()[deviceID]->updateLedState((BluetoothImuDevice::LedState)mBleLedStates[indexLed].ledState);
            mIOHandlerController->mIOHandler.mBLEController->devices()[deviceID]->setStateDeviceAddedToGui(true);
		}
	}
}

int TBleVvWidget::GetListRow(DWORD64 mac) {

    int row = 0;

    for (map<DWORD64,QListWidgetItem *>::iterator it = mMACToQListWidgetItem.begin(); it != mMACToQListWidgetItem.end(); it++) {
        if (it->first == mac) {
            row = mUI.listWidget->row(it->second);  
        }
    }
    return row;
}

#if 0
void TBleVvWidget::UpdateBleDeviceString(QString newDeviceString, int deviceID, QString deviceAddress, int deviceConnHandle, bool connHandleExists)
{
	// new implementation
	// find device using the device address
	bool foundDevice = false;
	int index = -1;
	for (unsigned int i = 0; i < mBleDeviceAddresses.size(); ++i)
	{
		if (QString::compare(mBleDeviceAddresses[i], deviceAddress) == 0)
		{
			foundDevice = true;
			index = i;
		}
	}
printf("%s: foundDevice %d connHandleExists %d\n", __FUNCTION__, foundDevice, connHandleExists);

	// if device is found, see if any of the information has changed
	if (foundDevice)
	{
		// update the device string in this case and update connection handle if needed
		QString newDevString;
		if (connHandleExists)
		{
			newDevString = mBleDeviceAddresses[index] + " Connection Handle: " + QString::number(deviceConnHandle);
			if (index < (int)mBleConnectionHandles.size())
			{
				mBleConnectionHandles[index] = deviceConnHandle;
			}
			else
			{
				for (int q = 0; q < index + 1 - (int)mBleConnectionHandles.size(); ++q)
				{
					int dummyInt(-1);
					mBleConnectionHandles.push_back(dummyInt);
				}
				mBleConnectionHandles[index] = deviceConnHandle;
			}
		}
		else
		{
			newDevString = mBleDeviceAddresses[index];
		}

		if (index < mUI.listWidget->count())
		{
			(mUI.listWidget->item(index))->setText(newDevString);
		}
	}
	else
	{
		// add the device
		// update the device string in this case and update connection handle if needed
		QString newDevString;
		if (connHandleExists)
		{
			newDevString = deviceAddress + " Connection Handle: " + QString::number(deviceConnHandle);

			// add connection handle to conn handle vec
			mBleConnectionHandles.push_back(deviceConnHandle);
		}
		else
		{
			newDevString = deviceAddress;
		}
	
		// add device address to vec
		mBleDeviceAddresses.push_back(deviceAddress);
		mBleDeviceStates.push_back(0);

		mUI.listWidget->addItem(new QListWidgetItem(newDevString));


		// see if this device was added previously to retrieve the LED state
		bool foundDeviceLed = false;
		int indexLed = -1;
		for (unsigned int i = 0; i < mBleLedStates.size(); ++i)
		{
			if (QString::compare(mBleLedStates[i].address, deviceAddress) == 0)
			{
				foundDeviceLed = true;
				indexLed = i;
			}
		}

		// if found, update the LED state of this device
		if (foundDeviceLed)
		{
			mIOHandlerController->mIOHandler.mBLEController->devices()[deviceID]->updateLedState((BluetoothImuDevice::LedState)mBleLedStates[indexLed].ledState);
		}
	}
}
#endif

void TBleVvWidget::BleDeviceStateChanged(DWORD64 deviceID)
{
	char macAdr[20];
	char *pMACString = formatMAC(deviceID, macAdr, sizeof(macAdr));

    printf("%s: \n", __FUNCTION__);
    if(mUI.listWidget->selectedItems().size()==0 || mUI.listWidget->currentItem() == 0)
        return;

	printf("selitmcnt %x\n", (DWORD)mUI.listWidget->selectedItems().size());
	printf("curitm %x\n", (DWORD)mUI.listWidget->currentItem());
	if ((DWORD)mUI.listWidget->currentItem() && mUI.listWidget->currentItem()->text()!=NULL)
		printf("ptr=%x\n", (DWORD)mUI.listWidget->currentItem()->text().toLatin1().constData());

	if (mUI.listWidget->currentItem() && pMACString == mUI.listWidget->currentItem()->text().toLatin1().constData())
	{
        DWORD64 macadr;
        if( (macadr = MACFromCurrentItem())==0)
            return;
        if(auto it = mIOHandlerController->mIOHandler.mBLEController->devices().find(macadr) == mIOHandlerController->mIOHandler.mBLEController->devices().end())
            return;

		BluetoothImuDevice::DeviceState deviceState = (mIOHandlerController->mIOHandler.mBLEController->devices())[macadr]->state();

		switch (deviceState)
		{
		case BluetoothImuDevice::UNINITIALIZED:
			mUI.labelBleDeviceState->setText("Unitialized/Terminated");
			mUI.labelBleDeviceRefreshRate->setText("");
			break;

		case BluetoothImuDevice::FINDING_SERVICES:
			mUI.labelBleDeviceState->setText("Finding Services");
			mUI.labelBleDeviceRefreshRate->setText("");
			break;

		case BluetoothImuDevice::FINDING_CHARACTERISTICS:
			mUI.labelBleDeviceState->setText("Finding Characteristics");
			mUI.labelBleDeviceRefreshRate->setText("");
			break;

		case BluetoothImuDevice::__READY:
			mUI.labelBleDeviceState->setText("Ready");
			mUI.labelBleDeviceRefreshRate->setText("");
			break;

		case BluetoothImuDevice::READING:
			mUI.labelBleDeviceState->setText("Reading Data");
			mUI.labelBleDeviceRefreshRate->setText("");
			break;

		case BluetoothImuDevice::WRITING:
			mUI.labelBleDeviceState->setText("Writing Data");
			mUI.labelBleDeviceRefreshRate->setText("");
			break;

		case BluetoothImuDevice::DEVICE_STATE_COUNT:
			mUI.labelBleDeviceState->setText("Device State Count");
			break;

		default:
			mUI.labelBleDeviceState->setText("");
			mUI.labelBleDeviceRefreshRate->setText("");
			break;
		}
	}
}

void TBleVvWidget::UpdateBleDeviceLedState()
{
	// update LED state
	int ledState;
		
    printf("%s: \n", __FUNCTION__);
	if (mUI.listWidget->currentRow() >= 0 && mUI.listWidget->currentRow() < (int)mIOHandlerController->mIOHandler.mBLEController->devices().size())
	{
        DWORD64 macadr = MACFromCurrentItem();
        if (auto it = mIOHandlerController->mIOHandler.mBLEController->devices().find(macadr) == mIOHandlerController->mIOHandler.mBLEController->devices().end()) 
            return;
        ledState = mIOHandlerController->mIOHandler.mBLEController->devices()[macadr]->ledState();
	}
	else
	{
		return;
	}

	switch (ledState)
	{
	case BluetoothImuDevice::TURN_ON_RED_LED:
		mUI.radioButtonRedLed->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_ON_YELLOW_LED:
		mUI.radioButtonYellowLed->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_ON_GREEN_LED:
		mUI.radioButtonGreenLed->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_ON_RED_LED_BLINKING:
		mUI.radioButtonBlinkingRedLed->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_ON_YELLOW_LED_BLINKING:
		mUI.radioButtonBlinkingYellowLed->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_ON_GREEN_LED_BLINKING:
		mUI.radioButtonBlinkingGreenLed->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_ON_ALL_LEDS_BLINKING:
		mUI.radioButtonBlinkingAllLeds->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_ON_RED_GREEN_LEDS_BLINKING:
		mUI.radioButtonRedGreenLed->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_ON_RED_YELLOW_LEDS_BLINKING:
		mUI.radioButtonRedYellowLed->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_ON_YELLOW_GREEN_LEDS_BLINKING:
		mUI.radioButtonYellowGreenLed->setChecked(true);
		break;

	case BluetoothImuDevice::TURN_OFF_ALL_LEDS:
		mUI.radioButtonNoLed->setChecked(true);
		break;

	default:
		mUI.radioButtonNoLed->setChecked(true);
		break;
	}

	// save this Led state
	if (mUI.listWidget->currentRow() >= (int)mBleDeviceAddresses.size())
	{
		return;
	}

	DeviceLedState newState;
	newState.address = mBleDeviceAddresses[mUI.listWidget->currentRow()];
	newState.ledState = ledState;

	// see if this device was added previously to retrieve the LED state
	bool foundDeviceLed = false;
	int indexLed = -1;
	for (unsigned int i = 0; i < mBleLedStates.size(); ++i)
	{
		if (QString::compare(mBleLedStates[i].address, newState.address) == 0)
		{
			foundDeviceLed = true;
			indexLed = i;
		}
	}

	// if found, update the LED state
	if (foundDeviceLed)
	{
		mBleLedStates[indexLed] = newState;
	}
	else
	{
		mBleLedStates.push_back(newState);
	}
}

void TBleVvWidget::ConnIntervalTextEdited()
{
	unsigned int minVal, maxVal;
	minVal = mUI.lineEditMinConnInterval->text().toUInt();
	maxVal = mUI.lineEditMaxConnInterval->text().toUInt();
	mIOHandlerController->mIOHandler.SetConnectionIntervalVals(minVal, maxVal);
}

void TBleVvWidget::ScanFilterStringTextEdited()
{
	if (mUI.checkBoxBleDeviceStringFiltering->isChecked())
	{
		mIOHandlerController->mIOHandler.SetScanningFilterString(mUI.lineEditScanFilterString->text());
	}
}

void TBleVvWidget::ClearDevices()
{
	mUI.listWidget->clear();
	mBleDeviceAddresses.clear();
	mBleDeviceStates.clear();
	mIOHandlerController->mIOHandler.mBLEController->clearDevices();
}

void TBleVvWidget::RemoveDevice(int row) {

	if (mBleDeviceAddresses.size() >= 1)
		mBleDeviceAddresses.erase(mBleDeviceAddresses.begin()+row);
	if (mBleDeviceStates.size() >= 1)
		mBleDeviceStates.erase(mBleDeviceStates.begin()+row);

    QListWidgetItem *wptr = mUI.listWidget->takeItem(row);
    if (wptr) {
        for (map<DWORD64,QListWidgetItem *>::iterator it = mMACToQListWidgetItem.begin(); it != mMACToQListWidgetItem.end(); it++) {
            if (it->second == wptr) {
                mMACToQListWidgetItem.erase(it);
				break;
            }
        }
        delete wptr;
    }
}

void TBleVvWidget::ClearDeviceListAsync()
{
	ClearDevices();
}

void TBleVvWidget::SlotRefreshRateUpdated(int index)
{
#if 0
	if (mUI.listWidget->currentRow() >= 0 && mUI.listWidget->currentRow() < (int)mIOHandlerController->mIOHandler.mBLEController->devices().size())
	{
		if (mUI.listWidget->currentRow() == index)
		{
			mUI.labelBleDeviceRefreshRate->setText(QString::number(mIOHandlerController->mIOHandler.mCurrentIMUs[index].mRefreshRate));
		}
	}
#endif
}

void TBleVvWidget::quit(int rc) {

	printf("%s: ", __FUNCTION__);
	QCoreApplication::exit(rc);
}
