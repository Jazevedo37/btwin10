#include "IMU.h"
#if 0
IMU::IMU():
mIsBleIMU(false),
mBleID(0),
mImuID(0),
mBleLedState(0),
mBleDevAddress(""),
mIsConnected(false),
mRSSI(0),
mRefreshRate(0),
mIterRefreshRate(0),
mMainIMUVectorIndex(0),
mDisplayPos(0),
mHaveAddedImuToGui(false),
mbIsAutomatedCalibrationComplete(false),
mbHasStreamingDataTimedOut(false),
mbHasStreamingDataStopped(false),
mStreamingDataTimeoutIter(0),
mDt(0),
mAliveTimerCounter(0)
{
}

IMU::~IMU()
{
}

IMU& IMU::operator=(const IMU& imu)
{
	QMutex mutex;
	mutex.lock();

	mIsConnected = imu.mIsConnected;
	mAliveTimerCounter = imu.mAliveTimerCounter;
	mRefreshRate = imu.mRefreshRate;
	mIterRefreshRate = imu.mIterRefreshRate;
	mDisplayPos = imu.mDisplayPos;
	mImuID = imu.mImuID;
	mDt = imu.mDt;
	mMainIMUVectorIndex = imu.mMainIMUVectorIndex;
	mIsBleIMU = imu.mIsBleIMU;
	mBleID = imu.mBleID;
	mbIsAutomatedCalibrationComplete = imu.mbIsAutomatedCalibrationComplete;

	mStreamingDataTimeoutIter = imu.mStreamingDataTimeoutIter;
	mbHasStreamingDataStopped = imu.mbHasStreamingDataStopped;
	mbHasStreamingDataTimedOut = imu.mbHasStreamingDataTimedOut;

	mHaveAddedImuToGui = imu.mHaveAddedImuToGui;
	mBleLedState = imu.mBleLedState;
	mRSSI = imu.mRSSI;
	mBleDevAddress = imu.mBleDevAddress;
	
	mutex.unlock();
	return *this;
}
#endif