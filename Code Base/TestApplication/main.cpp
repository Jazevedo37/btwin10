#include <QtWidgets/QApplication>
#include "BleVvWidget.h"
#include "..\..\BLEServer\version.h"
#include "..\..\BLEServer\pipeprot.h"
#include "BluetoothImuDevice.h"
#include "BluetoothController.h"

QApplication *gQAppPtr = NULL;
TBleVvWidget *gpWidget = NULL;


BTPipeMsgReturnCode ReadDataPipe(HANDLE pipeHandle, unsigned char *buffer, unsigned int buflen, DWORD *bytesRead) {
	DWORD bytesPeeked = 0, bytesAvail = 0, bytesRem = 0;
	unsigned char tmpbuf[128];

	*bytesRead = 0;
	if (PeekNamedPipe(pipeHandle, tmpbuf, sizeof(tmpbuf), &bytesPeeked, &bytesAvail, &bytesRem) && bytesPeeked) {
		if (!ReadFile(pipeHandle, buffer, buflen, bytesRead, 0)) {
			printf("Error %d reading data pipe.\n", GetLastError());
			return(BT_FAIL);
		}
	}
	else Sleep(1);

    return(BT_SUCCESS);
}

BTPipeMsgReturnCode SendPipeRequest(HANDLE pipeHandle, const BTPipeMessage *msg) {
	DWORD bytesWritten, bytesRead;

	if (!WriteFile(pipeHandle, msg, sizeof(BTPipeMessage), &bytesWritten, NULL)) {
		int rc = GetLastError();
		printf("WriteFile error %d.\n", rc);
        if (rc == ERROR_PIPE_NOT_CONNECTED || rc == ERROR_NO_DATA) {
			printf("%s: Pipe communication failed, rc=%d, exiting.\n", __FUNCTION__, rc);
			qApp->exit();
//			exit(EXIT_FAILURE);
        }
		return(BT_FAIL);
	}

	if (bytesWritten != sizeof(BTPipeMessage)) {
		printf("Pipe message corrupton during write: %d bytes written to pipe.\n", bytesWritten);
		return(BT_FAIL);
	}
	if (msg->msgId != BT_DISCONNECTING_CMD_PIPE && !ReadFile(pipeHandle, (BTPipeMessage *)msg, sizeof(BTPipeMessage), &bytesRead, 0)) {
			printf("Error %d reading pipe message response for msgId %d, bytesRead=%d.\n", GetLastError(), msg->msgId, bytesRead);
			return(BT_FAIL);		
	}
    // Do another read if it's a keep-alive packet
    if (msg->msgId == BT_KEEP_ALIVE) {
printf("Got keep alive Response!\n");
		if (!ReadFile(pipeHandle, (BTPipeMessage *)msg, sizeof(BTPipeMessage), &bytesRead, 0)) {
			printf("Error %d reading pipe message response, bytesRead=%d.\n", GetLastError(), bytesRead);
			return(BT_FAIL);
		}
    }

	switch (msg->msgId) {
	case BT_SCAN:
	case BT_DEVICE_INFO:
	case BT_SET_LED:
	case BT_SET_MAC_ADDRESSES:
	case BT_READ_CHARACTERISTIC:
	case BT_WRITE_CHARACTERISTIC:
	case BT_ENABLE_NOTIFICATION:
	case BT_DISABLE_NOTIFICATION:
	case BT_DISCONNECTING_CMD_PIPE:
	case BT_DISCONNECTING_DATA_PIPE:
		return(msg->rc);

	default:
		printf("Invalid pipe response %d!\n", msg->msgId);
	}
	return(BT_FAIL);
}

char *formatMAC(unsigned long long mac, char *ptr, int len) {
	char *origPtr = ptr;
	memset(ptr, 0, len);
	for (int x = 5; x >= 0; x--) {		
		sprintf(ptr, "%2.2X", (mac >> x*8) & 0xFF);		
		if (x != 0) strcat(ptr, ":");
		ptr += 3;
	}
	return(origPtr);
}


int main(int argc, char *argv[])
{
    int numDevices=0;

    printf("BLE Test Application v%d.%d.%d:\n", VER_MAJOR, VER_MINORH, VER_MINORL);
	QApplication a(argc, argv);
	gQAppPtr = &a;
	gpWidget = new TBleVvWidget;
    numDevices = gpWidget->EnumerateImus();;
	gpWidget->show();

	int returnCode = a.exec();
    // Program exiting
	// Disable notification for each device
	printf("Shutting down.\n");

	printf("Stopping keep alive.\n");
	gpWidget->mIOHandlerController->mIOHandler.mBLEController->communicationThread().stopKeepAlive();
	for (auto it = gpWidget->mIOHandlerController->mIOHandler.mBLEController->mDevices.begin(); 
		it != gpWidget->mIOHandlerController->mIOHandler.mBLEController->mDevices.end(); it++) {
		DWORD64 macadr = it->first;
		printf("Disabling notification for MAC %d\n", macadr);
		BluetoothImuDevice *btdev = gpWidget->mIOHandlerController->mIOHandler.mBLEController->mDevices[macadr];
		if (btdev)
			btdev->BLEDisableNotification(0xFFF4, 3000);		
	}

	printf("Removing devices.\n");
	for (auto it = gpWidget->mIOHandlerController->mIOHandler.mBLEController->mDevices.begin(); 
		it != gpWidget->mIOHandlerController->mIOHandler.mBLEController->mDevices.end(); ) {
		DWORD64 macadr = it->first;
		gpWidget->mIOHandlerController->mIOHandler.mBLEController->removeDevice(macadr);
		it = gpWidget->mIOHandlerController->mIOHandler.mBLEController->mDevices.begin();
	}

	printf("DONE Removing devices.\n");

	printf(".");
	gpWidget->mIOHandlerController->mIOHandler.mBLEController->endAll();

	printf("TestApplication: exiting.\n");

	delete gpWidget;
	return returnCode;
};
