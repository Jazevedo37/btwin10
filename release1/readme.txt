2019-2-13

BLEServer Installation steps:

1.	 Make the following directory and copy qwindows.dll to it:

		C:\qt\5.5\msvc2013\plugins\platforms

2.	 Add an envionment variable named QT_PLUGIN_PATH that points to the directory above.

3.	 Copy all of the installation files to an installtion directory of your choice.

4.	 Install the Visual Studio 2013 and 2017 redistributable packages (only 32 bit are used at this time).

5.	Run the executables BLEServer and TestApplication from the installation path, or take appropriate steps
	to ensure the QT DLLs are accessible.



